#!/usr/bin/env bash
set -e

source /opt/lofar/lofarinit.sh

exec "$@"