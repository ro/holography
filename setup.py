import setuptools
import os
import glob
if os.path.exists('README.md'):
    with open("README.md", "r") as fh:
        long_description = fh.read()
else:
    long_description = ''

scripts = glob.glob('bin/*.py')

setuptools.setup(
    name="lofar-holography",
    version="0.0.1",
    author="Mattia Mancini",
    author_email="mancini@astron.nl",
    description="Holography calibration algorithm applied to the LOFAR telescope array",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://git.astron.nl/ro/holography",
    packages=setuptools.find_packages(),
    data_files = [('share/holography', ['share/holography/observation-planning-TEMPLATE-HBA.ipynb'])],
    scripts=scripts,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: Apache2 License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires = [
        'dataclasses',
        'scipy>=1.3.1',
        'numpy>=1.17.0',
        'astropy>=3.2.3',
        'h5py>=2.9.0',
        'emcee>=2.2.1',
        'numba>=0.45.1',
        'matplotlib>=3.1.1',
        'llvmlite>=0.32.1',
        'python-casacore',
        'lofarantpos',
        'lofar-obs-xml',
        'mock'
    ],
    test_suite='tests',
)
