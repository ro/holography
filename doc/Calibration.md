# Calibration {#Calibration}

## GENERAL
The calibration package contains all the source code regarding the
 calibration of the LOFAR station array.

### Description
The calibration package contains code to process the holography datasets
and derive the calibration tables for each station.
To do so, it provides a conversion util to an intermediate data container
called [HolographyDataset](@ref HolographyDataset) for each station target
of the calibration. Furthermore, the calibration package contains 
the routines to process the HolographyDataset in order to derive the
gains to apply on each antenna and generate a calibration table.   

### Author/Owner
 [Thomas Jürges](mailto:jurges@astron.nl), [Mattia Mancini](mailto:mancini@astron.nl)
### Overview
- *Add a diagram*
- *Add a link to the overview diagram*
- *Add a link in the overview diagram to link back to this documentation*.

- - -

## DEVELOPMENT

### Analyses
*Add non-technical information and functional considerations here, like user requirements and links to minutes of
meetings with stakeholders.*

### Design
*Add technical considerations and design choices here*

### Source Code
- *Add a link to svn (trunk).*
- *Add a link to (generated?) source code documentation.*

### Testing
- *How do you run unit tests?*
- *How do you run integration tests?*
- *Add a link to Jenkins jobs (if available)*

### Build & Deploy
- *Add a link to general instructions or describe specifics here.*
- *Add a link to Jenkins jobs (if available)*

- - -

## OPERATIONS

### Configuration
- *Where is the configuration file?*
- *What are the configuration options?*

### Log Files
- *Where are the log files?*

### Runtime
- *Where does it run? (which user @ machine)*
- *How do I run it? (user documentation? examples? commandline parameters?)*
- *Other considerations? (what happens elsewhere when I start or stop it?)*

### Interfaces (API)
- *Describe interfaces to other applications (REST API? http requests? Messagebus?)*
- *Other communication (user? import/export?)*

### Files/Databases
- *Which databases are used?*
- *Which files are used?*

### Dependencies
- *To/from other applications?*
- *Files?*
- *Network locations?*
- *Other?*

### Security
- *Special privileges needed?*
- *User login?*
- *Certificates needed?*
- *Other considerations?*

- - -

## ADDITIONAL INFORMATION

### User Documentation

*e.g. Please refer to URL X for the User Documentation*

### Operations Documentation

*e.g. Please refer to URL X for Operations Documentation*
