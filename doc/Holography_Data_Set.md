# Definition of the LOFAR Holography Data Set {#HolographyDataset}

Authors:  [Thomas Jürges](mailto:jurges@astron.nl), [Mattia Mancini](mailto:mancini@astron.nl)

HOLOGRAPHY_DATA_SET_VERSION
Date:  2018-09-10


Version:  1.0


References:  n.a.


Abbreviations:

- *HDS*:  Holography Data Set
- *LOFAR*:  LOw Frequency ARray
- *MJD*:  Modified Julian Date
- *MS*:  Measurement Set


## The LOFAR Holography Data Set (HDS)
The LOFAR Holography Data Set (HDS) serves as data container for cross-correlation results that have been calculated from the Measurement Sets of holography observations.  It also contains the complete specification that was provided when the holography observation was entered into the observation planning system.

### HDS from planning to data
Once all information is known, the attributes of the HDS will get filled in and the first instance of the HDS will be stored as a file on disk.  After the observation is finished and the cross-correlations have been calculated the results are stored in MS.  A conversion script takes the MS and converts it into the HDS.


## The HDS in real life
### Attributes of the HDS
Attributes are entities that are specified during the planning of a holography observation.  They define what the *intention* at the time of the planning is.  After a holography observation is scheduled its attributes will not change any more.  Hence they are an immutable description of the planned observation.  This information allows to repeat the same holography observation at a later time.

Below is the list of defined attributes in this version of HDS:

- **HDS_version**:  The HDS is versioned and begins at version 1.0.  The versions are incremental.  *Unit:  [n.a.]*
- **RCU_list**:  An array that contains the list of RCUs that are planned to be used during the holography observation.  *Unit:  [n.a.]*
- **Mode**:  The RCU mode that will be set during the holography observation.  *Unit:  [n.a.]*
- **SAS_ids**:  An array that contains the SAS ids that were created for all observation runs of the holography observation.  *Unit:  [n.a.]*
- **Target_station_name**:  Name of the LOFAR target station.  *Unit:  [n.a.]*
- **Target_station_position**:  Position in cartesian coordinates.  *Unit:  [m, m, m]*
- **Central_beamlets**:  An array that contains the central beamlet number for each frequency.  *Unit:  [n.a.]* 
- **Source_name**:  Name of the source observed during the holography observation.  *Unit:  [n.a.]*
- **Source_position**:  Celestial coordinates (RA, DEC, EPOCH) of the source.  *Unit:  [rad, rad, n.a.]*
- **Observation_time**:  Planned start and end time-stamps of the entire holography observation in MJD.  *Unit:  [s]*
- **Rotation_matrix**:  Rotation matrix that allows to convert (RA, DEC) coordinates to (l, m) coordinates and vice versa.  *Unit:  [n.a.]*
- **Antenna_field_position**:  Cartesian coordinates with reference to IRTF of the centre of each antenna field that will be used in the holography observation.  *Unit:  [m, m, m]*

Additionally if the HDS has been used to specify the holography observation these fields will also be present:
- **Specified reference station**:  List of reference stations that were planned for the holography observation.  *Unit: [n.a.]*
- **Specified frequency**:  List of frequencies that were planned to be used for the holography observation.  *Unit: [Hz]*
- **Specified RA DEC**:  List of the planned target station's beamlet pointings for the holography observation.  *Unit: [rad, rad]*


### Data tables in the HDS
Data tables serve as the work horses for the cross-correlations in an HDS.  They contain a list of the actually used reference stations, a list of the actually used frequencies, the actually used target station beam positions and the calculated cross-calculation values.

Below is the list of defined data tables in this version of HDS:

- **Reference_station**:  The reference station data table contains the list of all reference stations that were actually used in the holography observation.  This list can contain less stations than the **Specified reference station** list.  For a given reference station the index of it in this list serves as one of the three indices of **data**.  *Unit:  [n.a.]
- **frequency**:  List of frequencies that were actually used for the holography observation.  This list can contain less elements than the **Specified frequency** list.  For a given frequency the index of it in this list serves as one of the three indices of **data**.  *Unit [Hz]*

Additionally, a table containing the cross correlations and the right ascension and declination of each beam are stored in a group structure that will be described in the next sub section.


### Groups in the HDS
The right ascension, declination and the crosscorrelation are stored in the HDS in a hierarchical group structure.  This allows to access a specific set of samples by the reference station name, the frequency of the samples and the beamlet number.

The notation ```%(station_name)``` is used to refer to a string containing a station name. The same notation is also used to refer to the string representation of the frequency in Hz (```%(frequency)```) and the beamlet number (```%(beamlet)```).

The structure of the groups is the following:

```
/CROSSCORRELATION
    /%(station_name)
        /%(frequency)
            %(beamlet) This data table contains the cross-correlations *XX*, *XY*, *YX*, *YY*, l, m, the time stamp of the sample, and if or not the sample has been flagged for a given frequency, beamlet# and reference station.  *Unit:  [(n.a, n.a.), (n.a, n.a), (n.a, n.a), (n.a, n.a), (n.a), (n.a), (s), (True|False)]*\

/RA_DEC
    /%(frequency)
        %(beamlet) This data table contains the (right ascension, declination, epoch) of the target station beam pointings.  This list makes it possible to translate between (frequency, beamlet) and (RA, DEC).  *Unit:  [rad, rad]* 
```

## Cross-correlation data in HDS
Values in the cross-correlation tables are stored in a pseudo-structure.  A simple representation of a single element in cross-correlation table can be given in Python language:

```
    data_element = dict(
        "XX" = c_XX,
        "XY" = c_XY,
        "YX" = c_YX,
        "YY" = c_YY,
        "t" = t_MJD,
        "l" = l,
        "m" = m,
        "flag"= flag)
```
Here the indices "XX", "XY", "YX", "YY" allow accessing the cross-correlation values, "t" allows to access the MJD of the observation and "l", "m" allow to access the coordinates l and m and finally flag to access to the flag value.

As described before each cross-correlation table can be accessed by indices, meaning that the observation samples at a given frequency `%(frequency)`, for a reference station `%(station_name)` and a beamlet `%(beamlet)` can be directly accessed as `/%(station_name)/%(frequency)/%(beamlet)`.  Example: `/RS409C/114843750.0/0`


## Layout of the HDS in HDF5 files
```
# Root
/
    # Attributes
    /ATTRS
        /Antenna field position (1-d array, double[3]), cartesian coordinates of the centres of each antenna field
        /HDS_version (double), holography file format version
        /Mode (int), RCU mode used during all observations
        /Observation_time (double[2]), Start and end time stamps of the entire holography observation
        /RCU_list (vararray[int]), entries are RCU#s used in all observations
        /Rotation_matrix (2-d array, double[3][3]), translation matrix for (RA, DEC) <-> (l, m) conversion
        /SAS_ids (vararray[string]), contains all SAS ids of all observations
        /Source_name (string), name of the observed source in the sky
        /Source_position (compound(double, double, string), RA, DEC and Epoch of the source in the sky
        /Target_station_name (string), name of the target station
        /Target_station_position (double[3]), position of the centre of the station in cartesian coordinates
        /Central_beamlets (1-d array, int), beamlet number of the central beamlet, index is the frequency
        /Specified_reference_station (1-d array, string), entries are reference station names [optional]
        /Specified_frequency (1-d array, double), entries are the frequencies of all observations that were specified [optional]
        /Specified_RA_DEC (2-d array, compound[double, double, string[10]]), indices are frequency index, beamlet# [optional]

    # Groups
    /Reference_station (1-d array, string[8]), entries are reference station names
    /Frequency (1-d array, double), entries are the frequencies of all observations
    /RA_DEC
        /[One entry for each of the frequencies]  Index is the frequency in Hz as string.
             /[One entry for each of the beamlets]  Index is the beamlet number as string.
                /(1-d array, compound[double, double, string[10]]), entries contain a struct of {RA, DEC, Epoch}

    /CROSSCORRELATION
        /[One entry for each of the reference stations]  Index is the name of the reference station.
            /[One entry for each of the frequencies]  Index is the frequency in Hz as string.
                /[One entry for each of the beamlets]  Index is the beamlet number as string.
                    /data (1-d array, vararray[compound[compound[4][double, double], double[3], boolean]], entries contain a struct of {XX, XY, YX, YY, l, m, t, flag}. 
```


## Example data for tests
Test data for two frequencies can be found on LOFAR's CEP3 in the directories
/data014/scratch/veen/holography/20180718/obs3/L661022 and
/data014/scratch/veen/holography/20180718/obs3/L661024.

The MS to HDS conversion utility will create an HDF5 file containing an HDS from a MS.  This file can then be used as input for some of the tests of the conversion utility that verifies a correct conversion.

