#!/usr/bin/env python3

import argparse
import logging
from lofar.calibration.common.utils import *
import lofar.calibration.processing as processing
import sys

DEFAULT_SLEEP_TIME = 1

logger = logging.getLogger('mshologextract')


def main():
    cla_parser = specify_command_line_arguments()
    arguments = parse_command_line_arguments_and_set_verbose_logging(cla_parser)

    preaverage_holography_dataset(input_path=arguments.input_path,
                                  output_path=arguments.output_path,
                                  average_samples=arguments.average_samples,
                                  time_average_step=arguments.time_average_step)


def parse_command_line_arguments_and_set_verbose_logging(parser):
    """
    Given a parser it parses the command line arguments and return the result of the parsing.

    :param parser: command line parser
    :type parser: argparse.ArgumentParser
    :return: the parsed arguments
    :rtype: argparse.Namespace
    """
    arguments = parser.parse_args()
    if arguments.v:
        logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=logging.DEBUG)

    else:
        logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=logging.INFO)

    logger.info('The selected arguments are %s', arguments)
    return arguments


def specify_command_line_arguments():
    """
    Setup the command argument parser and returns it
    :return: the parser for the command line arguments
    :rtype: argparse.ArgumentParser
    """
    parser = argparse.ArgumentParser(description='This program is meant for convert the Holography'
                                                 ' observation\'s data into an holography dataset')
    parser.add_argument('input_path', help='path to the holography observation data')
    parser.add_argument('output_path', help='path to the holography observation data')
    parser.add_argument('--time_average_step',
                        help='average window for the crosscorrelation in seconds', default=None,
                        type=float)
    parser.add_argument('--average_samples',
                        help='number of samples to perform one average step of the'
                             'cross correlations', default=None, type=int)

    parser.add_argument('--num_proc', help='number of processes used to convert the holography'
                                           ' observation', type=int)
    parser.add_argument('-v', help='verbose logging', action='store_true', default=False)

    return parser


def average_by_sample(input_path, average_samples):
    logger.info('Averaging %s with a sample window of %s', input_path, average_samples)
    output_hds = processing.average_dataset_by_sample(input_path, average_samples)
    logger.info('Averaged %s with a sample window of %s', input_path, average_samples)
    return output_hds


def average_by_time_interval(input_path, time_interval):
    logger.info('Averaging %s with a time sample window of %s s', input_path, time_interval)
    output_hds = processing.average_dataset_by_time(input_path, time_interval)
    logger.info('Averaged %s with a time sample window of %s', input_path, time_interval)
    return output_hds


def preaverage_holography_dataset(input_path, output_path, average_samples, time_average_step):
    """

    :param infile_path: path to the input file
    :param outfile_path: path to the output file
    :param average_samples: averaging sample step
    :param time_average_step: averaging time window
    :return:
    """

    if average_samples is not None and time_average_step is not None:
        logger.error('Please specify either the average window in samples'
                     ' or the time average step in seconds')
        raise ValueError('Both average_samples and time_average_step have been specified')

    if average_samples is not None:
        output_hds = average_by_sample(input_path, average_samples)
    elif time_average_step is not None:
        output_hds = average_by_time_interval(input_path, time_average_step)
    else:
        logger.error('Neither average_samples nor time_average_step has been specified')
        raise ValueError('Neither average_samples nor time_average_step has been specified')

    logger.info('Storing processed file %s in %s', input_path, output_path)
    output_hds.store_to_file(output_path)
    logger.info('Stored processed file %s in %s', input_path, output_path)


if __name__ == '__main__':
    main()
