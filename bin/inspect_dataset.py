#!/usr/bin/env python3
import argparse
import logging
import os
import sys

import h5py
import matplotlib as mpl

import lofar.calibration.common.datacontainers as datacontainers
from lofar.calibration.processing.decorators import log_step_execution
from lofar.calibration.processing.inspect import plot_station_averaged_visibilities_lm_plane, \
    plot_gains_per_antenna, plot_gains_fit_per_antenna, plot_illumination_comparison, plot_visibilities_comparison, \
    plot_illumination_comparison_full

logger = logging.getLogger('holography_inspect')


def setup_matplotlib():
    if 'cairo' in mpl.rcsetup.non_interactive_bk:
        mpl.rcParams['backend'] = 'cairo'
    elif 'agg' in mpl.rcsetup.non_interactive_bk:
        mpl.rcParams['backend'] = 'agg'
    else:
        raise ModuleNotFoundError('Please install either cairo or agg backend')

    mpl.rcParams['figure.autolayout'] = True


def setup_argument_parser():
    """

    :return:
    """
    parser = argparse.ArgumentParser(
        description='Inspect the processed holography dataset')
    parser.add_argument('input_path', help='Input holography dataset', type=str)
    parser.add_argument('output_path', help='Output holography dataset', type=str)
    parser.add_argument('verbose', help='Set verbose logging', action='store_false')
    return parser


def parse_command_arguments(arguments):
    """

    :param arguments:
    :type arguments: list
    :return:
    """
    parser = setup_argument_parser()
    return parser.parse_args(arguments)


@log_step_execution('load', logger)
def loading(path):
    """

    :param path:
    :type path: str
    :return:
    :rtype: datacontainers.HolographyDataset
    """
    if os.path.exists(path):
        f = h5py.File(path, 'r')
    else:
        raise Exception('File %s doesnt exit', path)

    dataset = datacontainers.HolographyDataset.read_holography_file_from_h5file(f)
    return dataset, f


@log_step_execution('plot station averaged visibilities', logger)
def plot_averaged_data(dataset: datacontainers.HolographyDataset, save_to):
    try:
        plot_station_averaged_visibilities_lm_plane(dataset, save_to=save_to)
    except KeyError:
        logger.error('Station averaged visibilities not present in the holography'
                     ' dataset. Skipping plot generation...')


@log_step_execution('plot gains per frequency', logger)
def plot_gains(dataset: datacontainers.HolographyDataset, save_to):
    try:
        plot_gains_per_antenna(dataset, save_to=save_to)
    except KeyError:
        logger.error('Gains not present in the holography dataset.'
                     ' Skipping plot generation...')


@log_step_execution('plot gains fit per frequency', logger)
def plot_gains_fit(dataset: datacontainers.HolographyDataset, save_to):
    try:
        plot_gains_fit_per_antenna(dataset, save_to=save_to)
    except KeyError:
        logger.error('Gains fit not present in the holography dataset. Skipping plot generation...')


@log_step_execution('plot illuminations', logger)
def plot_illumination_difference(dataset: datacontainers.HolographyDataset, save_to):
    try:
        plot_illumination_comparison(dataset, save_to=save_to)
    except KeyError:
        logger.error('Sampled illuminations not present in the holography dataset. Skipping plot generation...')


@log_step_execution('plot illuminations comparison full_grid', logger)
def plot_illumination_difference_fullgrid(dataset: datacontainers.HolographyDataset, save_to):
    try:
        plot_illumination_comparison_full(dataset, save_to=save_to)
    except KeyError as e:
        logger.exception(e)
        logger.error('Sampled model visibilities not present in the holography dataset. Skipping plot generation...')


@log_step_execution('plot visibilities comparison', logger)
def plot_reconstructed_visibilities_difference(dataset: datacontainers.HolographyDataset, save_to):
    try:
        plot_visibilities_comparison(dataset, save_to=save_to)
    except KeyError:
        logger.error('Model visibilities not present in the holography dataset. Skipping plot generation...')


def close_file(f):
    if f is not None:
        f.close()


def execute_processing(arguments):
    dataset, file_descriptor = loading(arguments.input_path)

    plot_gains_fit(dataset, save_to=arguments.output_path)
    plot_gains(dataset, save_to=arguments.output_path)

    plot_illumination_difference_fullgrid(dataset, save_to=arguments.output_path)
    plot_illumination_difference(dataset, save_to=arguments.output_path)
    plot_reconstructed_visibilities_difference(dataset, save_to=arguments.output_path)
    plot_averaged_data(dataset, save_to=arguments.output_path)

    close_file(file_descriptor)


def setup_logger(arguments):
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    if arguments.verbose:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)


if __name__ == '__main__':
    arguments = parse_command_arguments(sys.argv[1:])
    setup_logger(arguments)
    setup_matplotlib()
    execute_processing(arguments)
