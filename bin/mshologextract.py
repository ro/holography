#!/usr/bin/env python3

import argparse
from functools import reduce
import signal
import time
from multiprocessing import Pool as ThreadPool
import os

from lofar.calibration.common.utils import *
from lofar.calibration.common.datacontainers.holography_dataset import HolographyDataset
from lofar.calibration.common.datacontainers.calibration_table import \
    read_calibration_tables_per_station_antennaset_band
import logging

DEFAULT_SLEEP_TIME = 1

logger = logging.getLogger('mshologextract')


def main():
    cla_parser = specify_command_line_arguments()
    arguments = parse_command_line_arguments_and_set_verbose_logging(cla_parser)
    read_holography_datasets_and_store(arguments.input_path, arguments.output_path,
                                       holography_bsf_path=arguments.holography_bsf,
                                       holography_ms_path=arguments.holography_ms,
                                       calibration_tables_path=arguments.calibration_tables,
                                       n_processes=arguments.num_proc)


def parse_command_line_arguments_and_set_verbose_logging(parser):
    """
    Given a parser it parses the command line arguments and return the result of the parsing.

    :param parser: command line parser
    :type parser: argparse.ArgumentParser
    :return: the parsed arguments
    :rtype: argparse.Namespace
    """
    arguments=parser.parse_args()
    if arguments.v:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    return arguments


def specify_command_line_arguments():
    """
    Setup the command argument parser and returns it
    :return: the parser for the command line arguments
    :rtype: argparse.ArgumentParser
    """
    parser = argparse.ArgumentParser(description='This program is meant for convert the Holography'
                                                 ' observation\'s data into an holography dataset')
    parser.add_argument('input_path', help='path to the holography observation data')
    parser.add_argument('output_path', help='path to the holography observation data')

    parser.add_argument('--holography_bsf', help='override default path for the holography beam'
                                                 ' specification file', default=None)
    parser.add_argument('--holography_ms', help='override default path for the holography'
                                                ' observation MS files', default=None)
    parser.add_argument('--calibration_tables', help='override default path for the holography'
                                                     ' calibration table files used', default=None)

    parser.add_argument('--num_proc', help='number of processes used to convert the holography'
                                           ' observation', type=int)
    parser.add_argument('-v', help='verbose logging', action='store_true')

    return parser

def short_set_name(antennaset):
    if antennaset[0:3]=="HBA":
       return "HBA"
    else:
       return antennaset

def read_and_store_single_target_station(target_station_name,
                                         matched_bsf_ms_pair,
                                         caltables,
                                         store_path):
    """
    Reads a single target station hdf5 file from a list of bsf and ms pairs
    :param target_station_name: target station name to process
    :type target_station_name: str
    :param matched_bsf_ms_pair: the list containing a tuple with the holography specification and
    its specific holography observation
    :type matched_bsf_ms_pair: list[(lofar.calibration.common.datacontainers.HolographySpecification,
                                    lofar.calibration.common.datacontainers.HolographyObservation)]
    :param caltables: calibration tables used during the observation
    :type caltables: Dict[Tuple[str, str, str], CalibrationTable]
    :param store_path: path where to store the file
    :type store_path: str
    :return:
    """
    try:
        holography_dataset = HolographyDataset()
        logger.info('Loading data for station %s ', target_station_name)

        holography_dataset.load_from_beam_specification_and_ms(target_station_name,
                                                               matched_bsf_ms_pair)
        logger.info('Loading calibration tables per station %s', target_station_name)

        station_name = target_station_name.replace('HBA0', '').replace('HBA1', '').replace('HBA', '')
        try:
            holography_dataset.insert_calibration_table(caltables[(station_name,
                                                                  short_set_name(holography_dataset.antenna_set), holography_dataset.filter)])
        except KeyError:
            logger.error('cannot find calibration file for station %s and mode %s', station_name,
                                                                               holography_dataset.mode)
            raise Exception('cannot find calibration file for station %s and mode %s' % (station_name,
                                                                                         holography_dataset.mode))

        filename = '%s.hdf5' % target_station_name
        outpath = os.path.join(store_path, filename)

        logger.info('Storing data for station %s in %s', target_station_name, outpath)

        holography_dataset.store_to_file(outpath)

        logger.info('Stored data for station %s in %s', target_station_name, outpath)
        return True, target_station_name, ''

    except Exception as e:
        return False, target_station_name, str(e)


def make_worker_ignore_siginterrupt():
    """
    Force the worker process to ignore keyboard interrupt signals
    """
    signal.signal(signal.SIGINT, signal.SIG_IGN)


def wait_for_processes_to_complete_execution(thread_pool,
                                             future_results,
                                             sleep_time):
    """
    Wait for the processess to complete execution if KeyboardInterrupt signal is sent
    it terminates all the processes and wait for them to terminated.
    :param thread_pool: pool of processes
    :type thread_pool: multiprocess.Pool
    :param future_results: list of result objects
    :type future_results: list[multiprocess.pool.ApplyResult]
    """
    all_done = False
    n_to_be_processed = len(future_results)
    try:
        while not all_done:
            are_results_ready = list(map(lambda x: x.ready(), future_results))

            result_status = map(lambda x: 1 if x else 0,
                                          are_results_ready)
            n_processed_dataset = sum(result_status)
            logger.info('processed %s of %s', n_processed_dataset, n_to_be_processed)

            all_done = reduce(lambda x, y: x and y, are_results_ready, True)

            time.sleep(sleep_time)

    except KeyboardInterrupt:
        thread_pool.terminate()
        thread_pool.join()

    thread_pool.close()
    thread_pool.join()

    successful_stations = []
    stations_with_errors_and_errors = []
    for result in future_results:
        is_successful, station_name, error = result.get()
        if is_successful:
            successful_stations.append(station_name)
        else:
            stations_with_errors_and_errors.append((station_name, error))

    logger.info('Data for stations %s converted successfully', successful_stations)
    logger.info('Converting data per station %s presented errors', stations_with_errors_and_errors)


def read_holography_datasets_and_store(holography_observation_path,
                                       store_path,
                                       holography_bsf_path=None,
                                       holography_ms_path=None,
                                       calibration_tables_path=None,
                                       n_processes=1,
                                       sleep_time=DEFAULT_SLEEP_TIME):

    """
    Read the holography datasets and store them in a the specified directory
    :param holography_observation_path: path to the holography observation
    :type holography_observation_path: str
    :param store_path: path were
    :type store_path: str
    :param holography_bsf_path: path to the beam specification file
    :type holography_bsf_path: str
    :param holography_ms_path: path to the measurement set directory
    :type holography_ms_path: str
    :param calibration_tables_path: path to the calibration tables
    :type calibration_tables_path: str
    :param n_processes: number of precesses to spawn to do the conversion
    :type n_processes: int
    """

    if holography_ms_path is not None:
        raise NotImplementedError()
    if holography_bsf_path is not None:
        if not os.path.exists(holography_bsf_path):
            raise FileNotFoundError(holography_bsf_path)
    else:
        holography_bsf_path = holography_observation_path

    if calibration_tables_path is None:
        calibration_tables_path = holography_observation_path

    target_station_names = list_all_target_stations(holography_bsf_path)

    logger.debug('target station names %s', target_station_names)
    matched_bsf_ms_pair =\
        match_holography_beam_specification_file_with_observation(holography_bsf_path, holography_observation_path)
    logger.debug('matched beam specification files and measurement sets: %s',
                 matched_bsf_ms_pair)

    calibration_tables = read_calibration_tables_per_station_antennaset_band(calibration_tables_path)
    logger.info('calibration tables found %s', list(calibration_tables.keys()))
    if len(calibration_tables) == 0:
        logger.error('calibration tables not found in directory %s', calibration_tables_path)
        raise SystemExit
    try:
        if not os.path.exists(store_path):
            os.makedirs(store_path)
    except Exception as e:
        logger.exception('Cannot create output path %s: %s', store_path, e)
        raise e

    thread_pool = ThreadPool(n_processes, make_worker_ignore_siginterrupt)
    results = []
    for station_name in target_station_names:
        result = thread_pool.apply_async(read_and_store_single_target_station,
                               (station_name, matched_bsf_ms_pair, calibration_tables,
                                store_path))

        results.append(result)

    wait_for_processes_to_complete_execution(thread_pool, results, sleep_time)


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=logging.DEBUG)
    main()
