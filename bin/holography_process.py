#!/usr/bin/env python3
import argparse
import functools
import logging
import os
import sys

import lofar.calibration.common.datacontainers as datacontainers
from lofar.calibration.processing.averaging import average_data, \
    weighted_average_dataset_per_station
from lofar.calibration.processing.decorators import log_step_execution
from lofar.calibration.processing.inspect import compute_illumination, compute_beam_shape,\
    compute_sampled_illumination, compute_model_visibilities
from lofar.calibration.processing.interpolate import derive_interpolation_parameters
from lofar.calibration.processing.normalize import normalize_beams_by_central_one
from lofar.calibration.processing.solver import solve_gains_per_datatable

logger = logging.getLogger('holography_process')

FAIL_SAVE_STORAGE_PATH = os.getcwd()


def save_in_case_of_exception():
    def save_in_case_of_exception_decorator(function):
        @functools.wraps(function)
        def wrapper(input, *args, **kwargs):
            try:
                result = function(input, *args, **kwargs)
                return result
            except Exception as e:
                logger.exception('exception occurred executing function %s: %s',
                                 function.__name__, e)
                if isinstance(input, datacontainers.HolographyDataset):
                    store_step(input, os.path.join(FAIL_SAVE_STORAGE_PATH, 'ERROR-%s.hdf5' % input.target_station_name))
                raise e

        return wrapper

    return save_in_case_of_exception_decorator


def setup_argument_parser():
    """

    :return:
    """
    parser = argparse.ArgumentParser(
        description='Computes the gains per each antenna from the given Holography Dataset')
    parser.add_argument('input_path', help='Input holography dataset', type=str)
    parser.add_argument('output_path', help='Output holography dataset', type=str)
    parser.add_argument('--verbose', help='Set verbose logging', action='store_true')
    parser.add_argument('--fail_save_path', help='path where to save the dataset if an error occurs',
                        default=os.getcwd())
    return parser


def parse_command_arguments(arguments):
    """

    :param arguments:
    :type arguments: list
    :return:
    """
    parser = setup_argument_parser()
    return parser.parse_args(arguments)


@log_step_execution('load', logger)
def loading(path):
    """

    :param path:
    :type path: str
    :return:
    :rtype: datacontainers.HolographyDataset
    """

    dataset = datacontainers.HolographyDataset.load_from_file(path)
    return dataset


@log_step_execution('normalize', logger)
@save_in_case_of_exception()
def normalize_step(dataset, input_datatable):
    """
    Normalize the crosscorrelation for the central beam
    :param dataset: Input dataset
    :type dataset: datacontainers.HolographyDataset
    :param input_datatable: Input data
    :type input_datatable: dict(dict(dict(numpy.ndarray)))
    :return: the crosscorrelations normalized for the central beam
    :rtype: dict(dict(dict(numpy.ndarray)))
    """
    output_datatable = normalize_beams_by_central_one(dataset,
                                                      input_datatable,
                                                      dataset.central_beamlets())
    dataset.derived_data['NORMALIZED'] = output_datatable
    return output_datatable


@log_step_execution('time average', logger)
@save_in_case_of_exception()
def time_averaging_step(dataset, input_datatable):
    """
    Perform the time average of the crosscorrelation
    :param dataset: Input dataset
    :type dataset: datacontainers.HolographyDataset
    :param input_datatable: crosscorrelation on which to compute the average
    :type input_datatable: dict(dict(dict(numpy.ndarray)))
    :return: the time averaged crosscorrelations
    :rtype: dict(dict(dict(numpy.ndarray)))
    """
    output_datable = average_data(input_datatable)
    dataset.derived_data['TIME_AVERAGED'] = output_datable
    return output_datable


@log_step_execution('station average', logger)
@save_in_case_of_exception()
def station_averaging_step(dataset, input_datatable):
    """
    Perform the weighted average of the crosscorrelations over the station
    the weight is choose to be 1/sigma^2

    :param dataset: Input dataset
    :type dataset: datacontainers.HolographyDataset
    :param input_datatable: Input datatable
    :type input_datatable: dict(dict(dict(numpy.ndarray)))
    :return: the station averaged crosscorrelations
    :rtype: dict(dict(dict(numpy.ndarray)))
    """
    output_datable = weighted_average_dataset_per_station(dataset, input_datatable)
    if dataset.derived_data is None:
        dataset.derived_data = dict()
    dataset.derived_data['STATION_AVERAGED'] = output_datable
    return output_datable


@log_step_execution('solving gains', logger)
@save_in_case_of_exception()
def compute_gains_step(dataset, input_datatable):
    """
    Derive the gains for every antenna in the station given the input crosscorrelations

    :param dataset: Input dataset
    :type dataset: datacontainers.HolographyDataset
    :param input_datatable: Input datatable
    :type input_datatable: dict(dict(dict(numpy.ndarray)))
    :return: The gains computed for each antenna
    :rtype: dict(dict(dict(numpy.ndarray)))
    """
    output_datable = solve_gains_per_datatable(dataset, input_datatable)
    if dataset.derived_data is None:
        dataset.derived_data = dict()
    dataset.derived_data['GAINS'] = output_datable
    return output_datable


@log_step_execution('gains interpolation', logger)
@save_in_case_of_exception()
def interpolate_gains_step(dataset, input_datatable):
    """
    Interpolate the gains with a linear regression for phases and amplitude as a function of the frequency
    :param dataset: Input dataset
    :type dataset: datacontainers.HolographyDataset
    :param input_datatable: Input datatable
    :type input_datatable: dict(dict(dict(numpy.ndarray)))
    :return: Gains fit parameters
    :rtype: dict(dict(dict(numpy.ndarray)))
    """
    output_datable = derive_interpolation_parameters(input_datatable)
    if dataset.derived_data is None:
        dataset.derived_data = dict()
    dataset.derived_data['INTERPOLATED_GAINS'] = output_datable
    return output_datable


@log_step_execution('compute illumination', logger)
@save_in_case_of_exception()
def illumination_step(dataset):
    """
    Compute the illumination of the array
    :param dataset: Input dataset
    :type dataset: datacontainers.HolographyDataset
    :param input_datatable: Input datatable
    :type input_datatable: dict(dict(dict(numpy.ndarray)))
    :return: Holography dataset
    """
    compute_illumination(dataset)
    return dataset


@log_step_execution('compute sampled illumination', logger)
@save_in_case_of_exception()
def sampled_illumination_step(dataset):
    """
    Compute the sampled illumination from the visibilities
    :param dataset: Input dataset
    :type dataset: datacontainers.HolographyDataset
    :param input_datatable: Input datatable
    :type input_datatable: dict(dict(dict(numpy.ndarray)))
    :return: Holography dataset
    """
    if dataset.derived_data is None:
        dataset.derived_data = dict()
    dataset.derived_data['SAMPLED_ILLUMINATION'] = compute_sampled_illumination(dataset)

    return dataset

@log_step_execution('compute reconstructed visibilities', logger)
@save_in_case_of_exception()
def reconstructed_visibilities_step(dataset):
    """
    Compute the reconstructed visibilities comparison
    :param dataset: Input dataset
    :type dataset: datacontainers.HolographyDataset
    :param input_datatable: Input datatable
    :type input_datatable: dict(dict(dict(numpy.ndarray)))
    :return: Holography dataset
    """
    if dataset.derived_data is None:
        dataset.derived_data = dict()
    dataset.derived_data['MODEL_VISIBILITIES'] = compute_model_visibilities(dataset)
    return dataset


@log_step_execution('compute beam shape', logger)
@save_in_case_of_exception()
def beam_shape_step(dataset):
    """
    Compute the recontructed beam shape
    :param dataset: Input dataset
    :type dataset: datacontainers.HolographyDataset
    :param input_datatable: Input datatable
    :type input_datatable: dict(dict(dict(numpy.ndarray)))
    :return: Holography dataset
    """
    compute_beam_shape(dataset)
    return dataset


@log_step_execution('save', logger)
@save_in_case_of_exception()
def store_step(dataset, filepath):
    """
    Store the dataset at the given path
    :param dataset: Input dataset
    :type dataset: datacontainers.HolographyDataset
    :param filepath: path where to store the file
    :type filepath: str
    """
    absolute_filepath = os.path.abspath(filepath)
    dataset.store_to_file(absolute_filepath)


def prepare_dataset_for_processing(dataset: datacontainers.HolographyDataset):
    if dataset.derived_data is None:
        dataset.derived_data = dict()


def execute_processing(arguments):
    dataset = loading(arguments.input_path)
    prepare_dataset_for_processing(dataset)

    normalized_data = normalize_step(dataset, dataset.data)
    averaged_data = time_averaging_step(dataset, normalized_data)
    station_averaged_data = station_averaging_step(dataset, averaged_data)
    gains = compute_gains_step(dataset, station_averaged_data)
    interpolate_gains_step(dataset, gains)

    illumination_step(dataset)
    sampled_illumination_step(dataset)
    beam_shape_step(dataset)
    reconstructed_visibilities_step(dataset)

    store_step(dataset, arguments.output_path)


def setup_logger(arguments):
    format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    if arguments.verbose:
        logging.basicConfig(format=format, level=logging.DEBUG)
    else:
        logging.basicConfig(format=format, level=logging.INFO)


if __name__ == '__main__':
    arguments = parse_command_arguments(sys.argv[1:])
    FAIL_SAVE_STORAGE_PATH = arguments.fail_save_path
    setup_logger(arguments)
    execute_processing(arguments)
