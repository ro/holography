#!/usr/bin/env python
from __future__ import print_function
import sys
if sys.version_info[0] < 3 or (sys.version_info[0] == 3 and sys.version_info[1] < 6):
    sys.stderr.write('FATAL: Python version must be at least 3.6\n')
    sys.exit(-1)

import argparse
import os, getpass
import pathlib
from astropy import units as u
from astropy import time 


def install_prefix():
    from lofar import __file__ as fname
    path_elements = pathlib.PurePath(fname).parts
    path_to_module = path_elements[:-2]
    if path_to_module[-1] == 'site-packages':
        if 'python' in path_to_module[-2] and 'lib' in path_to_module[-3]:
            return os.path.join(*(path_to_module[:-3]))
    return os.path.join(*path_to_module)

search_path = [install_prefix(),
               os.sep.join(install_prefix().split(os.sep)[:-2]),
               '/usr/local', '/usr']

for attempt in search_path:
    share = os.path.join(attempt, os.path.join('share', 'holography'))
    print(share)
    if os.path.exists(os.path.join(share, 'observation-planning-TEMPLATE-HBA.ipynb')):
        break

LOFAR_INSTALL_ROOT = install_prefix()
LOFAR_SHARE = share

DEFAULT_TEMPLATE = os.path.join(LOFAR_SHARE, 'observation-planning-TEMPLATE-HBA.ipynb')
DEFAULT_ROOT     = 'sessions/'


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('antenna_set',
                        help='Antenna set to use.',
                        choices=['LBA_INNER', 'LBA_OUTER', 'LBA_SPARSE_EVEN', 'HBA_ZERO', 'HBA_ONE', 'HBA_DUAL'])
    parser.add_argument('filter', help='Frequency filter to use',
                        choices=['10_90', '30_90', '110_190', '210_250', '10_70', '30_70', '170_230'])
    parser.add_argument('session_start_utc',
                        help='Session START date in UTC, using ISO date notation, e.g. \'2020-01-31 13:17:01\'.')
    parser.add_argument('session_end_utc',
                        help='Session END date in UTC, using ISO date notation, e.g. \'2020-01-31 13:17:01\'.')
    parser.add_argument('--name', help='Session name. If not provided it defaults to \'Holog-<ANTENNA_SET>-<SESSION_START>\'.')
    parser.add_argument('--input', help='Template notebook in which to interpolate values. Default: %r'% DEFAULT_TEMPLATE,
                        default=DEFAULT_TEMPLATE)
    parser.add_argument('--station-set', help='Stationset to include in the observations.',
                        choices=['superterp', 'core', 'remote', 'nl', 'int', 'all', 'none'],
                        default='all')
    parser.add_argument('--obs-delay-min', help='Minutes to wait between session start (swlevel down) and observation start.', default='15.0')
    parser.add_argument('--beam-start-interval-s', help='float indicating the number of seconds between the start of beamctl commands at a station. Default 0.032',
                        default='0.032')
    parser.add_argument('--exclude', help='Comma-separated list of stations to exclude, e.g. \'CS031,RS205,DE609\'')
    parser.add_argument('--include', help='Comma-separated list of stations to include in addition to those in the station-set, e.g. \'CS031,RS205,DE609\'')
    parser.add_argument('--output-name', '-o', help='Output notebook. Defaults to <session-name>.ipynb')
    parser.add_argument('--output-root', help='Directory under which output sub directories are created. Default: "./sessions"',
                        default=DEFAULT_ROOT)
    parser.add_argument('--output-subdir', help='Directory under OUTPUT-ROOT in which output files are created. Default: "<OUTPUT-ROOT>/<session-name>/')
    parser.add_argument('--num-subbands', help='Number of subbands to spread along the frequency range [Default: 12].', default='12')
    parser.add_argument('--force', help='Force overwriting pre-existing output file',
                        action='store_true')
    parser.add_argument('--holog-user',
                        help='Username at lcuhead that stores <session-name>.txt files in their ~/Holog directory. Default: %s' % getpass.getuser(),
                        default=getpass.getuser())
    options = parser.parse_args()
    if options.exclude:
        options.exclude = options.exclude.upper().split(',')
    else:
        options.exclude=[]
    if options.include:
        options.include = options.include.upper().split(',')
    else:
        options.include=[]

    if options.name is None:
        short_date_time = options.session_start_utc.replace('-', '').replace(':','').replace(' ','-')[0:13] # 2:13
        options.name = 'Holog-%s-%s' % (short_date_time, options.antenna_set)            #H
    if options.output_name is None:
        options.output_name = options.name+'.ipynb'
    if options.output_subdir is None:
        options.output_subdir = options.name
    output_dir = os.path.join(options.output_root, options.output_subdir)
    output_path = os.path.join(output_dir, options.output_name)
    if os.path.exists(output_path) and not options.force:
        print('ERROR: Output file %r already exists; use --force to force overwriting it.'
              % output_path,
              file=sys.stderr, flush=True)
        sys.exit(-1)
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    symlink_name = os.path.join(output_dir, 'lofar')
    if os.path.exists(symlink_name):
        os.remove(symlink_name)
    os.symlink(os.path.join(LOFAR_INSTALL_ROOT, 'lofar/'), symlink_name)

    with open(output_path, mode='w') as output:
        for line in open(options.input):
            if line:
                out_line = line.replace('%SESSION_NAME%', options.name)\
                               .replace('%SESSION_START_UTC%', options.session_start_utc)\
                               .replace('%SESSION_END_UTC%', options.session_end_utc)\
                               .replace('%ANTENNA_SET%', options.antenna_set)\
                               .replace('%RCU_FILTER%', options.filter)\
                               .replace('%OBS_DELAY_MIN%', options.obs_delay_min)\
                               .replace('%BEAM_START_INTERVAL_S%', options.beam_start_interval_s)\
                               .replace('%STATION_SET%', options.station_set)\
                               .replace('%NUM_SUBBANDS%', options.num_subbands)\
                               .replace('%EXCLUDE_LIST%', '%r' % (options.exclude,))\
                               .replace('%INCLUDE_LIST%', '%r' % (options.include,))\
                               .replace('%HOLOG_USER%', options.holog_user)\
                               .replace('%COMMAND_LINE%', '%r\\n",\n    "\\n",\n    "    %s' % (sys.argv, ' '.join(sys.argv)))
                output.write(out_line)
    print(options)
    print('')
    print('---------------------'+'-'*len(output_path))
    print('Notebook written to: %s' % output_path)
    print('---------------------'+'-'*len(output_path))
    print('Useful next steps:')
    print('')
    print('cd %s ' % output_dir)
    print('jupyter notebook %s' % options.output_name)
    print('')

