#!/usr/bin/env python3
import argparse
import sys
import os
from lofar.calibration.common.datacontainers.holography_dataset import HolographyDataset

from lofar.calibration.common.datacontainers.calibration_table import \
    read_calibration_tables_per_station_mode, CalibrationTable
import re
from lofar.calibration.processing.convert.calibration_table import compute_calibration_table
import logging
from typing import Union
from lofar.calibration.processing.decorators import log_step_execution

logger = logging.getLogger('holography_process')

def setup_argument_parser():
    """

    :return:
    """
    parser = argparse.ArgumentParser(
        description='Computes the gains per each antenna from the given Holography Dataset')
    parser.add_argument('input_path', help='Input holography dataset', type=str)
    parser.add_argument('output_path', help='Output holography dataset', type=str)
    parser.add_argument('--path_to_calibration_tables',
                        help='overrides the calibration tables specified in the dataset',
                        type=str, default=None)
    parser.add_argument('--comment', help='Add a comment in the calibration table file',
                        type=str, default='')
    parser.add_argument('--verbose', help='Set verbose logging', action='store_true')
    return parser


def parse_command_arguments(arguments):
    """

    :param arguments:
    :type arguments: list
    :return:
    """
    parser = setup_argument_parser()
    return parser.parse_args(arguments)

def convert_antennaset_for_caltable(antenna_set):
    if antenna_set[0:3] == "HBA":
       return "HBA"
    else:
       return antenna_set

def convert_filter_for_caltable(filter):
    if filter == "30_90":
       return "10_90"
    else:
       return filter

@log_step_execution('calibration table load', logger)
def load_calibration_table_in_path(path, station, mode):
    cal_tables = read_calibration_tables_per_station_mode(path)
    logger.debug('found calibration tables %s', cal_tables)
    try:
        cal_table = cal_tables[(station, mode)]
    except KeyError:
        logger.error('Cannot find the right calibration table in path %s', path)
        raise SystemExit()
    return cal_table


@log_step_execution('replace calibration table', logger)
def replace_calibration_table(dataset: HolographyDataset,
                              calibration_table: CalibrationTable):
    dataset.insert_calibration_table(calibration_table)
    return dataset


@log_step_execution('load', logger)
def loading(path):
    """

    :param path:
    :type path: str
    :return:
    :rtype: datacontainers.HolographyDataset
    """
    dataset = HolographyDataset.load_from_file(path)
    return dataset


def get_station_code_from_name(station_name):
    found = re.search(r'[A-Z]{2}([0-9]{3}).*', station_name).groups()
    if found:
        logger.info('station name %s - %s', station_name, found)
        return found[0]
    else:
        logger.error('unexpected station name %s', station_name)
        raise SystemExit()


@log_step_execution('save', logger)
def store_step(dataset: HolographyDataset, filepath):
    absolute_filepath = os.path.abspath(filepath)

    logger.info('Storing calibration tables in path %s', absolute_filepath)
    os.makedirs(absolute_filepath, exist_ok=True)

    for mode, calib_table in dataset.derived_calibration_tables.items():

        station_code = get_station_code_from_name(dataset.target_station_name)
        logger.debug('station code %s - mode %s', station_code, mode)
        
        antenna_set_name=convert_antennaset_for_caltable(dataset.antenna_set)
        filter_name=convert_filter_for_caltable(dataset.filter)
        calibration_tables_file_name = 'CalTable-%s-%s-%s.dat' % (station_code, antenna_set_name, filter_name)

        logger.debug('file name to save %s %s', absolute_filepath, calibration_tables_file_name)
        complete_path = os.path.join(absolute_filepath, calibration_tables_file_name)
        logging.debug('storing calibration file in %s', complete_path)
        calib_table.store_to_file(complete_path)


@log_step_execution('compute calibration table', logger)
def compute_cal_table(dataset: HolographyDataset, comment: str):
    compute_calibration_table(dataset, comment)
    logger.debug('stuff2 %s', dataset.derived_calibration_tables)


def prepare_dataset_for_processing(dataset: HolographyDataset,
                                   path_to_calibration_table: Union[str, None]):
    if path_to_calibration_table:
        station = dataset.target_station_name\
            .replace('HBA0', '')\
            .replace('HBA1', '')\
            .replace('HBA', '')
        mode = dataset.mode
        cal_table = load_calibration_table_in_path(path=path_to_calibration_table,
                                                   station=station,
                                                   mode=mode)
        dataset = replace_calibration_table(dataset=dataset, calibration_table=cal_table)

        logger.debug('stuff %s', dataset.calibration_tables)
        return dataset
    else:
        return dataset


def execute_processing(arguments):
    dataset = loading(arguments.input_path)
    dataset = prepare_dataset_for_processing(dataset, arguments.path_to_calibration_tables)

    compute_calibration_table(dataset, arguments.comment)

    store_step(dataset, arguments.output_path)


def setup_logger(arguments):
    format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'

    if arguments.verbose:
        logging.basicConfig(level=logging.DEBUG, format=format)
    else:
        logging.basicConfig(level=logging.INFO, format=format)


if __name__ == '__main__':
    arguments = parse_command_arguments(sys.argv[1:])
    setup_logger(arguments)
    execute_processing(arguments)

