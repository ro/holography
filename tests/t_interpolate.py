import logging
import unittest
from typing import Dict

import lofar.calibration.processing.interpolate as interpolate
import numpy
import matplotlib.pyplot as plt



def generate_signal(nu, slope, intercept, noise):
    signal_amplitude_slope = numpy.abs(slope)
    signal_phase_slope = numpy.angle(slope)

    noise_amplitude = numpy.abs(noise)

    signal = (signal_amplitude_slope * nu +
              abs(intercept)
              ) * numpy.exp((signal_phase_slope * nu + numpy.angle(intercept))*1.j) + \
             numpy.random.normal(0, noise_amplitude) +\
             1.j * numpy.random.normal(0, noise_amplitude)

    return signal


def generate_test_data(slope: complex,
                       intercept: complex,
                       noise_amplitude: complex,
                       n_antennas=40) -> Dict[str, Dict[str, Dict[str, numpy.ndarray]]]:
    """
    As input the routines expects the following structure in the holography dataset
    represented in nested dicts

    [station_name]| all
            + [frequency]
                _+ [polarization]
                    + gains
                    + [...]

    """

    station_set = {'ALL', 'CS001'}
    frequencies = {1., 2., 3., 4.}
    polarizations = ['XX', 'XY', 'YX', 'YY']

    data = lambda nu: numpy.zeros(n_antennas, dtype=numpy.complex128) + \
                      generate_signal(nu, slope, intercept, noise_amplitude)
    return {
        station: {
            str(frequency): {
                polarization: {
                    'gains': data(frequency),
                    'flag': numpy.array(False)
                } for polarization in polarizations
            } for frequency in frequencies
        } for station in station_set
    }


class TestInterpolateStep(unittest.TestCase):

    def test_mc_interpolation(self):
        numpy.random.seed(0)
        frequencies = numpy.linspace(0, 10, 15)
        slope = 10 * numpy.exp(.01j)
        intercept = 1. * numpy.exp(+.4j)
        noise_amplitude = .3

        signal = numpy.array([generate_signal(frequency, slope, intercept, noise_amplitude)
                              for frequency in frequencies])

        result = interpolate._interpolate_gains_mc(frequencies, signal)

        amplitude_slope, amplitude_intercept, amplitude_sigma = result['amplitude']['parameters'][0]
        phase_slope, phase_intercept, phase_sigma = result['phase']['parameters'][0]

        signal_amplitude = numpy.abs(signal)
        signal_phase = numpy.angle(signal)

        expected_residual_amplitude = signal_amplitude - abs(slope) * frequencies - abs(intercept)
        expected_residual_phase = signal_amplitude - numpy.angle(slope) * frequencies - numpy.angle(intercept)

        residual_amplitude = signal_amplitude - amplitude_slope * frequencies - amplitude_intercept
        residual_phase = signal_phase - phase_slope * frequencies - phase_intercept

        error_amplitude = 1 - numpy.std(residual_amplitude) / numpy.std(expected_residual_amplitude)
        error_phase = numpy.std(residual_phase) / numpy.std(expected_residual_phase)

        self.assertLessEqual(numpy.abs(error_amplitude), .05)
        self.assertLessEqual(numpy.abs(error_phase), .05)

    def test_interpolation(self):
        slope = 10 * numpy.exp(1.j)
        intercept = 1. * numpy.exp(-.4j)
        noise_amplitude = 1  # 3.*numpy.exp(.001j)
        test_data = generate_test_data(slope, intercept, noise_amplitude, n_antennas=4)
        result = interpolate.derive_interpolation_parameters(test_data)
        self.assertEqual(test_data.keys(), result.keys())

    def test_fourier_interpolation(self):
        numpy.random.seed(0)
        def compare_ratio(first, second, sigma):
            if first + second != 0:
                ratio = numpy.abs(first/(first + second)) - 1
                self.assertTrue(ratio < sigma, msg=f'{first} differs from {second} for more then {sigma} %')

        n_samples = 10
        frequencies = numpy.linspace(100.e6, 125.e6, n_samples)
        tau = 15.e-9 # 15 nano seconds
        m_amp = 1.7e-8
        q_amp = 2 - frequencies[0] * m_amp

        m_phs = tau
        q_phs = .2

        noise_amplitude = .05
        acceptance_sigma = noise_amplitude / numpy.sqrt(n_samples - 2)
        accepted_parameters_ratio = 0.1# The error is withing 1%

        noise = numpy.random.normal(0, noise_amplitude, n_samples) + 1.j * numpy.random.normal(0, noise_amplitude, n_samples)
        phases = frequencies * m_phs + q_phs
        amplitude = (frequencies * m_amp + q_amp)
        signal = amplitude * numpy.exp(1.j * 2 * numpy.pi * phases) + noise

        result = interpolate._interpolate_gains_fourier(frequencies, signal)
        interpolated_values_abs = result['amplitude']['parameters']['m'][0] * frequencies +\
                                  result['amplitude']['parameters']['q'][0]

        interpolated_values_phases = result['phase']['parameters']['m'][0] * frequencies +\
                                     result['phase']['parameters']['q'][0]

        interpolated_values = interpolated_values_abs * numpy.exp(1.j * 2 * numpy.pi * interpolated_values_phases)

        compare_ratio(result['amplitude']['parameters']['m'][0], m_amp, accepted_parameters_ratio)
        compare_ratio(result['amplitude']['parameters']['q'][0], q_amp, 50 * accepted_parameters_ratio)
        compare_ratio(result['phase']['parameters']['m'][0], m_phs, accepted_parameters_ratio)
        compare_ratio(numpy.exp(1.j * (result['phase']['parameters']['q'][0] - q_phs)), 0, accepted_parameters_ratio)

        residual_noise = numpy.sum(numpy.abs(interpolated_values - signal))
        self.assertTrue(residual_noise < 4 * acceptance_sigma * n_samples,
                        msg='the fitted values differ from more the 4 sigma with the noise')




if __name__ == '__main__':
    logging.basicConfig(format='%(name)s : %(message)s')
    unittest.main()
