import logging
import unittest

import numpy
from astropy.coordinates import SkyCoord
from astropy.time import Time

from lofar.calibration.common.datacontainers.holography_measurementset import \
    _compute_lm_from_ra_dec_station_position_rotation_matrix_and_time

logger = logging.getLogger('t_holography_ms_class')


class TestHolographyDatasetClass(unittest.TestCase):
    def test_lm_conversion(self):
        sample_time = Time('2015-06-19 13:50:00').mjd
        sample_time *= 24 * 60 * 60
        target_3c196 = SkyCoord('08h13m36.0561s', '+48d13m02.636s', frame='icrs')

        coordinate_type = numpy.dtype([('RA', numpy.float64),
                                       ('DEC', numpy.float64),
                                       ('EPOCH', 'S10')])

        target_icrs_rad = numpy.array(
            (target_3c196.icrs.ra.rad, target_3c196.icrs.dec.rad, 'J2000'),
            dtype=coordinate_type)
        core_pqr_itrs_mat = numpy.array([[-1.19595000e-01, -7.91954000e-01, 5.98753000e-01],
                                         [9.92823000e-01, -9.54190000e-02, 7.20990000e-02],
                                         [3.30000000e-05, 6.03078000e-01, 7.97682000e-01]],
                                        dtype=numpy.float64)
        result = _compute_lm_from_ra_dec_station_position_rotation_matrix_and_time(
            target_icrs_rad, core_pqr_itrs_mat, [sample_time, sample_time])

        return_value_dtype = [('l', numpy.float64),
                              ('m', numpy.float64)]
        expected_result = numpy.zeros((2), dtype=return_value_dtype)
        expected_result['l'] = 0.2874042
        expected_result['m'] = 0.698615

        for field in expected_result.dtype.names:
            self.assertIn(field, result.dtype.names)
            numpy.testing.assert_almost_equal(result[field],
                                              expected_result[field],
                                              err_msg='%s mismatch' % field)


if __name__ == '__main__':
    logging.basicConfig(format='%(name)s : %(message)s')
    unittest.main()
