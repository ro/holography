import unittest
from lofar.calibration.common.datacontainers.calibration_table import CalibrationTable,\
    InvalidFileException, read_calibration_tables_in_directory, read_calibration_tables_per_station_mode, read_calibration_tables_per_station_antennaset_band
from h5py import File as H5File
from os import getcwd
import logging
from tempfile import NamedTemporaryFile
from numpy.testing import assert_array_equal
from numpy import array, linspace


CALIBRATION_TABLE_FILENAME = 'tests/t_calibration_table.in_CalTable-401-HBA-110_190.dat'


class TestCalibrationTable(unittest.TestCase):
    def test_loading(self):
        calibration_table = CalibrationTable.load_from_file(CALIBRATION_TABLE_FILENAME)
        self.assertEqual(calibration_table.observation_station, 'CS401')

    def test_unvalid_file(self):
        with NamedTemporaryFile('w+b') as temp_file:
            temp_file.write(b'stuff')
            temp_file.flush()

            with self.assertRaises(InvalidFileException):
                _ = CalibrationTable.load_from_file(temp_file.name)

    def test_storing_file(self):
        with NamedTemporaryFile('w+b') as temp_file:
            test_calibration_table = CalibrationTable.load_from_file(CALIBRATION_TABLE_FILENAME)

            test_calibration_table.store_to_file(temp_file.name)

            stored_calibration_table = CalibrationTable.load_from_file(temp_file.name)
            self.assertEqual(test_calibration_table,
                             stored_calibration_table)

    def test_storing_to_hdf(self):
        with NamedTemporaryFile('w+b') as temp_file:
            h5_file = H5File(temp_file.name, 'w')
            test_calibration_table = CalibrationTable.load_from_file(CALIBRATION_TABLE_FILENAME)

            test_calibration_table.store_to_hdf(h5_file, '/calibration_table')
            h5_file.close()
            h5_file = H5File(temp_file.name, 'r')
            self.assertEqual(
                h5_file['/calibration_table'].attrs['observation_station'],
                'CS401'
            )
            assert_array_equal(array(h5_file['/calibration_table']), test_calibration_table.data)

    def test_loading_to_hdf(self):
        with NamedTemporaryFile('w+b') as temp_file:
            h5_file = H5File(temp_file.name, 'w')
            test_calibration_table = CalibrationTable.load_from_file(CALIBRATION_TABLE_FILENAME)

            test_calibration_table.store_to_hdf(h5_file, '/calibration_table')
            h5_file.close()
            h5_file = H5File(temp_file.name, 'r')
            caltable = CalibrationTable.load_from_hdf(file_descriptor=h5_file,
                                                      uri='/calibration_table')
            self.assertEqual(caltable, test_calibration_table)

    def test_loading_to_hdf_raise_value_error(self):
        with NamedTemporaryFile('w+b') as temp_file:
            h5_file = H5File(temp_file.name, 'w')
            h5_file.close()
            h5_file = H5File(temp_file.name, 'r')
            with self.assertRaises(ValueError):
                caltable = CalibrationTable.load_from_hdf(file_descriptor=h5_file,
                                                      uri='/calibration_table')

    def test_frequency_generation(self):
        test_calibration_table = CalibrationTable.load_from_file(CALIBRATION_TABLE_FILENAME)
        sb = linspace(1, 512, 512)
        clock = 200
        nyq = 2

        expected_frequency_array = freq = clock / 1024 * sb + (nyq-1) * clock / 2.
        assert_array_equal(expected_frequency_array, test_calibration_table.frequencies())

    def test_list_calibration_tables_in_path(self):
        calibration_tables = read_calibration_tables_in_directory(getcwd())
        self.assertEqual(len(calibration_tables), 1)
        expected_calibration_table = CalibrationTable.load_from_file(CALIBRATION_TABLE_FILENAME)
        obtained_calibration_table = calibration_tables[0]
        self.assertEqual(expected_calibration_table, obtained_calibration_table)

    def test_read_calibration_tables_per_station_mode(self):
        calibration_tables_dict = read_calibration_tables_per_station_mode(getcwd())
        self.assertIn(('CS401', 5), calibration_tables_dict.keys())

    def test_read_calibration_tables_per_station_antennaset_band(self):
        calibration_tables_dict = read_calibration_tables_per_station_antennaset_band(getcwd())
        self.assertIn(('CS401', 'HBA', '110_190'), calibration_tables_dict.keys())

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    unittest.main()
