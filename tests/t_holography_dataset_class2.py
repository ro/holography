import logging
import unittest
import tempfile
import os
from lofar.calibration.common.datacontainers import HolographyDataset
from lofar.calibration.common.datacontainers.holography_specification import list_bsf_files_in_path
from lofar.calibration.common.datacontainers.holography_observation import list_observations_in_path


logger = logging.getLogger('t_holography_dataset_class')
# READ doc/Holography_Data_Set.md!  It contains the location from which the
# test data must be downloaded.
path_to_test_data = '/var/tmp/holography'


def _is_test_data_missing():
    return not os.path.exists(path_to_test_data)


class TestHolographyDatasetClass(unittest.TestCase):
    @unittest.skipIf(_is_test_data_missing(), 'test data is missing please check documentation')
    def test_create_hdf5_and_read_it_in(self):
        '''
        Reads a Measurement Set from a file and converts it to an HDS.
        Then the HDS is stored as an HDF5 file while the HDS data in still kept
        in memory.  At last the HDF5 file is read back into memory the HDS in
        the file is compared with the HDS in memory.
        ''' 
        # Read the MS into memory.
        holist = list_observations_in_path(path_to_test_data)
        hbsflist = list_bsf_files_in_path(path_to_test_data)
        for hbsf in hbsflist:
            hbsf.read_file()
        ho_per_ms = [(hbsf, ho) for hbsf, ho in zip(hbsflist, holist)]
  
        # Now create the Holography Data Set in memory from the MS data.
        holography_dataset_1 = HolographyDataset()
        holography_dataset_1.load_from_beam_specification_and_ms(
            'CS013HBA0', ho_per_ms)

        # Store the HDS in a temporary HDF5 file.
        hdf5_data_file = None
        try:
            # Create a temporary file and...
            (fd, hdf5_data_file) = tempfile.mkstemp(suffix = ".hdf5")
            # close it immediately.  Python documentation states that the file
            # gets created and is opened automatically.
            os.close(fd)

            # Store the HDS in memory to disk.
            holography_dataset_1.store_to_file(hdf5_data_file)

            # Read the HDS data from the HDF5 file back into memory.
            holography_dataset_2 = HolographyDataset.load_from_file(
                hdf5_data_file)
        
            # And make sure that both HDSes contain the same information.
            self.assertTrue(holography_dataset_1 == holography_dataset_2)
        finally:
            # If the temp file was created, remove it.
            if hdf5_data_file is not None:
                os.unlink(hdf5_data_file)


if __name__ == '__main__':
    logging.basicConfig(format='%(name)s : %(message)s')
    unittest.main()
