import logging
import os
import unittest

import lofar.calibration.processing.averaging as averaging
import lofar.calibration.processing.solver as solver
import lofar.calibration.processing.test.utils as utils
import mock
from lofar.calibration.common.datacontainers import HolographyDataset
from lofar.calibration.processing import average_dataset_by_sample, average_values_by_sample
from lofar.calibration.processing.test.utils import create_holography_dataset

logger = logging.getLogger('t_processing')
import numpy

# READ doc/Holography_Data_Set.md!  It contains the location from which the
# test data must be downloaded and this file generated.
SAMPLE_DATASET = '/data/test/HolographyObservation/CS001HBA0.hdf5'


def is_sample_dataset_not_available():
    return not os.path.exists(SAMPLE_DATASET)


class TestSignalGenerator(unittest.TestCase):
    def test_gaussian_noise_generation_produce_expected_results(self):
        test_average_value = 5. + 12j
        test_real_std = 12
        test_imag_std = 3
        test_sample_size = 100
        gaussian_generator = utils.TestComplexGaussianNoise(test_average_value,
                                                            test_real_std,
                                                            test_imag_std)
        sample_noise = gaussian_generator.generate(test_sample_size, seed=5)
        self.assertGreater(numpy.std(sample_noise.imag) / test_imag_std, .9)
        self.assertGreater(numpy.std(sample_noise.real) / test_real_std, .9)
        self.assertGreater(abs(numpy.average(sample_noise).real / test_average_value.real), .9)
        self.assertGreater(abs(numpy.average(sample_noise).imag / test_average_value.imag), .9)

    def test_signal_generator_produce_expected_linear_trend_with_expected_noise(self):
        test_imag_coeff = 3.j
        test_real_coeff = 2.
        expected_dependencies_imag_real = test_imag_coeff / test_real_coeff
        linear_function = lambda x: 2 * x + 3.j * x

        test_average_value = 5. + 12j
        test_real_std = 12
        test_imag_std = 3
        test_time_samples = numpy.linspace(0, 100, 1000).tolist()
        gaussian_generator = utils.TestComplexGaussianNoise(test_average_value,
                                                            test_real_std,
                                                            test_imag_std)

        signal_generator = utils.TestSignal(linear_function, gaussian_generator)
        sample_signal = signal_generator.generate(test_time_samples)

        coefficients = numpy.polyfit(sample_signal.real, sample_signal.imag, 1)
        m, q = coefficients[0], coefficients[1]
        self.assertGreater(abs(m / expected_dependencies_imag_real), .9)


class TestProcessing(unittest.TestCase):
    sample_dataset = None

    def setUp(self):
        self.sample_dataset = create_holography_dataset()

    def test_if_sample_data_has_correct_metadata(self):
        for reference_station in self.sample_dataset.reference_stations():
            self.assertIn(reference_station, self.sample_dataset.data)

    def test_average_array_by_sample_produces_right_average_for_real_values(self):
        """
        Test if specifying the number of sample the averaging produces meaningful results
        """
        test_data = numpy.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], dtype=float)
        expected_result_mean = numpy.array([1.5, 3.5, 5.5, 7.5, 9.5], dtype=float)
        expected_result_std = numpy.array([0.5, 0.5, 0.5, 0.5, 0.5], dtype=float)

        result = average_values_by_sample(test_data, 2)

        numpy.testing.assert_almost_equal(expected_result_mean, result['mean'])
        numpy.testing.assert_almost_equal(expected_result_std, result['std'])

    def test_average_array_by_sample_produces_right_average_for_complex_values(self):
        """
        Test if specifying the number of sample the averaging produces meaningful results
        """
        test_data = numpy.array([1 + 1.j, 2 + 2.j, 3 + 3.j, 4 + 4.j, 5 + 5.j,
                                 6 + 6.j, 7 + 7.j, 8 + 8.j, 9 + 9.j, 10 + 10.j],
                                dtype=complex)
        expected_result_mean = numpy.array([1.5 + 1.5j, 3.5 + 3.5j,
                                            5.5 + 5.5j, 7.5 + 7.5j, 9.5 + 9.5j],
                                           dtype=complex)
        expected_result_std = numpy.array([0.5 + 0.5j,
                                           0.5 + 0.5j,
                                           0.5 + 0.5j,
                                           0.5 + 0.5j,
                                           0.5 + 0.5j], dtype=complex)

        result = average_values_by_sample(test_data, 2)
        numpy.testing.assert_almost_equal(result['mean'], expected_result_mean)
        numpy.testing.assert_almost_equal(result['std_real'], expected_result_std.real)
        numpy.testing.assert_almost_equal(result['std_imag'], expected_result_std.real)

    @unittest.skipIf(is_sample_dataset_not_available(), 'missing test dataset')
    def test_average_datatable_by_sample_respect_structure_of_input_data(self):
        holography_dataset = HolographyDataset.load_from_file(SAMPLE_DATASET)
        averaged_data = average_dataset_by_sample(SAMPLE_DATASET, 2).derived_data[
            'PRE_NORMALIZATION_AVERAGE_TIME']
        self._assert_recursive_dict_have_the_same_keys(holography_dataset.data, averaged_data)

    def _assert_recursive_dict_have_the_same_keys(self, expected, actual):
        '''
        This function compares the keys of two dicts.  It recursively
        iterates over all keys and embedded dicts.
        @return: False if a key in dict1 is not a key in dict2 or if values for
        a key differ in the dicts, True if the contents of both dicts match.
        @param expected:  A dict
        @param actual:  Another dict
        '''
        result = True
        for key in expected.keys():
            if key in actual.keys():
                if isinstance(expected[key], dict) and isinstance(actual[key], dict):
                    result = result and self._assert_recursive_dict_have_the_same_keys(expected[key],
                                                                                       actual[key])
                else:
                    pass
            else:
                self.assertTrue(False, 'Expected key {} is missing in the actual dict'.format(key))

    def test_coordinate_rotation_performed_correctly(self):
        test_dataset = self.setup_coordinate_rotation_performed_correctly()
        result = solver._rotate_antenna_coordinates(test_dataset)
        numpy.testing.assert_equal(result, -test_dataset.antenna_field_position())

    def setup_coordinate_rotation_performed_correctly(self):
        test_dataset = utils.create_holography_dataset()
        test_dataset._target_station_position = numpy.zeros(3)
        test_dataset._rotation_matrix = numpy.array([
            [1, 0, 0],
            [0, 1, 0],
            [0, 0, 1]])
        return test_dataset

    def test_if_pointing_matrix_generation_is_correct(self):

        test_dataset, test_datatable, central_beam, freq, expected_matrix = \
            self.setup_test_if_pointing_matrix_generation_is_correct()
        matrix = solver._compute_pointing_matrices_per_station_frequency_beam(test_dataset,
                                                                              test_datatable,
                                                                              central_beam,
                                                                              freq)


        numpy.testing.assert_array_almost_equal(matrix.real, expected_matrix)

    def setup_test_if_pointing_matrix_generation_is_correct(self):
        test_dataset = utils.create_holography_dataset()
        test_dataset._rotation_matrix = numpy.identity(3)
        test_dataset._antenna_field_position = numpy.array([[1, 0, 1],
                                                            [-1, 0, 1],
                                                            [0, 0, 1]])
        test_datatable = dict()

        for i in range(3):
            test_data_per_beam = utils.generate_single_pointing_dataset([1], dict(
                average=1. + 1.j,
                std_imag=0,
                std_real=0
            ))

            test_data_per_beam['l'][0] = .5 if i != 1 else 0
            test_data_per_beam['m'][0] = .5 if i != 1 else 0

            test_datatable[str(i)] = dict(mean=test_data_per_beam)

        freq = 1. / solver._FRQ_TO_PHASE  # Trick to get one of the phase shift
        central_beam = '1'
        # The rows will be equal to the exp(j * [x*l + y*m]) / n_antennas

        row1 = numpy.cos(1 * .5) / test_dataset.antenna_field_position().shape[0]
        row2 = numpy.cos(-1 * .5) / test_dataset.antenna_field_position().shape[0]
        row3 = numpy.cos(0) / test_dataset.antenna_field_position().shape[0]

        expected_matrix = numpy.array([
            [row1, row2, row3],
            [row3, row3, row3],
            [row1, row2, row3]
        ])
        return test_dataset, test_datatable, central_beam, freq, expected_matrix

    def test_if_matrix_solution_is_performed_correctly(self):

        test_dataset, test_datatable, freq, matrix, expected_gains =\
            self.setup_if_matrix_solution_is_performed_correctly()

        result = solver.solve_gains_per_datatable(dataset=test_dataset, datatable=test_datatable)
        gains = numpy.array(result['all'][str(freq)]['XX']['gains'])
        numpy.testing.assert_array_almost_equal(gains, expected_gains)

    @unittest.skip('still unstable')
    def test_if_matrix_solution_is_performed_correctly_with_mcmc(self):

        test_dataset, test_datatable, freq, matrix, expected_gains =\
            self.setup_if_matrix_solution_is_performed_correctly()

        result = solver.solve_gains_per_datatable(dataset=test_dataset, datatable=test_datatable, type='MCMC')
        gains = numpy.array(result['all'][str(freq)]['XX']['gains'])
        numpy.testing.assert_allclose(gains, expected_gains, rtol=1.e-3)

    def test_if_matrix_solution_is_performed_correctly_with_weightedlstsqr(self):

        test_dataset, test_datatable, freq, matrix, expected_gains =\
            self.setup_if_matrix_solution_is_performed_correctly()

        result = solver.solve_gains_per_datatable(dataset=test_dataset, datatable=test_datatable, type='WEIGHTED_LSTSQR')
        gains = numpy.array(result['all'][str(freq)]['XX']['gains'])
        numpy.testing.assert_allclose(gains, expected_gains, rtol=1.e-3)


    def setup_if_matrix_solution_is_performed_correctly(self):
        test_dataset = utils.create_holography_dataset()
        test_dataset._rotation_matrix = numpy.identity(3)
        test_dataset._antenna_field_position = numpy.array([[1, 1, 1],
                                                           [-1, 0, 1],
                                                           [5, 0, 1],
                                                           [-2, -1, 1]])
        test_datatable = dict()
        test_dataset._beamlets = []

        for i in range(8):
            test_data_per_beam = utils.generate_single_pointing_dataset([1], dict(
                average=1. + 1.j,
                std_imag=0,
                std_real=0
            ))
            test_dataset._beamlets.append(str(i))
            test_data_per_beam['l'][0] = +.5 * float(i) - 1. if i != 1 else 0
            test_data_per_beam['m'][0] = +.5 * float(i) - 1. if i != 1 else 0
            test_std_per_beam = utils.generate_single_pointing_dataset([1], dict(
                average=1. + 1.j,
                std_imag=0,
                std_real=0
            ))

            test_datatable[str(i)] = dict(mean=test_data_per_beam, std=test_std_per_beam)

        freq = 1. / solver._FRQ_TO_PHASE  # Trick to get one of the phase shift
        central_beamlet = '1'
        test_dataset._central_beamlets[str(freq)] = central_beamlet
        matrix = solver._compute_pointing_matrices_per_station_frequency_beam(test_dataset,
                                                                              test_datatable,
                                                                              central_beamlet,
                                                                              freq)

        n_antennas = len(test_dataset.antenna_field_position())
        expected_gains = numpy.array(utils.TestComplexGaussianNoise(average=1,
                                                                    std_real=.2,
                                                                    std_imag=.2).
                                      generate(n_antennas, seed=0))

        expected_visibilities = matrix @ expected_gains

        for i, expected_visibility in enumerate(expected_visibilities):
            test_datatable[str(i)]['mean']['XX'][0] = expected_visibility
            test_datatable[str(i)]['mean']['XY'][0] = expected_visibility
            test_datatable[str(i)]['mean']['YX'][0] = expected_visibility
            test_datatable[str(i)]['mean']['YY'][0] = expected_visibility
            for pol in ['XX', 'YY', 'XY', 'YX']:
                re, im = numpy.random.uniform(.01, .2, size=2)
                test_datatable[str(i)]['std'][pol][0] = re + 1.j * im

        test_datatable = dict(all={str(freq): test_datatable})

        return test_dataset, test_datatable, freq, matrix, expected_gains

    def test_if_the_time_averaging_is_performed_correctly(self):
        test_dataset, timesamples = self.setup_if_the_time_averaging_is_performed_correctly()

        out_datatable = averaging.average_data_by_sample(test_dataset.data, 2)

        for station in test_dataset.data:
            self.assertIn(station, out_datatable.keys())
            for frequency in test_dataset.data[station]:
                self.assertIn(frequency, out_datatable[station].keys())
                for beam in test_dataset.data[station][frequency]:
                    self.assertIn(beam, out_datatable[station][frequency].keys())
                    self.assertEqual(out_datatable[station][frequency][beam]['mean'].shape[0],
                                     len(timesamples) / 2)
        out_datatable = averaging.average_data_by_sample(test_dataset.data, 3)

        for station in out_datatable:
            for frequency in out_datatable[station]:
                for beam in out_datatable[station][frequency]:
                    self.assertEqual(out_datatable[station][frequency][beam]['mean'].shape[0],
                                     len(timesamples) // 3 + 1)

    def setup_if_the_time_averaging_is_performed_correctly(self):
        test_dataset = utils.create_holography_dataset()
        timesamples = [0, 1, 2, 3, 4, 5, 6, 7]
        utils.generate_sample_data(test_dataset, timesamples)
        return test_dataset, timesamples


if __name__ == '__main__':
    logging.basicConfig(format='%(name)s : %(message)s')
    unittest.main()
