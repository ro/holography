import logging
import unittest
import os
from lofar.calibration.common.datacontainers.holography_observation import list_observations_in_path

logger = logging.getLogger('t_holography_dataset_class')
path_to_test_data = '/var/tmp/holography'


def is_data_not_available():
    return not os.path.exists(path_to_test_data)


class TestHolographyDatasetClass(unittest.TestCase):
    @unittest.skipIf(is_data_not_available(), 'test data not available')
    def test_holography_observation_path_listing(self):
        observation_list = list_observations_in_path(path_to_test_data)

        ## Assert correct reading of the files
        HolographyObservation_freq_one = observation_list.pop()
        self.assertEqual(HolographyObservation_freq_one.sas_id, "661022")
        self.assertEqual(HolographyObservation_freq_one.source_name, '3C 147')
        self.assertEqual(HolographyObservation_freq_one.sub_band, 76)
        self.assertEqual(HolographyObservation_freq_one.start_mjd, 5038710120.0)
        self.assertEqual(HolographyObservation_freq_one.end_mjd, 5038710149.540695)
        self.assertAlmostEqual((HolographyObservation_freq_one.end_datetime -
                                HolographyObservation_freq_one.start_datetime).seconds,
                               29)


if __name__ == '__main__':
    unittest.main()
