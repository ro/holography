from lofarobsxml import BackendProcessing, Observation, Folder, Beam, TargetSource
from lofarobsxml.pipelines import NDPPP, AveragingPipeline
from lofarobsxml import simbad, SourceCatalogue, Folder, xml
from lofarobsxml.utilities import station_list, sort_station_list
from astropy.time import Time
from astropy.coordinates import SkyCoord
import astropy.units as u
from .stations import beamctl_setup, beamctl_as_text
from .grids import penrose
import datetime
import numpy as np
import subprocess
import os



def fieldname_from_lcu(lcu_or_station, mode='HBA'):
    r'''
    **Examples**
    >>> fieldname_from_lcu('cs004c')
    'CS004HBA'
    >>> fieldname_from_lcu('LV614', mode='LBA')
    'LV614LBA'
    >>> fieldname_from_lcu('cs002lba', mode='LBA')
    Traceback (most recent call last):
    ...
    ValueError: 'cs002lba' is not a valid LOFAR LCU or station name such as 'LV614C' or 'CS002'

    '''
    if (len(lcu_or_station) == 6 and lcu_or_station.upper()[-1] != 'C') or len(lcu_or_station) < 5 or len(lcu_or_station) > 6 or not lcu_or_station[2:5].isdigit():
        raise ValueError('%r is not a valid LOFAR LCU or station name such as \'LV614C\' or \'CS002\'' % lcu_or_station)
    return lcu_or_station.upper().rstrip('C')+mode.upper()



def scp_to_pwd(path, hostname):
    print('Retrieving %s from %s...' % (path, hostname))
    process_result = subprocess.run(['scp', hostname+':'+path, '.'])
    return process_result.check_returncode()
    


def broken_rcus(antenna_set,
                path='/globaldata/array_status/STATIONS/DATA/broken.p',
                hostname='lcuhead'):
    r'''
    TODO: Still HBA specific 

    **Examples**

    >>> broken_rcus('HBA_DUAL', path='tests/broken.p', hostname=None)
    {'LV614HBA': [46, 47, 58, 59, 190, 191], 'RS205HBA': [], 'RS208HBA': [60, 61], 'CS021HBA': [], 'PL610HBA': [120, 121, 138, 139], 'UK608HBA': [40, 41, 146, 147], 'RS106HBA': [60, 61], 'RS406HBA': [], 'DE601HBA': [36, 37, 188, 189], 'DE602HBA': [166, 167], 'CS017HBA': [66, 67], 'DE604HBA': [], 'CS011HBA': [28, 29], 'CS013HBA': [], 'DE609HBA': [], 'RS305HBA': [], 'RS307HBA': [], 'RS306HBA': [36, 37, 64, 65, 74, 75], 'PL611HBA': [0, 1, 6, 7, 20, 21, 66, 67], 'IE613HBA': [84, 85, 160, 161], 'RS508HBA': [], 'CS032HBA': [], 'CS030HBA': [2, 3, 50, 51, 54, 55, 68, 69, 78, 79, 88, 89], 'CS031HBA': [16, 17], 'RS409HBA': [26, 27, 38, 39, 76, 77], 'FR606HBA': [20, 21, 138, 139, 144, 145], 'RS509HBA': [84, 85], 'RS210HBA': [], 'CS501HBA': [24, 25, 30, 31], 'RS503HBA': [24, 25, 60, 61, 70, 71, 82, 83], 'CS401HBA': [], 'SE607HBA': [], 'CS103HBA': [42, 43], 'PL612HBA': [124, 125, 188, 189], 'CS101HBA': [], 'RS310HBA': [74, 75, 88, 89], 'CS301HBA': [46, 47], 'CS302HBA': [], 'CS028HBA': [], 'RS407HBA': [], 'CS003HBA': [42, 43], 'CS024HBA': [48, 49, 54, 55, 80, 81], 'CS001HBA': [], 'CS026HBA': [88, 89, 94, 95], 'CS007HBA': [42, 43, 58, 59], 'CS006HBA': [70, 71], 'CS005HBA': [38, 39, 40, 41], 'CS004HBA': [58, 59], 'DE603HBA': [52, 53], 'CS002HBA': [], 'CS201HBA': [], 'DE605HBA': []}
    >>> broken_rcus('LBA_OUTER', path='tests/broken.p', hostname=None) # doctest: +SKIP
    '''
    import pickle
    lba_or_hba = antenna_set.upper()[0:3]
    if hostname is None:
        local_filename = path
    else:
        local_filename = os.path.basename(path)
        scp_to_pwd(path, hostname)
    with open(local_filename, 'rb') as broken_f:
        broken = pickle.load(broken_f)
    by_field = dict()
    for st in broken.keys(): # TODO: The HBA specific part
        by_field[fieldname_from_lcu(st, mode=lba_or_hba)] = sorted([2*tile for tile in broken[st]['tiles']] \
                                                             + [2*tile+1 for tile in broken[st]['tiles']])
    return by_field



def beyond_repair_rcu(antenna_set,
                      path='/home/log/bad_antenna_list.txt',
                      hostname='lcuhead'):
    r'''
    **Examples**

    >>> beyond_repair_rcu('HBA_DUAL', path='tests/bad_antenna_list.txt', hostname=None)
    {'CS001HBA': [], 'CS002HBA': [], 'CS003HBA': [], 'CS004HBA': [], 'CS005HBA': [], 'CS006HBA': [], 'CS007HBA': [], 'CS011HBA': [], 'CS013HBA': [], 'CS017HBA': [], 'CS021HBA': [], 'CS024HBA': [], 'CS026HBA': [], 'CS028HBA': [], 'CS030HBA': [54, 55, 68, 69, 78, 79, 88, 89], 'CS031HBA': [], 'CS032HBA': [], 'CS101HBA': [], 'CS103HBA': [], 'CS201HBA': [], 'CS301HBA': [], 'CS302HBA': [], 'CS401HBA': [], 'CS501HBA': [], 'RS106HBA': [], 'RS205HBA': [], 'RS208HBA': [], 'RS210HBA': [], 'RS305HBA': [], 'RS306HBA': [], 'RS307HBA': [], 'RS310HBA': [88, 89], 'RS406HBA': [], 'RS407HBA': [], 'RS409HBA': [], 'RS503HBA': [], 'RS508HBA': [], 'RS509HBA': [], 'DE601HBA': [], 'DE602HBA': [], 'DE603HBA': [], 'DE604HBA': [], 'DE605HBA': [], 'FR606HBA': [], 'SE607HBA': [], 'UK608HBA': [], 'DE609HBA': [], 'PL610HBA': [], 'PL611HBA': [], 'PL612HBA': [], 'IE613HBA': [], 'LV614HBA': []}
    >>> beyond_repair_rcu('LBA_SPARSE_EVEN', path='tests/bad_antenna_list.txt', hostname=None)
    {'CS001LBA': [168, 169, 178, 179], 'CS002LBA': [], 'CS003LBA': [], 'CS004LBA': [], 'CS005LBA': [172, 173, 142, 143, 174, 175, 190, 191], 'CS006LBA': [70, 71, 174, 175], 'CS007LBA': [2, 3], 'CS011LBA': [], 'CS013LBA': [], 'CS017LBA': [], 'CS021LBA': [54, 55, 92, 93, 118, 119, 144, 145, 156, 157], 'CS024LBA': [176, 177], 'CS026LBA': [], 'CS028LBA': [], 'CS030LBA': [], 'CS031LBA': [], 'CS032LBA': [114, 115, 182, 183], 'CS101LBA': [], 'CS103LBA': [76, 77, 170, 171], 'CS201LBA': [], 'CS301LBA': [6, 7], 'CS302LBA': [], 'CS401LBA': [], 'CS501LBA': [], 'RS106LBA': [], 'RS205LBA': [], 'RS208LBA': [], 'RS210LBA': [166, 167, 168, 169, 188, 189, 190, 191], 'RS305LBA': [], 'RS306LBA': [], 'RS307LBA': [], 'RS310LBA': [], 'RS406LBA': [], 'RS407LBA': [], 'RS409LBA': [], 'RS503LBA': [], 'RS508LBA': [114, 115], 'RS509LBA': [], 'DE601LBA': [], 'DE602LBA': [], 'DE603LBA': [], 'DE604LBA': [], 'DE605LBA': [166, 167], 'FR606LBA': [], 'SE607LBA': [], 'UK608LBA': [], 'DE609LBA': [], 'PL610LBA': [], 'PL611LBA': [], 'PL612LBA': [], 'IE613LBA': [], 'LV614LBA': []}
    '''
    lba_or_hba = antenna_set.upper()[0:3]
    if hostname is None:
        local_filename = path
    else:
        local_filename = os.path.basename(path)
        scp_to_pwd(path, hostname)
    by_lcu = dict()
    
    with open(local_filename,'r') as bad_ant_file:
        bad_ant_lines = bad_ant_file.readlines()
    bad_ant_stations = [l.split()[0] for l in bad_ant_lines if l[0] != '#'] 
    bad_ant=[l.split()[1:] for l in bad_ant_lines if l[0] != '#']

    for lcu, ant in zip(bad_ant_stations, bad_ant):
        by_lcu[lcu] = [a for a in ant if lba_or_hba in a]

    by_rcu = dict()
    for k,v in by_lcu.items():
        by_rcu[fieldname_from_lcu(k, lba_or_hba)]=[]
        for antenna in v:
            by_rcu[fieldname_from_lcu(k, lba_or_hba)].extend([2*int(antenna[3:]),2*int(antenna[3:])+1])
    return by_rcu



def make_bad_antenna_dict(antenna_set,
                          disable_only_beyond_repair=True,
                          bad_antenna_list_path='/home/log/bad_antenna_list.txt',
                          broken_p_path='/globaldata/array_status/STATIONS/DATA/broken.p',
                          hostname='lcuhead'):
    broken_rcu_dict = broken_rcus(antenna_set,
                                  path=broken_p_path,
                                  hostname=hostname)
    beyond_repair_rcu_dict = beyond_repair_rcu(antenna_set,
                                               path=bad_antenna_list_path,
                                               hostname=hostname)
    for k,v in beyond_repair_rcu_dict.items():
        for rcu in v:
            if rcu not in broken_rcu_dict[k]:
                print("WARNING, beyond repair antenna not reported as broken. Please contact the operator",
                      k,beyond_repair_rcu_dict[k], broken_rcu_dict[k])
                broken_rcu_dict[k].append(rcu)

    if disable_only_beyond_repair:
        return beyond_repair_rcu_dict
    else:
        return broken_rcu_dict


def cal_source_catalogue(hba_source_names=None,
                         lba_source_names=None):
    r'''
    Create a lofarobsxml.SourceCatalogue based on favourite holography
    sources. Defaults:
        hba_source_names = ['3C 196', '3C 48', '3C 147', '3C 286']
        lba_source_names = ['Cyg A', 'Tau A', '3C 196', '3C 295', '3C 380']

    **Parameters**
    
    hba_source_names : [string] or None:
        List of allowed HBA target sources. If None: default.

    lba_source_names : [string] or None:
        List of allowed LBA target sources. If None: default.

    **Returns**

    lofarobsxml.SourceCatalogue


    **Examples**

    >>> cat=cal_source_catalogue(lba_source_names=['M31']) # highly inappropriate though
    >>> cat.source_table
    {'HBA': [[['3C 196'], (8, 13, 36.05608999999788), ('+', 48, 13, 2.635970000004022)], [['3C 48'], (1, 37, 41.2995845985003), ('+', 33, 9, 35.079126037994115)], [['3C 147'], (5, 42, 36.13789842999978), ('+', 49, 51, 7.233725100038555)], [['3C 286'], (13, 31, 8.288506066401311), ('+', 30, 30, 32.96082510699563)]], 'LBA': [[['M31'], (0, 42, 44.3299999999995), ('+', 41, 16, 7.499999999989768)]]}
    '''
    if hba_source_names is None:
        hba_source_names = ['3C 196', '3C 48', '3C 147', '3C 286']
    if lba_source_names is None:
        lba_source_names = ['Cyg A', 'Tau A', '3C 196', '3C 295', '3C 380']
    hba_target_sources = [simbad(source) for source in hba_source_names]
    lba_target_sources = [simbad(source) for source in lba_source_names] 
    return SourceCatalogue(source_table={
        'HBA': [[[src.name], src.ra_angle.as_shms()[1:], src.dec_angle.as_sdms()]
                for src in hba_target_sources],
        'LBA': [[[src.name], src.ra_angle.as_shms()[1:], src.dec_angle.as_sdms()]
                for src in lba_target_sources]})




def target_reference_station_sets(base_station_set, include_list=None, exclude_list=None):
    r'''
    Compose two sets of stations that alternately can become target or reference stations.

    **Parameters**

    base_station_set : string
        Name of the lofarobsxml station set to use as a basis: 'core', 'nl', 'remote', 'int', 'all'.

    include_list : [string]
        List of stations to add to the base list. Use uppercase only, e.g. 'DE601'.

    exlude_list : [string]
        List of station names to exclude from the base list. Use uppercase only, e.g. 'DE601'.

    **Returns**

    Pair of lists of station names (set_a, set_b).

    **Examples**

    >>> target_reference_station_sets('nl', include_list=['UK608', 'SE607'], exclude_list=['RS409', 'RS508'])
    (['CS001', 'CS002', 'CS003', 'CS004', 'CS005', 'CS006', 'CS007', 'CS011', 'CS013', 'CS017', 'CS021', 'CS024', 'CS026', 'CS028', 'CS030', 'CS031', 'CS032', 'CS101', 'CS103', 'CS201', 'CS301', 'CS302', 'CS401', 'CS501', 'RS106', 'RS208', 'RS305', 'RS307', 'RS406', 'SE607'], ['RS205', 'RS210', 'RS306', 'RS310', 'RS407', 'RS503', 'RS509', 'UK608'])
    '''
    eu_a = ['DE602', 'DE603','DE604', 'DE605', 'SE607', 'PL610', 'PL612']
    eu_b = ['DE601', 'FR606', 'DE609', 'PL611', 'IE613', 'UK608', 'DE609','LV614']
    default_station_set_a = [s for s in station_list('core')+station_list('remote')[0::2] + eu_a
                             if s not in exclude_list]
    default_station_set_b = [s for s in station_list('remote')[1::2] + eu_b
                             if s not in exclude_list]

    all_requested_stations = station_list(base_station_set,
                                          include=include_list,
                                          exclude=exclude_list)
    station_set_a = [s for s in default_station_set_a if s in all_requested_stations]
    station_set_b = [s for s in default_station_set_b if s in all_requested_stations]
    return station_set_a, station_set_b
    


def full_session(antenna_set, rcu_filter, n_sb=10, sb_list=None):
    '''
    antenna_set
    rcu_filter: '10_90', '30_90', '110_190', '170_230', '210_250'
    '''
    n_upper = int(np.floor(n_sb*2.0/3.0))
    n_lower =  n_sb - n_upper + 1
    sb_lba_low = np.linspace(int(np.floor(20/100.*512)), int(np.floor(50/100.*512)), n_lower, dtype=int)[:-1]
    sb_lba_high = np.linspace(int(np.floor(50/100.*512)), int(np.floor(75/100.*512)), n_upper, dtype=int)
    sb_lba = np.concatenate([sb_lba_low, sb_lba_high])
    default_subbands = {
        '10_90' : sb_lba,
        '30_90' : sb_lba,
        '10_70' : sb_lba,
        '30_70' : sb_lba,
        '110_190' : np.linspace(int(np.floor(15/100.*512)), int(np.floor(80/100.*512)), n_sb, dtype=int),
        '170_230' : np.linspace(int(np.floor(10/100.*512)), int(np.floor(70/100.*512)), n_sb, dtype=int),
        '210_250' : np.linspace(int(np.floor(10/100.*512)), int(np.floor(45/100.*512)), n_sb, dtype=int)}
    if sb_list is None:
        subbands = default_subbands[rcu_filter]
    else:
        subbands = sb_list
    full_list = [(antenna_set, rcu_filter, sb) for sb in subbands]
    return full_list  # [::-1] # begin with highest frequencies...



def momxml_holog_obs(antenna_set, rcu_filter, target_source,
                     obs_start_date, obs_duration_s, sub_band, num_beams,
                     stations, clock_mhz=200, bit_mode=8):
    r'''
    Return the observation and pipeline objects for one holography observation.
    '''
    backend = BackendProcessing(
        channels_per_subband=64,
        integration_time_seconds=1,
        correlated_data=True,
        beamformed_data=False,
        default_template='BeamObservationNoStationControl')
    freq_range = {'10_90': 'LBA_LOW',
                  '30_90': 'LBA_HIGH',
                  '110_190':'HBA_LOW',
                  '170_230': 'HBA_MID',
                  '210_250': 'HBA_HIGH'}
    start_utc_tuple = Time(obs_start_date).datetime.timetuple()[:6]
    end_utc_tuple = (Time(obs_start_date)+obs_duration_s*u.second).datetime.timetuple()[:6]
    pipeline_start_utc_tuple=(Time(obs_start_date)+obs_duration_s*u.second +10*u.minute).datetime.timetuple()[:6]

    beam_list = [Beam(sap_id=0,
                      target_source=target_source,
                      subband_spec=','.join(['%d' % sub_band
                                             for i in range(num_beams)]))]
    obs = Observation(
        antenna_set=antenna_set,
        frequency_range=freq_range[rcu_filter],
        start_date=start_utc_tuple,
        duration_seconds=obs_duration_s,
        stations=stations,
        clock_mhz=clock_mhz,
        bit_mode=bit_mode,
        backend=backend,
        name='H_'+antenna_set,
        initial_status='approved',
        beam_list=beam_list)

    pipeline = AveragingPipeline(name='AvgH_'+antenna_set,
                                 initial_status='approved',
                                 ndppp=NDPPP(64, 3, 64, 15, [], []),
                                 input_data=obs.children,
                                 duration_s=2*obs_duration_s,
                                 start_date=pipeline_start_utc_tuple,
                                 processing_nr_tasks=max(int(num_beams/13+1), 37))
    obs.append_child(pipeline)
    return obs






def holog_observation(antenna_set, rcu_filter,
                      target_stations, reference_stations,
                      target_source, obs_start_date,
                      sub_band,
                      bad_antenna_dict,
                      obs_gap_s,
                      obs_duration_s,
                      lm_grid_unit=penrose(481),
                      clock_mhz=200, bit_mode=8,
                      over_sampling_factor=1.2,
                      beam_start_interval_s=0.032,
                      verbose=False):
    r'''
    Beam startup interval is at least 0.032 s for all.

    **Examples**

    >>> bad = {k+'LBA':[] for k in ['CS002', 'RS106', 'RS305', 'RS509', 'DE609']+['RS503', 'RS508', 'RS205', 'DE603']}
    >>> mom_obs, beamctl_txt = holog_observation('LBA_INNER', rcu_filter='10_90',
    ...    target_stations=['CS002', 'RS106', 'RS305', 'RS509', 'DE609'],
    ...    reference_stations=['RS503', 'RS508', 'RS205', 'DE603'],
    ...    target_source=simbad('3C 295'),
    ...    obs_start_date='2015-07-07 19:00:01',
    ...    obs_gap_s=120.0,
    ...    obs_duration_s=30.0,
    ...    sub_band=270, bad_antenna_dict=bad)
    >>> mom_obs.children[0].target_source
    TargetSource(name      = '3C 295',
                 ra_angle  = Angle(shms = ('+', 14, 11, 20.467)),
                 dec_angle = Angle(sdms = ('+', 52, 12, 9.52)))
    >>> mom_obs.stations
    ['CS002', 'RS106', 'RS205', 'RS305', 'RS503', 'RS508', 'RS509', 'DE603', 'DE609']
    >>> mom_obs.children[1].ndppp.avg_time_step
    3
    >>> mom_obs.children[1].ndppp.avg_freq_step
    64

    >>> beamctl_txt.split('\n')[0]
    '2015-07-07 18:59:20   2015-07-07 19:00:36   8   0.032'

    >>> beamctl_txt.split('\n')[1]
    'RS503LBA    LBA_INNER   270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270,270        0:480       0:91   10_90   3.714676386,0.91110802,J2000   3.714676386,0.91110802,J2000'

    >>> beamctl_txt.split('\n')[-200:-198]
    ['RS509LBA    LBA_INNER    270          281       0:91   10_90   3.482209636,-0.06492851,J2000   3.714676386,0.91110802,J2000', 'RS509LBA    LBA_INNER    270          282       0:91   10_90   nan,nan,J2000   3.714676386,0.91110802,J2000']
    '''
    if verbose: print(10,datetime.datetime.now()) # check
    target_skycoord = SkyCoord(ra=target_source.ra_angle.as_rad()*u.rad,
                               dec=target_source.dec_angle.as_rad()*u.rad,
                               frame='icrs',
                               obstime=Time(obs_start_date)+obs_duration_s*0.5*u.second)
    if verbose: print(20,datetime.datetime.now())

    beams = beamctl_setup(target_skycoord,
                          obs_start_date=obs_start_date,
                          obs_gap_s=obs_gap_s,
                          obs_duration_s=obs_duration_s,
                          target_stations=target_stations,
                          reference_stations=reference_stations,
                          sub_band=sub_band,
                          rcu_filter=rcu_filter,
                          antenna_set=antenna_set,
                          bit_mode=bit_mode,
                          clock_mhz=clock_mhz,
                          lm_grid_unit=lm_grid_unit,
                          bad_antenna_dict=bad_antenna_dict,
                          over_sampling_factor=over_sampling_factor,
                          beam_start_interval_s=beam_start_interval_s,
                          verbose=verbose)
    if verbose: print(30,datetime.datetime.now())

    mom_obs = momxml_holog_obs(
        antenna_set=antenna_set,
        rcu_filter=rcu_filter,
        target_source=target_source,
        obs_start_date=obs_start_date,
        obs_duration_s=obs_duration_s,
        sub_band=sub_band,
        num_beams=len(lm_grid_unit),
        stations=sort_station_list(target_stations+reference_stations),
        clock_mhz=clock_mhz,
        bit_mode=bit_mode)
    if verbose: print(40,datetime.datetime.now())

    return mom_obs, beamctl_as_text(beams)




def holog_session_files(session_name, session_start_date_utc, observing_blocks,
                        station_set_a, station_set_b,
                        bad_antenna_dict,
                        hba_obs_duration_s=30.0, lba_obs_duration_s=300.0,
                        obs_gap_s=120.0, beam_start_interval_s=0.032,
                        station_dir='stationbeams'):
    r'''
    '''
    cat = cal_source_catalogue()
    mom_observations, station_observations = [], []
    start_date_utc = Time(session_start_date_utc)+ 2*u.minute #+ 6*u.minute
    date_utc = start_date_utc
    print(1, datetime.datetime.now()) # check
    for antenna_set, rcuf, sub_band in observing_blocks:
        src = cat.cal_source(date_utc.iso, antenna_set[:3], max_elevation_deg=91)
        set_a = station_set_a
        set_b = station_set_b
        print(1,datetime.datetime.now())
        if 'HBA' in antenna_set:
            obs_duration_s = hba_obs_duration_s
        elif 'LBA' in antenna_set:
            obs_duration_s = lba_obs_duration_s
        else:
            raise ValueError('Do not recognize antenna_set(%s)' % antenna_set)
        print(2,datetime.datetime.now())   # check  
        mom_obs, station_obs = holog_observation(
            antenna_set=antenna_set,
            rcu_filter=rcuf,
            target_stations=set_a,
            reference_stations=set_b,
            target_source=src,
            sub_band=sub_band,
            bad_antenna_dict=bad_antenna_dict,
            obs_gap_s=obs_gap_s,
            obs_duration_s=obs_duration_s,
            obs_start_date=date_utc,
            beam_start_interval_s=beam_start_interval_s)
        print(3,datetime.datetime.now()) # check
        mom_observations.append(mom_obs)
        station_observations.append(station_obs)
        date_utc = date_utc + (obs_duration_s + obs_gap_s)*u.second

        mom_obs, station_obs = holog_observation(
            antenna_set=antenna_set,
            rcu_filter=rcuf,
            target_stations=set_b,
            reference_stations=set_a,
            target_source=src,
            sub_band=sub_band,
            bad_antenna_dict=bad_antenna_dict,
            obs_gap_s=obs_gap_s,
            obs_duration_s=obs_duration_s,
            obs_start_date=date_utc,
            beam_start_interval_s=beam_start_interval_s)
        print(4,datetime.datetime.now()) # check
        mom_observations.append(mom_obs)
        station_observations.append(station_obs)
        date_utc = date_utc + (obs_duration_s + obs_gap_s)*u.second


    # Produce MoM XML file    
    folder = Folder(session_name, children=mom_observations,
                    description='Aperture holograpy',
                    grouping_parent=True)
    with open(session_name+'.xml', 'w') as out:
        out.write(xml([folder], project='HOLOG_WINDMILL_TESTS'))
    print(5,datetime.datetime.now()) # check
    # Station control files
    if not os.path.exists(station_dir):
        os.makedirs(station_dir)
    # TODO: write index.csv file
    for sequence_number, station_obs in enumerate(station_observations):
        with open(os.path.join(station_dir, (session_name+'-%03d.txt' % sequence_number)), 'w') as out:
            out.write(station_obs)
    print(6,datetime.datetime.now()) # check
    
