from numpy import arange, around, array, exp, meshgrid, pi, sqrt, unique


def rectangular(n_x, n_y, offset_x=0, offset_y=0):
    r'''
    Generate an offset rectangular grid with unit distance between
    points. If the offsets are zero, the grid is centred at point
    (n_x//2, n_y//2).

    **Parameters**

    n_x : int
        Number of grid points in x direction.

    n_y : int
        Number of grid points in y direction.

    offset_x : float
        Amount added to x positions.

    offset_x : float
        Amount added to x positions.

    **Examples**

    >>> rectangular(3, 2)
    array([[-1, -1],
           [ 0, -1],
           [ 1, -1],
           [-1,  0],
           [ 0,  0],
           [ 1,  0]])

    >>> rectangular(5, 5, offset_x=+1, offset_y=-1.5)
    array([[-1. , -3.5],
           [ 0. , -3.5],
           [ 1. , -3.5],
           [ 2. , -3.5],
           [ 3. , -3.5],
           [-1. , -2.5],
           [ 0. , -2.5],
           [ 1. , -2.5],
           [ 2. , -2.5],
           [ 3. , -2.5],
           [-1. , -1.5],
           [ 0. , -1.5],
           [ 1. , -1.5],
           [ 2. , -1.5],
           [ 3. , -1.5],
           [-1. , -0.5],
           [ 0. , -0.5],
           [ 1. , -0.5],
           [ 2. , -0.5],
           [ 3. , -0.5],
           [-1. ,  0.5],
           [ 0. ,  0.5],
           [ 1. ,  0.5],
           [ 2. ,  0.5],
           [ 3. ,  0.5]])
    '''
    x = arange(n_x) - n_x//2 + offset_x
    y = arange(n_y) - n_y//2 + offset_y
    xx, yy = meshgrid(x, y)
    return array([xx.ravel(), yy.ravel()]).T




def penrose_subdivide(triangles):
    r'''
    Take a list of triangles for a Penrose tiling, and subdivide into
    a higher resolution grid. The algorithm is from
    http://preshing.com/20110831/penrose-tiling-explained/ and more
    information about Penrose tilings is found at
    https://en.wikipedia.org/wiki/Penrose_tiling
    Make sure that neighbouring triangles have the opposite sense, so
    they combine to form the proper rhombs.


    **Parameters**

    triangles : list of 4-tuples
        Each tuple contains the type of triangle and its three
        vertices encoded as complex numbers x +1.j*y. The tuple is
        formatted as (angle_deg, top_xy, bottom1_xy,
        bottom2_xy). Angle_deg may be either 36 or 108 degrees.


    **Returns**

    List of triangle tuples with the same format as the input, where
    each 36 degree triangle is replaced by a 36 and a 108 degree
    triangle, and each 108 degree triangle is replaced by one 36 and
    two 108 degree triangles.


    **Examples**

    >>> triangles = [(36, 0.0,
    ...               exp(1j*(angle+36*(i%2))*pi/180),
    ...               exp(1j*(angle+36*(1-i%2))*pi/180))
    ...              for i, angle in enumerate(range(0, 37, 36))]
    >>> triangles
    [(36, 0.0, (1+0j), (0.8090169943749475+0.5877852522924731j)), (36, 0.0, (0.30901699437494745+0.9510565162951535j), (0.8090169943749475+0.5877852522924731j))]
    >>> sub_1 = penrose_subdivide(triangles)
    >>> sub_1
    [(36, (0.8090169943749475+0.5877852522924731j), (0.6180339887498948+0j), (1+0j)), (108, (0.6180339887498948+0j), (0.8090169943749475+0.5877852522924731j), 0.0), (36, (0.8090169943749475+0.5877852522924731j), (0.19098300562505258+0.587785252292473j), (0.30901699437494745+0.9510565162951535j)), (108, (0.19098300562505258+0.587785252292473j), (0.8090169943749475+0.5877852522924731j), 0.0)]
    >>> triangles_108 = [sub_1[1], sub_1[3]]
    >>> penrose_subdivide(triangles_108)
    [(108, (0.3090169943749475+0.22451398828979274j), 0.0, (0.6180339887498948+0j)), (108, (0.6909830056250525+0.22451398828979274j), (0.3090169943749475+0.22451398828979274j), (0.8090169943749475+0.5877852522924731j)), (36, (0.3090169943749475+0.22451398828979274j), (0.6909830056250525+0.22451398828979274j), (0.6180339887498948+0j)), (108, (0.3090169943749475+0.22451398828979274j), 0.0, (0.19098300562505258+0.587785252292473j)), (108, (0.4270509831248423+0.587785252292473j), (0.3090169943749475+0.22451398828979274j), (0.8090169943749475+0.5877852522924731j)), (36, (0.3090169943749475+0.22451398828979274j), (0.4270509831248423+0.587785252292473j), (0.19098300562505258+0.587785252292473j))]

    '''
    golden_ratio = (1 + sqrt(5)) / 2.0
    result = []
    for angle_deg, A, B, C in triangles:
        if angle_deg == 36:
            # Subdivide red triangle
            P = A + (B - A) / golden_ratio
            result += [(36, C, P, B), (108, P, C, A)]
        else:
            # Subdivide blue triangle
            Q = B + (A - B) / golden_ratio
            R = B + (C - B) / golden_ratio
            result += [(108, R, C, A), (108, Q, R, B), (36, R, Q, A)]
    return result



def penrose_unique_vertices(triangles, precision=12):
    r'''
    For holography, we are only interested in the vertices of the
    Penrose tiling, not in the faces. The penrose_subdivide() function
    delivers a set of faces that share vertices. This function takes
    such a list of triangles, and outputs the unique vertices as a
    single dimensional array of complex x+1.j*y numbers.

    To deal with non-uniqueness due to finite machine precision, the
    points are first rounded to precision decimal places using the
    numpy around() function.

    **Parameters**

    triangles : list of 4-tuples
        Each tuple contains the type of triangle and its three
        vertices encoded as complex numbers x +1.j*y. The tuple is
        formatted as (angle_deg, top_xy, bottom1_xy,
        bottom2_xy). Angle_deg may be either 36 or 108 degrees.

    precision : integer
        Number of decimal places to round to. Default: 12.

    **Returns**

    A single dimensional array of complex x+1.j*y numbers.


    **Examples**

    >>> triangles = [(36, 0.0,
    ...               exp(1j*(angle+36*(i%2))*pi/180),
    ...               exp(1j*(angle+36*(1-i%2))*pi/180))
    ...              for i, angle in enumerate(range(0, 361, 36))]
    >>> all_vertices = array([ triangle[1:] for triangle in triangles]).ravel()
    >>> all_vertices.shape
    (33,)
    >>> unique_vertices = penrose_unique_vertices(triangles)
    >>> unique_vertices
    array([-1.        +0.j        , -0.80901699-0.58778525j,
           -0.80901699+0.58778525j, -0.30901699-0.95105652j,
           -0.30901699+0.95105652j,  0.        +0.j        ,
            0.30901699-0.95105652j,  0.30901699+0.95105652j,
            0.80901699-0.58778525j,  0.80901699+0.58778525j,
            1.        -0.j        ])

    >>> unique_vertices = penrose_unique_vertices(triangles, precision=3)
    >>> unique_vertices
    array([-1.   +0.j   , -0.809-0.588j, -0.809+0.588j, -0.309-0.951j,
           -0.309+0.951j,  0.   +0.j   ,  0.309-0.951j,  0.309+0.951j,
            0.809-0.588j,  0.809+0.588j,  1.   -0.j   ])
    '''
    points = unique(around(array([triangle[1:]
                                  for triangle in triangles]).ravel(),
                           decimals=precision))
    return points



def penrose(n_beams, precision=12):
    r'''
    Generate a circular grid from the vertices of a Penrose
    tiling. The grid contains at most n_beams beams. Tile edges are
    all of unit length.


    **Parameters**

    n_beams : int
        The maximum number of beams  that may occur in the grid.

    precision : integer
        Number of decimal places to round to. Default: 12.


    **Returns**

    An nx2 numpy.array of floats containing the x and y positions.


    **Examples**

    >>> penrose(5)
    array([[0., 0.]])
    >>> penrose(11)
    array([[-1.        ,  0.        ],
           [-0.80901699, -0.58778525],
           [-0.80901699,  0.58778525],
           [-0.30901699, -0.95105652],
           [-0.30901699,  0.95105652],
           [ 0.        ,  0.        ],
           [ 0.30901699, -0.95105652],
           [ 0.30901699,  0.95105652],
           [ 0.80901699, -0.58778525],
           [ 0.80901699,  0.58778525],
           [ 1.        , -0.        ]])
    >>> penrose(15, precision=8)
    array([[-0.80901699, -0.58778525],
           [-0.80901699,  0.58778525],
           [ 0.        ,  0.        ],
           [ 0.309017  , -0.95105651],
           [ 0.309017  ,  0.95105651],
           [ 1.        ,  0.        ]])
    >>> penrose(16, precision=8)
    array([[-1.61803399,  0.        ],
           [-1.30901699, -0.95105651],
           [-1.30901699,  0.95105651],
           [-0.80901699, -0.58778525],
           [-0.80901699,  0.58778525],
           [-0.49999999, -1.53884177],
           [-0.49999999,  1.53884177],
           [ 0.        ,  0.        ],
           [ 0.309017  , -0.95105651],
           [ 0.309017  ,  0.95105651],
           [ 0.49999999, -1.53884177],
           [ 0.49999999,  1.53884177],
           [ 1.        ,  0.        ],
           [ 1.30901699, -0.95105651],
           [ 1.30901699,  0.95105651],
           [ 1.61803399,  0.        ]])
    '''
    golden_ratio = (1 + sqrt(5)) / 2.0
    triangles = [(36, 0.0,
                  exp(1j*(angle+36*(i%2))*pi/180),
                  exp(1j*(angle+36*(1-i%2))*pi/180))
                 for i, angle in enumerate(range(0, 361, 36))]
    beams = penrose_unique_vertices(triangles, precision=precision)
    scale = 1.0
    while len(beams) < n_beams:
        triangles = penrose_subdivide(triangles)
        beams = penrose_unique_vertices(triangles, precision=precision)
        scale *= golden_ratio

    surface_ratio = 1.0
    while len(beams) > n_beams:
        surface_ratio *= n_beams/float(len(beams))
        beams = beams[abs(beams) <= sqrt(surface_ratio)]
    return array([beams.real, beams.imag]).T*scale


