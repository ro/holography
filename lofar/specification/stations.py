from lofarantpos.db import LofarAntennaDatabase
from lofarobsxml.utilities import parse_subband_list
import astropy.units as u
from astropy.coordinates import SkyCoord
from astropy.time import Time
from numpy import array, dot, newaxis, sqrt, nan, float64
import os
from collections import Counter
import datetime

from ..calibration.common.coordinates import pqr_from_icrs, icrs_from_pqr

DB = LofarAntennaDatabase()

def rcumode2filter(rcu_mode):
    ''' Map rcu mode to filter. e.g. rcumode2filter(5) return 110_190 '''
    mapping={1: '10_90', 2: '30_90', 3: '10_90', 4: '30_90', 5: '110_190', 6: '170_230', 7: '210_250'}
    return mapping[rcu_mode]

def spec2list(spec,separator=':'):
    ant=[]
    for a in spec.split(','):
        if separator not in a:
            ant.extend([int(a),])
        else:
            ant.extend(range(int(a.split(separator)[0]),int(a.split(separator)[1])+1))
    return ant

def list2spec(antlist,separator=':'):
    import numpy as np
    # make a numpy array of the antenna list
    ant=np.sort(np.array(antlist))
    # calculate the difference between the antennas
    dant=np.diff(ant)
    # the first antenna of a sequence will have a difference greater than one compared to the previous antenna. The very first antenna is also a first antenna.
    firstant=[ant[0],]+list(ant[1:][dant>1])
    # the last antenna of a sequence will have a difference greater than one compared to the next antenna. The very last antenna is also a last antenna.
    lastant=list(ant[0:-1][dant>1])+[ant[-1],]
    spec=""
    # go through the pairs from first till last antenna
    for ff,ll in zip(firstant,lastant):
        # if antennas are equal, there is just one in the set. Add e.g. 7,
        if int(ff)==int(ll):
            spec+=str(ff)+","
        # if antennas are separted by one, use a comma. Add e.g. 7,8,
        elif int(ll)-int(ff)==1:
            spec+=str(ff)+","+str(ll)+","
        # if antennas are separted more, use the separator, e.g. 7:11,
        else:
            spec+=str(ff)+separator+str(ll)+","
    return spec.rstrip(',')





def filterRCU(spec, brokenlist, seperator=':'):
    # remove broken antennas from the list of antennas specified
    # get all antennas
    allantenna = spec2list(spec,seperator)
    # remove antennas that are in the brokenlist
    antennalist = [a for a in allantenna if a not in brokenlist]
    # convert to the specification code
    newspec = list2spec(antennalist,seperator)
    return newspec

def pqr_to_itrs_matrix_from_field_name(field_name):
    r'''
    Look up the PQR to ITRS rotation matrix for the specified antenna
    field.

    **Parameters**

    field_name : string
        For example: 'CS002HBA1', 'DE601LBA', 'RS205HBA'

    **Returns**

    A 2D numpy.array of floats containing the pqr-to-itrs rotation
    matrix for the antenna field.


    **Examples**

    >>> pqr_to_itrs_matrix_from_field_name('CS002HBA0')
    array([[-1.19595105e-01, -7.91954452e-01,  5.98753002e-01],
           [ 9.92822748e-01, -9.54186800e-02,  7.20990002e-02],
           [ 3.30969000e-05,  6.03078288e-01,  7.97682002e-01]])

    >>> pqr_to_itrs_matrix_from_field_name('DE609LBA')
    array([[-0.13843728, -0.80081964,  0.58268604],
           [ 0.99003118, -0.0964868 ,  0.10260877],
           [-0.0259496 ,  0.59108222,  0.80619379]])
    '''
    global DB
    return DB.pqr_to_etrs[field_name]







def antenna_field_diameter_m(station_name, antenna_set):
    r'''
    Returns a float containing the antenna field diameter for a given
    station and rcu mode.

    **Parameters**

    station_name : string
        Name of the station, eg CS002, PL611, RS106.

    antenna_set : string
        'LBA_INNER', 'LBA_OUTER', 'LBA_SPARSE_EVEN', 'HBA_DUAL', 'HBA_ZERO', 'HBA_ONE'

    **Returns**

    A float in meters.

    **Examples**

    >>> antenna_field_diameter_m('CS003', 'HBA_DUAL')
    31.0
    >>> antenna_field_diameter_m('RS106', 'HBA_DUAL')
    41.0
    >>> antenna_field_diameter_m('CS302', 'LBA_OUTER')
    87.0
    >>> antenna_field_diameter_m('DE609', 'LBA_INNER')
    70.0
    >>> antenna_field_diameter_m('DE609', 'HBA_DUAL')
    60.0
    '''
    if station_name[:2].upper() == 'CS':
        if antenna_set in ['LBA_OUTER', 'LBA_SPARSE_EVEN', 'LBA_SPARSE_ODD']: return 87.0
        if antenna_set in ['LBA_INNER']:                                      return 30.0
        if antenna_set in ['HBA_DUAL', 'HBA_DUAL_INNER', 'HBA', 'HBA_ZERO', 'HBA_ONE']: return 31.0
    elif station_name[:2].upper() == 'RS':
        if antenna_set in ['LBA_OUTER', 'LBA_SPARSE_EVEN', 'LBA_SPARSE_ODD']: return 87.0
        if antenna_set in ['LBA_INNER']:                                      return 30.0
        if antenna_set in ['HBA_DUAL', 'HBA', 'HBA_ZERO', 'HBA_ONE']:         return 41.0
        if antenna_set in ['HBA_DUAL_INNER']:                                 return 31.0
    elif station_name.upper() == 'PL611':
        if 'LBA' in antenna_set: return 70.0
        if 'HBA' in antenna_set: return 41.0
    else:
        if 'LBA' in antenna_set: return 70.0
        if 'HBA' in antenna_set: return 60.0
    raise ValueError('Don\'t know diameter of %s of %s' % (antenna_set, station_name))




def antenna_field_names(station_name, antenna_set):
    r'''
    Return a list of antenna field names for this station and RCU
    mode.

    **Parameters**:

    station_name : string
        Name of the station, eg CS002, PL611, RS106.

    antenna_set : string
        'LBA_INNER', 'LBA_OUTER', 'LBA_SPARSE_EVEN', 'HBA_DUAL', 'HBA_ZERO', 'HBA_ONE'

    **Returns**

    A list of strings containing antenna field names for this station
    and RCU mode.


    **Examples**

    >>> antenna_field_names('CS002', 'LBA_INNER')
    ['LBA']
    >>> antenna_field_names('DE602', 'HBA_DUAL')
    ['HBA']
    >>> antenna_field_names('CS002', 'HBA_DUAL')
    ['HBA0', 'HBA1']
    >>> antenna_field_names('RS407', 'HBA_DUAL')
    ['HBA']
    '''
    if antenna_set[:3] == 'LBA':
        return ['LBA']
    else:
        if station_name[:2].upper() == 'CS':
            return {'HBA_ZERO':['HBA0'],
                    'HBA_ONE': ['HBA1'],
                    'HBA_DUAL': ['HBA0', 'HBA1']}[antenna_set]
        else:
            return ['HBA']
    raise ValueError('No antenna field for antenna_set %r at %s' %
                     (antenna_set, station_name))


def antenna_set_name(field_name, antenna_set, rcu_filter):
    r'''
    Get the antenna set name most appropriate for a beamctl call.

    **Parameters**

    field_name : string
        For example: 'CS002HBA1', 'DE601LBA', 'RS205HBA'

    antenna_set : string
        'LBA_INNER', 'LBA_OUTER', 'LBA_SPARSE_EVEN', 'HBA_DUAL', 'HBA_ZERO', 'HBA_ONE'

    rcu_mode : int
        0 (off), 1 (10 MHz, LBA_OUTER), 2 (30 MHz, LBA_OUTER), 3 (10
        MHz LBA_INNER), 4 (30 MHz LBA_INNER), 5 (HBA 110--190 MHz), 6
        (HBA 170--230 MHz), 7 (HBA 210--250 MHz)

    **Returns**

    String containing 'HBA_ZERO', 'HBA_ONE', 'HBA_DUAL', 'LBA_INNER'
    or 'LBA_OUTER'.


    **Examples**

    >>> antenna_set_name('CS002LBA', 'LBA_OUTER', rcu_filter='30_90')
    'LBA_OUTER'
    >>> antenna_set_name('CS002LBA', 'LBA_INNER', rcu_filter='10_90')
    'LBA_INNER'
    >>> antenna_set_name('CS002HBA0', 'HBA_DUAL', rcu_filter='170_230')
    'HBA_ZERO'
    >>> antenna_set_name('CS002HBA1', 'HBA_DUAL_INNER', rcu_filter='210_250')
    'HBA_ONE'
    >>> antenna_set_name('RS503HBA', 'HBA_DUAL', rcu_filter='110_190')
    'HBA_DUAL'
    >>> antenna_set_name('FR606LBA', 'LBA_INNER', rcu_filter='10_90')
    'LBA_INNER'
    '''
    if 'HBA0' in field_name.upper():
        return 'HBA_ZERO'
    elif 'HBA1' in field_name.upper():
        return 'HBA_ONE'
    elif 'HBA_ZERO' in field_name.upper():
        return 'HBA_ZERO'
    elif 'HBA_ONE' in field_name.upper():
        return 'HBA_ONE'
    elif 'HBA' in field_name.upper():
        return 'HBA_DUAL'
    elif 'LBA' in field_name.upper():
        return antenna_set
    else:
        raise ValueError('No valid antenna set for field %s and RCU filter %r' %
                         (field_name, rcu_filter))


def rcu_selection(field_name, antenna_set, bad_antenna_dict):
    r'''
    Get the RCU selection required for beamctl.

    **Parameters*

    field_name : string
        For example: 'CS002HBA1', 'DE601LBA', 'RS205HBA'

    antenna_set : string
        'LBA_INNER', 'LBA_OUTER', 'LBA_SPARSE_EVEN', 'HBA_DUAL', 'HBA_ZERO', 'HBA_ONE'

    bad_antenna_list : dict
        List with bad_antennas for each station.
        For example: {'CS001HBA': [20,21], 'CS002HBA': [30,31]}

    **Returns**

    A string containing an RCU selection.


    **Examples**

    >>> bad_antenna_dict = {'CS002HBA': [], 'RS205HBA': [], 'DE601LBA': [16,17,44,45], 'CS103LBA': []}
    >>> rcu_selection('CS002HBA0', 'HBA_DUAL', bad_antenna_dict)
    '0:47'
    >>> rcu_selection('CS002HBA1', 'HBA_DUAL', bad_antenna_dict)
    '48:95'
    >>> rcu_selection('RS205HBA', 'HBA_DUAL', bad_antenna_dict)
    '0:95'
    >>> rcu_selection('DE601LBA', 'LBA_OUTER', bad_antenna_dict)
    '0:15,18:43,46:191'
    >>> rcu_selection('CS103LBA', 'LBA_INNER', bad_antenna_dict)
    '0:91'
    >>> rcu_selection('CS103LBA', 'LBA_OUTER', bad_antenna_dict)
    '0:95'
    '''
    if 'HBA0' in field_name:
        spec='0:47'
    elif 'HBA1' in field_name:
        spec='48:95'
    elif 'CS' in field_name or 'RS' in field_name:
        if antenna_set.upper() == 'LBA_INNER':
            spec='0:91'
        else:
            spec='0:95'
    else:
        spec='0:191'
    return filterRCU(spec, bad_antenna_dict[field_name[0:8]])



def nyquist_zone(rcu_filter):
    r'''
    Compute Nyquist zone for RCU modes.

    **Parameters**

    rcu_filter : string
        '10_90', '30_90', '10_70', '30_70', '110_190', '170_230', or '210_250'

    **Examples**

    >>> nyquist_zone('10_90')
    0
    >>> nyquist_zone('30_90')
    0
    >>> nyquist_zone('110_190')
    1
    >>> nyquist_zone('170_230')
    2
    >>> nyquist_zone('210_250')
    2
    >>> nyquist_zone('280_400')
    Traceback (most recent call last):
    ...
    ValueError: Unknown RCU filter '280_400'
    '''
    if rcu_filter in ['10_90', '30_90', '10_70', '30_70']:
        return 0
    elif rcu_filter == '110_190':
        return 1
    elif rcu_filter == '170_230':
        return 2
    elif rcu_filter == '210_250':
        return 2
    else:
        raise ValueError('Unknown RCU filter %r' % rcu_filter)


def subband_freq_hz(sub_band, rcu_filter):
    r'''
    Compute central frequency of sub band based on rcu mode and clock
    frequency.


    **Parameters**

    sub_band : int
        Sub band number (0--511).

    rcu_filter : string
        '10_90', '30_90', '10_70', '30_70', '110_190', '170_230', or '210_250'

    **Returns**

    Central frequency of a sub band in Hz as a float.


    **Examples**

    >>> subband_freq_hz(77, rcu_filter='110_190')
    115039062.5
    >>> subband_freq_hz(256+128, rcu_filter='30_90')
    75000000.0
    >>> subband_freq_hz(256+128, rcu_filter='10_70')
    60000000.0
    >>> subband_freq_hz(77, rcu_filter='210_250')
    215039062.5
    >>> subband_freq_hz(128, rcu_filter='170_230')
    180000000.0
    '''
    clock_mhz = 200
    if rcu_filter in ['10_70', '30_70', '170_230']: clock_mhz = 160
    base_freq_hz = nyquist_zone(rcu_filter)*clock_mhz*1e6/2.0
    offset_freq_hz = sub_band*clock_mhz*1e6/1024.0
    return base_freq_hz + offset_freq_hz









def beamctl_setup(target_skycoord, obs_start_date, obs_gap_s, obs_duration_s,
                  target_stations, reference_stations,
                  sub_band, rcu_filter, antenna_set, clock_mhz, bit_mode,
                  lm_grid_unit, bad_antenna_dict, over_sampling_factor=1.2,
                  beam_start_interval_s=0.032,
                  verbose=True):
    r'''
    Compute beam positions and specifications for all target and
    reference antenna fields. This function takes a rather long time
    to evaluate. For example: 481 beams for 4 target antenna fields
    (one core station, one remote, and one international station)
    takes about 2m30s. Expect 45 minutes for a full LOFAR HBA
    holography run.

    Beam startup interval is at least 0.032 seconds for a typical station.


    **Parameters**

    target_skycoord : astropy.coordinates.SkyCoord
        Position of the target source.

    obs_start_date : astropy.time.Time or compatible string
        The UTC start date of the observation.

    obs_gap_s : float
        Gap between previous observation and this one in seconds.

    obs_duration_s : number
        The duration of the observation in seconds. Recommended: 30.

    target_stations : list of strings
        Names of antenna stations that need to be measured. For
        example ['CS002', 'RS106', 'UK608'].

    reference_stations : list of strings
        Names of reference antenna stations. Should not overlap with
        target stations! For example: ['RS205', 'DE601', 'CS302'].

    sub_band : int (0..511 inclusive)
        Number of the sub band at the station.

    rcu_filter : string
        '10_90', '30_90', '110_190', '170_230', or '210_250'

    antenna_set : string
        'LBA_INNER', 'LBA_OUTER', 'LBA_SPARSE_EVEN', 'HBA_DUAL', 'HBA_ZERO', 'HBA_ONE'

    clock_mhz : int
        Station clock frequency. Either 200 or 160.

    bit_mode : int
        Bit mode for beam forming and signal processing. Either 4, 8,
        or 16. 16: 244 beams max; 8: 488 beams max; 4 is broken at the
        stations.

    lm_grid_unit : numpy.array of floats
        The pointing grid to be put around the target source. nominal
        distance between points must be unity. Positions are
        multiplied by beam spacing inside this function. Dimensions
        [n_beams, 2].

    over_sampling_factor : float
        Divide Nyquist beam spacing of lambda/D by this number to over
        sample the beam and separate aliased aperture images from the
        main aperture image.

    beam_start_interval_s : float
        Number of seconds to wait until the next beam is started. Default: 0.05.

    bad_antenna_dict : dict
        Dictionary of bad antennas per field
        For example: {'CS001HBA': [20,21], 'CS002HBA': [30,31]}

    verbose : bool
        If True: emit debug messages.

    **Returns**

    List of lists of strings, one per beamlet per antenna
    field. Format of individual list:
    [field_name, antenna_set, sub_band_spec, beamlet_spec,
    rcu_selection, rcu_filter,
    digital_beam_direction, calibration_source_position (even if LBA)]


    **Examples**

    >>> from .grids import penrose
    >>> bad = {'CS002HBA': [], 'RS509HBA': [], 'DE609HBA': [], 'CS301HBA': [], 'RS407HBA': [], 'DE604HBA': []}
    >>> target_3c196 = SkyCoord('8h13m36.0561s', '+48d13m02.636s', frame='icrs')
    >>> beams = beamctl_setup(target_skycoord=target_3c196,
    ...                       obs_start_date='2015-07-08 14:00:00',
    ...                       obs_gap_s=120.0,
    ...                       obs_duration_s=30.0,
    ...                       sub_band=280,
    ...                       rcu_filter='110_190',
    ...                       antenna_set='HBA_DUAL',
    ...                       clock_mhz=200,
    ...                       bit_mode=8,
    ...                       target_stations=['CS002', 'RS509', 'DE609'],
    ...                       reference_stations=['CS301', 'RS407', 'DE604'],
    ...                       bad_antenna_dict = bad,
    ...                       lm_grid_unit=penrose(40), verbose=False)
    >>> beams[0]
    ('2015-07-08 13:59:33', '2015-07-08 14:00:35', '8', '0.032')
    >>> beams[2:4]
    [('CS301HBA1', 'HBA_ONE', '280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280', '1000:1030', '48:95', '110_190', '2.153740377,0.84155237,J2000', '2.153740377,0.84155237,J2000'), ('RS407HBA', 'HBA_DUAL', '280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280,280', '0:30', '0:95', '110_190', '2.153740377,0.84155237,J2000', '2.153740377,0.84155237,J2000')]

    >>> beams[45:49]
    [('CS002HBA0', 'HBA_ZERO', '280', '20', '0:47', '110_190', '2.077504252,0.85803804,J2000', '2.153740377,0.84155237,J2000'), ('CS002HBA1', 'HBA_ONE', '280', '1020', '48:95', '110_190', '2.077504252,0.85803804,J2000', '2.153740377,0.84155237,J2000'), ('CS002HBA0', 'HBA_ZERO', '280', '21', '0:47', '110_190', '2.104822092,0.79953358,J2000', '2.153740377,0.84155237,J2000'), ('CS002HBA1', 'HBA_ONE', '280', '1021', '48:95', '110_190', '2.104822092,0.79953358,J2000', '2.153740377,0.84155237,J2000')]


    >>> print('\n'.join([repr(bm) for bm in beams[-16:-13]]))
    ('RS509HBA', 'HBA_DUAL', '280', '15', '0:95', '110_190', '2.196862182,0.74749315,J2000', '2.153740377,0.84155237,J2000')
    ('RS509HBA', 'HBA_DUAL', '280', '16', '0:95', '110_190', '2.153740377,0.84155237,J2000', '2.153740377,0.84155237,J2000')
    ('RS509HBA', 'HBA_DUAL', '280', '17', '0:95', '110_190', '2.097118356,0.89386139,J2000', '2.153740377,0.84155237,J2000')


    Note that for the central beam, the digital and analogue beams are
    different at the 1e-7 degree (20 mas) level. This is because the
    rotation matrices in AntennaField.conf are listed with limited
    precision and are therefore not true rotation matrices.

    Dutch stations in LBA need factor two less beam startup time than internationals.

    >>> bad = {'CS002LBA': [], 'RS509LBA': [], 'CS301LBA': [], 'RS407LBA': [], 'DE604LBA': []}
    >>> beams = beamctl_setup(target_skycoord=target_3c196,
    ...                       obs_start_date='2015-07-08 14:00:00',
    ...                       obs_gap_s=120.0,
    ...                       obs_duration_s=30.0,
    ...                       sub_band=280,
    ...                       rcu_filter='10_90',
    ...                       antenna_set='LBA_INNER',
    ...                       clock_mhz=200,
    ...                       bit_mode=8,
    ...                       target_stations=['CS002', 'RS509'],
    ...                       reference_stations=['CS301', 'RS407', 'DE604'],
    ...                       beam_start_interval_s=0.03, bad_antenna_dict=bad,
    ...                       lm_grid_unit=penrose(40), verbose=False)
    >>> beams[0]
    ('2015-07-08 13:59:34', '2015-07-08 14:00:35', '8', '0.030')

    '''
    if beam_start_interval_s < 0.030:
        raise ValueError('beam_start_interval_s=%r s too short: In measurements d.d. 2021-02-04 S. ter Veen and M.A. Brentjens determined that apparently a LOFAR station needs 0.0316 seconds or more to start a beam process.' % beam_start_interval_s)

    ra0_rad = target_skycoord.icrs.ra.rad
    dec0_rad = target_skycoord.icrs.dec.rad
    mid_date = Time(obs_start_date) + 0.5*obs_duration_s*u.second
    lightspeed_m_s = 299792458.0
    freq_hz = subband_freq_hz(sub_band=sub_band, rcu_filter=rcu_filter)
    num_beams = len(lm_grid_unit)
    reference_antenna_fields = [(station+field).upper()
                                for station in reference_stations
                                for field in antenna_field_names(station, antenna_set)]
    if verbose: print(100,datetime.datetime.now())
    ref_station_beams = [(field,
                          antenna_set_name(field, antenna_set, rcu_filter),
                          ','.join([str(sub_band)]*num_beams),
                          '%d:%d' % ((1000, 1000+num_beams-1) if 'HBA1' in field else (0, num_beams-1)),
                          rcu_selection(field, antenna_set, bad_antenna_dict),
                          rcu_filter,
                          '%.9f,%.8f,J2000' % (ra0_rad, dec0_rad),
                          '%.9f,%.8f,J2000' % (ra0_rad, dec0_rad))
                         for field in reference_antenna_fields]

    if verbose: print(200,datetime.datetime.now())
    target_antenna_fields = [(station+field).upper()
                             for station in target_stations
                             for field in antenna_field_names(station, antenna_set)]
    diameters_m = array([antenna_field_diameter_m(field, antenna_set)
                        for field  in target_antenna_fields])
    beam_spacings_rad = (lightspeed_m_s/freq_hz)/(over_sampling_factor*diameters_m)
    if verbose: print(210,datetime.datetime.now())
    pqr_to_itrs_matrices = [
        pqr_to_itrs_matrix_from_field_name(field)
        for field in target_antenna_fields]
    pqr0s = [pqr_from_icrs(array([ra0_rad, dec0_rad]), mid_date, mat)
             for mat in pqr_to_itrs_matrices]
    if verbose: print(220,datetime.datetime.now())

    pq_grids_rad = array([pqr0[newaxis, :2] - spacing*lm_grid_unit
                          for spacing, pqr0  in
                          zip(beam_spacings_rad, pqr0s)])
    if verbose: print(230,datetime.datetime.now())
    p = pq_grids_rad[:,:,0]
    q = pq_grids_rad[:,:,1]
    r_squared = 1.0 - p**2 -q**2
    pqr_mask = r_squared >= 1.0
    r = sqrt(1.0 - p**2 -q**2) # nan if not observable.
    pqr_grids_rad = array([p, q, r]).transpose((1,2,0))
    if verbose: print(240,datetime.datetime.now())
    icrs_beams_skycoord = [icrs_from_pqr(pqr[pqr[:,2] != nan], mid_date, mat)
                           for pqr, mat in
                           zip(pqr_grids_rad, pqr_to_itrs_matrices)]
    if verbose: print(250,datetime.datetime.now())
    icrs_beams_rad = [array([beams.icrs.ra.rad, beams.icrs.dec.rad], dtype=float64).T
                      for beams in icrs_beams_skycoord]
    if verbose: print(260,datetime.datetime.now())
    target_station_beams = [
        (field,
         antenna_set_name(field, antenna_set, rcu_filter),
         '%d' % sub_band,
         '%d' % (beamlet+1000 if 'HBA1' in field else beamlet, ),
         rcu_selection(field, antenna_set, bad_antenna_dict),
         rcu_filter,
         '%.9f,%.8f,J2000' % (beam_coord[0], beam_coord[1]),
         '%.9f,%.8f,J2000' % (ra0_rad, dec0_rad))
        for field, beams in zip(target_antenna_fields, icrs_beams_rad)
        for beamlet, beam_coord in enumerate(beams)
        ]
    beam_start_duration = Counter()
    if verbose: print(300,datetime.datetime.now())
    for station_beam in target_station_beams:
        beam_startup_interval_s = beam_start_interval_s
        #if station_beam[0][:2] == 'CS':
        #    beam_startup_interval_s = beam_start_interval_s
        #if station_beam[0][:2] == 'RS':
        #    beam_startup_interval_s = beam_start_interval_s
        beam_start_duration[station_beam[0][:5]] += beam_startup_interval_s
    if verbose: print(400,datetime.datetime.now())
    max_beam_start_duration_s = beam_start_duration.most_common(1)[0][1]
    lead_in_time = (1.05*max_beam_start_duration_s+25.0)*u.second
    if obs_gap_s*u.s <= lead_in_time:
        raise ValueError('obs_gap_s (%r) < lead_in_time(%r)' % (obs_gap_s*u.s, lead_in_time))
    if verbose: print('lead_in_time= %r ' % lead_in_time)
    beams_start_date = Time(obs_start_date)-lead_in_time
    beams_start_date.precision=0
    if verbose: print(500,datetime.datetime.now())
    beams_kill_date = Time(obs_start_date) + (obs_duration_s + 5)*u.second
    beams_kill_date.precision=0
    if verbose: print(600,datetime.datetime.now())

    def sort_target_key_fn(row):
        bl_list = ','.join(['%05d'% (int(s)%1000) for s in parse_subband_list(row[3].replace(':','..'))])
        return bl_list

    target_station_beams_sorted = sorted(target_station_beams, key=sort_target_key_fn)
    return [(beams_start_date.iso,
             beams_kill_date.iso,
             '%d' % bit_mode, '%.3f' % beam_start_interval_s)]\
             +ref_station_beams\
             +sorted(target_station_beams_sorted, key=lambda x: x[0][:5])






def beamctl_as_text(beams_list):
    r'''
    Write the beam setup (generated by beamctl_setup() ) to
    output_stream in a plain text format.

    **Parameters**

    beams_list : list of list of strings
        Generated by e.g. beamctl_setup(). First element is a header
        containing the beam start date, beam kill date, and
        beamstartup interval in seconds.

    output_stream : file object
        The stream to which to write the output.

    **Returns**

    String containing UTF-8 representation of beamctl beams.

    **Examples**

    >>> from .grids import penrose
    >>> bad = {'CS002HBA': [], 'RS509HBA': [], 'DE609HBA': [], 'CS301HBA': [], 'RS407HBA': [], 'DE604HBA': []}
    >>> target_3c196 = SkyCoord('8h13m36.0561s', '+48d13m02.636s', frame='icrs')
    >>> beams = beamctl_setup(target_skycoord=target_3c196,
    ...                       obs_start_date='2015-07-08 14:00:00',
    ...                       obs_gap_s=120.0,
    ...                       obs_duration_s=30.0,
    ...                       sub_band=280,
    ...                       rcu_filter='110_190',
    ...                       antenna_set='HBA_DUAL',
    ...                       clock_mhz=200,
    ...                       bit_mode=8,
    ...                       target_stations=['DE609'],
    ...                       reference_stations=['RS407'],
    ...                       bad_antenna_dict=bad,
    ...                       lm_grid_unit=array([[-1., 0.], [0., 0.], [1., 0.]]), verbose=False)
    >>> print(beamctl_as_text(beams))
    2015-07-08 13:59:35   2015-07-08 14:00:35   8   0.032
    RS407HBA     HBA_DUAL   280,280,280          0:2       0:95   110_190   2.153740377,0.84155237,J2000   2.153740377,0.84155237,J2000
    DE609HBA     HBA_DUAL    280            0      0:191   110_190   2.194137846,0.84864059,J2000   2.153740377,0.84155237,J2000
    DE609HBA     HBA_DUAL    280            1      0:191   110_190   2.153740377,0.84155237,J2000   2.153740377,0.84155237,J2000
    DE609HBA     HBA_DUAL    280            2      0:191   110_190   2.113747811,0.83355638,J2000   2.153740377,0.84155237,J2000
    '''
    header = '   '.join(beams_list[0])
    return '\n'.join([header]+['%8s   %10s   %4s   %10s   %8s   %2s   %s   %s' % words
                               for words in beams_list[1:]])
