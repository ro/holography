import logging

import numpy
from lofar.calibration.common.datacontainers import HolographyDataset
from lofar.calibration.common.datacontainers.holography_dataset_definitions \
    import HDS_data_sample_type

logger = logging.getLogger(__name__)


def average_values_by_sample(data_array, window_size, field_name=None):
    """
    Average the values present in the data_array with a given sample width
    :param data_array: input array
    :type data_array: numpy.ndarray
    :param window_size: window width in samples
    :type window_size: int
    :param field_name: name of the field to select
    :type field_name: str
    :return: an array containing the averaged values and the standard deviation
    :rtype: numpy.ndarray
    """

    new_array_size = _compute_size_of_new_array(len(data_array), window_size)

    # select the right field
    if field_name is not None:
        data_array = data_array[field_name]

    field_dtype = data_array.dtype
    if numpy.issubdtype(field_dtype, numpy.complexfloating):
        result = numpy.zeros(new_array_size, dtype=[('mean', field_dtype),
                                                    ('std_real', float),
                                                    ('std_imag', float),
                                                    ('averaged_samples', int)])

    else:
        result = numpy.zeros(new_array_size, dtype=[('mean', field_dtype),
                                                    ('std', field_dtype),
                                                    ('averaged_samples', int)])

    # compute the average and the std of all the steps except for the last one
    for i in range(new_array_size - 1):
        data_array_view = numpy.array(data_array[i * window_size: (i + 1) * window_size])

        result['mean'][i] = numpy.nanmean(data_array_view)
        if numpy.issubdtype(field_dtype, numpy.complexfloating):
            result['std_real'][i] = numpy.nanstd(numpy.real(data_array_view))
            result['std_imag'][i] = numpy.nanstd(numpy.imag(data_array_view))
        else:
            result['std'][i] = numpy.nanstd(data_array_view)
        # Counts the values that are not nan therefore the one used to do the mean
        result['averaged_samples'][i] = numpy.count_nonzero(~numpy.isnan(data_array_view))

    data_array_view = data_array[(new_array_size - 1) * window_size:]
    result['mean'][-1] = numpy.nanmean(data_array_view)
    if numpy.issubdtype(field_dtype, numpy.complexfloating):
        result['std_real'][-1] = numpy.nanstd(numpy.real(data_array_view))
        result['std_imag'][-1] = numpy.nanstd(numpy.imag(data_array_view))
    else:
        result['std'][-1] = numpy.nanstd(data_array_view)
    # Counts the values that are not nan therefore the one used to do the mean
    result['averaged_samples'][-1] = numpy.count_nonzero(~numpy.isnan(data_array_view))

    return result


def is_datatable_completely_flagged(data_table: numpy.ndarray):
    """
    Checks if the datatable is entirely flagged
    :param data_table: input data
    :return: if the data is completely flagged
    :rtype: bool
    """
    flags = numpy.array(data_table)['flag']
    return numpy.product(flags)

def std_dtype_from_array(data_table_in):
    field_names_std = []
    formats_std = []
    for field_name in data_table_in.dtype.names:
        field_dtype = data_table_in.dtype[field_name]
        # if the datatype is complex the standard deviation is computed only on the real(imaginary)
        # part
        if numpy.issubdtype(field_dtype, numpy.complexfloating):
            field_names_std.append(field_name + '_imag')
            formats_std.append(float)

            field_names_std.append(field_name + '_real')
            formats_std.append(float)
        else:
            field_names_std.append(field_name)
            formats_std.append(field_dtype)

    field_names_std += ['averaged_samples']
    formats_std += [int]

    return  numpy.dtype(dict(names=field_names_std, formats=formats_std))

def value_dtype_from_array(data_table_in):
    field_names_mean = []
    formats_mean = []
    for field_name in data_table_in.dtype.names:

        field_names_mean.append(field_name)
        field_dtype = data_table_in.dtype[field_name]
        formats_mean.append(field_dtype)
        # if the datatype is complex the standard deviation is computed only on the real(imaginary)
        # part

    field_names_mean += ['averaged_samples']
    formats_mean += [int]

    return numpy.dtype(dict(names=field_names_mean, formats=formats_mean))

def _average_datatable_with_function(data_table_in, parameter, expected_array_size, function):
    mean_output_data_table_dtype = value_dtype_from_array(data_table_in)
    std_output_data_table_dtype = std_dtype_from_array(data_table_in)
    output_array_average = numpy.zeros((expected_array_size,), dtype=mean_output_data_table_dtype)
    output_array_std = numpy.zeros((expected_array_size,), dtype=std_output_data_table_dtype)

    fields_to_average = list(data_table_in.dtype.names)
    fields_to_average.remove('flag')

    is_flagged = is_datatable_completely_flagged(data_table_in)

    for field_name in fields_to_average:
        datatable_values = numpy.array(data_table_in)
        # set flagged values to nan
        if field_name not in ['t', 'l', 'm']:
            datatable_values[field_name][data_table_in['flag']] = numpy.nan

        averaged_content = function(datatable_values, parameter, field_name=field_name)

        output_array_average[field_name] = averaged_content['mean']

        field_dtype = data_table_in.dtype[field_name]

        if numpy.issubdtype(field_dtype, numpy.complexfloating):
            output_array_std[field_name + '_real'] = averaged_content['std' + '_real']
            output_array_std[field_name + '_imag'] = averaged_content['std' + '_imag']
        else:
            output_array_std[field_name] = averaged_content['std']
        # flag the sample if the samples used are less that the window size
        output_array_average['averaged_samples'] = averaged_content['averaged_samples']
        output_array_std['averaged_samples'] = averaged_content['averaged_samples']
    output_array_average['flag'] = is_flagged
    output_array_std['flag'] = is_flagged

    return dict(mean=output_array_average, std=output_array_std)


def average_datatable_by_sample(data_table_in, window_size):
    """
    Average the datatable with a given sample window width
    :param data_table_in: the datatable to be averaged
    :type data_table_in: numpy.ndarray
    :param window_size: window width in samples
    :return: the array with the averaged values and the array with the standard deviations
    :rtype: dict(str:numpy.ndarray)
    """
    new_array_size = _compute_size_of_new_array(len(data_table_in), window_size)
    result = _average_datatable_with_function(data_table_in,
                                              window_size,
                                              new_array_size,
                                              average_values_by_sample)
    result['averaged_samples'] = window_size
    return result


def average_datatable(data_table_in):
    """
    Average the datatable with a given sample window width
    :param data_table_in: the datatable to be averaged
    :type data_table_in: numpy.ndarray
    :param window_size: window width in samples
    :return: the array with the averaged values and the array with the standard deviations
    :rtype: dict(str:numpy.ndarray)
    """
    window_size = len(data_table_in)
    new_array_size = 1
    result = _average_datatable_with_function(data_table_in,
                                              window_size,
                                              new_array_size,
                                              average_values_by_sample)
    result['averaged_samples'] = window_size
    return result


def calculate_window_size(data_table_in, time_interval):
    assert (time_interval > 0)

    max_t, min_t = numpy.max(data_table_in['t']), numpy.min(data_table_in['t'])
    assert (max_t > min_t)

    samples = len(data_table_in['t'])
    assert (samples > 0)

    average_samples_dt = (max_t - min_t) / float(samples)
    window_size = int(time_interval / average_samples_dt)

    if window_size == 0:
        logger.warning('The time window %s is smaller than the sample interval %s', time_interval,
                       average_samples_dt)
        window_size = 1
    logging.debug('averaging with a sample size of %s', window_size)

    return window_size


def average_datatable_by_time_interval(data_table_in, time_interval):
    """
    Average the datatable with a given sample time interval window
    :param data_table_in: the datatable to be averaged
    :type data_table_in: numpy.ndarray
    :param time_interval: time interval window
    :type time_interval: float
    :return: the array with the averaged values and the array with the standard deviations
    :rtype: dict(str:numpy.ndarray)
    """
    window_size = calculate_window_size(data_table_in, time_interval)
    new_array_size = _compute_size_of_new_array(len(data_table_in), window_size)
    result = _average_datatable_with_function(data_table_in,
                                              window_size,
                                              new_array_size,
                                              average_values_by_sample)
    result['averaged_samples'] = window_size
    return result


def _average_dataset_by_function(dataset, function, *parameters):
    out_data = {}
    for reference_station, data_per_reference_station in dataset.items():

        if reference_station not in out_data:
            out_data[reference_station] = dict()

        out_data_per_reference_station = out_data[reference_station]

        for frequency_string, data_per_frequency in data_per_reference_station.items():

            if frequency_string not in out_data_per_reference_station:
                out_data_per_reference_station[frequency_string] = dict()

            outdata_per_beamlet = out_data_per_reference_station[frequency_string]

            for beamlet, data_per_beamlet in data_per_frequency.items():
                logger.debug('processing reference station=%s, frequency=%s, beamlet=%s with %s',
                             reference_station,
                             frequency_string,
                             beamlet,
                             function.__name__)
                outdata_per_beamlet[beamlet] = dict()
                averaged_datatable = function(data_per_beamlet, *parameters)
                outdata_per_beamlet[beamlet]['mean'] = averaged_datatable['mean']
                outdata_per_beamlet[beamlet]['std'] = averaged_datatable['std']
                outdata_per_beamlet[beamlet]['ATTRIBUTES'] = {'AVERAGED_SAMPLES':
                                                                  averaged_datatable[
                                                                      'averaged_samples']}
    return out_data


def average_dataset_by_sample(input_data_set, window_size):
    """
    Averaged the dataset with a given sample window width
    :param input_data_set: holography dataset, either in an HDF5 file or as an
    HDS in memory
    :type input_data_set: str (file name) or HolographyDataset
    :param window_size: window width in samples
    :type window_size: int (enforced!)
    :return: the averaged data set structured in a dict of dicts.  Keys are
        [the reference station name]
            [the frequency]
                [the beamlet]
                    mean : -> containing the averaged results
                    std : -> containing the standard deviation of the averaged results
    :rtype: HolographyDataset
    """
    if input_data_set is not None:
        if isinstance(input_data_set, HolographyDataset) is True:
            dataset = input_data_set
        elif isinstance(input_data_set, str):
            dataset = HolographyDataset.load_from_file(input_data_set)
        else:
            text = "Cannot average data by samples!  The input data is neither an HDS nor a string."
            logger.error(text)
            raise ValueError(text)
    else:
        text = "Cannot average data by samples!  The input data is of the invalid type %s." % (
            type(input_data_set))
        logger.error(text)
        raise ValueError(text)

    if isinstance(window_size, int) is False or window_size < 1:
        text = "Cannot average data by samples!  The window size must be an integer value bigger than 0."
        logger.error(text)
        raise ValueError(text)

    out_data = _average_dataset_by_function(dataset.data, average_datatable_by_sample, window_size)

    if dataset.derived_data is None:
        dataset.derived_data = dict()
    dataset.derived_data['PRE_NORMALIZATION_AVERAGE_TIME'] = out_data
    dataset.derived_data['PRE_NORMALIZATION_AVERAGE_TIME']['ATTRIBUTES'] = \
        {"AVERAGING_WINDOW_SAMPLES": numpy.array(window_size, dtype=int)}
    return dataset


def average_dataset_by_time(input_data_set, time_interval):
    """
    Averaged the dataset with a given time interval in seconds
    :param input_data_set: holography dataset, either in an HDF5 file or as an
    HDS in memory
    :type input_data_set: str (file name) or HolographyDataset
    :param time_interval: average width in seconds
    :type time_interval: float (enforced!)
    :return: the averaged data set structured in a dict of dicts which keys are
        [the reference station name]
            [the frequency]
                [the beamlet]
                    mean : -> containing the averaged results
                    std : -> containing the standard deviation of the averaged results
    :rtype: HolographyDataset
    """
    if input_data_set is not None:
        if isinstance(input_data_set, HolographyDataset) is True:
            dataset = input_data_set
        elif isinstance(str, input_data_set):
            dataset = HolographyDataset.load_from_file(input_data_set)
        else:
            text = "Cannot average data by time!  The input data is neither an HDS not a string."
            logger.error(text)
            raise ValueError(text)
    else:
        text = "Cannot average data by time!  The input data is of the invalid type %s." % (
            type(input_data_set))
        logger.error(text)
        raise ValueError(text)

    if isinstance(float, time_interval) is False or time_interval < 0:
        text = "Cannot average data by samples!  The time interval must be positive."
        logger.error(text)
        raise ValueError(text)

    out_data = _average_dataset_by_function(dataset.data, average_datatable_by_time_interval,
                                            time_interval)
    if dataset.derived_data is None:
        dataset.derived_data = dict()
    dataset.derived_data['PRE_NORMALIZATION_AVERAGE_TIME'] = out_data
    dataset.derived_data['PRE_NORMALIZATION_AVERAGE_TIME']['ATTRIBUTES'] = \
        {"AVERAGING_WINDOW_INTERVAL": time_interval}
    return dataset


def average_data_by_sample(input_data_table, samples):
    out_data = _average_dataset_by_function(input_data_table, average_datatable_by_sample,
                                            samples)
    return out_data


def average_data(input_data_table):
    """

    :param input_data_table: dict(dict(dict(numpy.ndarray)
    :type input_data_table: dict(dict(dict(numpy.ndarray)
    :return:
    :rtype: dict(dict(dict(numpy.ndarray)
    """
    out_data = _average_dataset_by_function(input_data_table, average_datatable)
    return out_data


def _compute_size_of_new_array(array_size, window_size):
    """
    Round up the array size given a window size.
    If array_size % window_size = 0 it returns array_size/window_size
    otherwise it returns array_size/window_size + 1
    :param array_size: size of the input array
    :param window_size: window size for the averaging
    :return: the new array size after averaging with the given window size
    """
    # python does not enforce the sample_width to be an integer and a value between 0 and 1
    # will create an oversampling of the array
    assert window_size >= 1

    new_array_size = int(array_size // window_size)
    # add one if the len of the data_array is not divisible by the sample_width
    new_array_size += 1 if array_size % window_size else 0
    return new_array_size


def _weighted_average_data_per_polarization(input_dataset, output_dataset,
                                            sigma,
                                            std_real, std_imag, polarization):
    mask = input_dataset['flag'] == False

    try:
        real_weights_are_zeros_mask = std_real[polarization][mask] == 0.
        imag_weights_are_zeros_mask = std_imag[polarization][mask] == 0.


        real_weights = 1./std_real[polarization][mask]
        imag_weights = 1./std_imag[polarization][mask]

        real_weights[real_weights_are_zeros_mask] = 1
        imag_weights[imag_weights_are_zeros_mask] = 1

        output_dataset[polarization] = numpy.average(
            input_dataset[polarization][mask].real,
            weights=real_weights) \
            + 1.j * numpy.average(
            input_dataset[polarization][mask].imag,
            weights=imag_weights)
        sigma[polarization] = numpy.sqrt(numpy.average(
            (output_dataset[polarization].real - input_dataset[polarization][mask].real)**2.,
            weights=real_weights)) \
            + 1.j * numpy.sqrt(numpy.average(
            (output_dataset[polarization].imag - input_dataset[polarization][mask].imag)**2.,
            weights=imag_weights))
    except ZeroDivisionError as e:
        logger.warning('error in weighted average : %s', e)
        output_dataset['flag'] = True


def __prepare_input_to_weight_dataset(stations, frequency, beam,
                                      input_data_table):
    """
    Prepare the input data for the weighted average
    :param stations: list of stations to iterate on
    :type stations: (str)
    :param frequency: frequency ID
    :type frequency: str
    :param beam: beam ID
    :type beam: str
    :param input_data_table: data to iterate over
    :return: the average values,
     the standard deviation of the real part,
      the standard deviation of the imaginary part
    :rtype: list(numpy.ndarray, numpy.ndarray, numpy.ndarray)
    """

    n_stations = len(stations)

    average_per_beam_station = numpy.zeros(n_stations, dtype=HDS_data_sample_type)
    std_real_per_beam_station = numpy.zeros(n_stations, dtype=HDS_data_sample_type)
    std_imag_per_beam_station = numpy.zeros(n_stations, dtype=HDS_data_sample_type)

    for i, station in enumerate(stations):
        data_per_station_frequency_beam = \
            input_data_table[station][str(frequency)][beam]
        average_per_beam_station['XX'][i] = data_per_station_frequency_beam['mean']['XX']
        average_per_beam_station['XY'][i] = data_per_station_frequency_beam['mean']['XY']
        average_per_beam_station['YX'][i] = data_per_station_frequency_beam['mean']['YX']
        average_per_beam_station['YY'][i] = data_per_station_frequency_beam['mean']['YY']

        average_per_beam_station['l'][i] = data_per_station_frequency_beam['mean']['l']
        average_per_beam_station['m'][i] = data_per_station_frequency_beam['mean']['m']
        average_per_beam_station['flag'][i] = data_per_station_frequency_beam['mean']['flag']
        average_per_beam_station['t'][i] = data_per_station_frequency_beam['mean']['t']

        std_real_per_beam_station['XX'][i] = \
            data_per_station_frequency_beam['std']['XX_real']
        std_real_per_beam_station['XY'][i] = \
            data_per_station_frequency_beam['std']['XY_real']
        std_real_per_beam_station['YX'][i] = \
            data_per_station_frequency_beam['std']['YX_real']
        std_real_per_beam_station['YY'][i] = \
            data_per_station_frequency_beam['std']['YY_real']

        std_imag_per_beam_station['XX'][i] = \
            data_per_station_frequency_beam['std']['XX_imag']
        std_imag_per_beam_station['XY'][i] = \
            data_per_station_frequency_beam['std']['XY_imag']
        std_imag_per_beam_station['YX'][i] = \
            data_per_station_frequency_beam['std']['YX_imag']
        std_imag_per_beam_station['YY'][i] = \
            data_per_station_frequency_beam['std']['YY_imag']
    return average_per_beam_station, std_real_per_beam_station, std_imag_per_beam_station


def weighted_average_dataset_per_station(dataset, input_data_table):
    """
    Perform a weighted average of the dataset using the std deviations in the input data
    :param dataset: input dataset
    :type dataset: HolographyDataset
    :param input_data_table: input datatable
    :type input_data_table: dict(dict(numpy.ndarray))
    :return:
    """
    stations = dataset.reference_stations()
    first_station = stations[0]

    frequencies = dataset.frequencies()

    result_ALL = dict()
    result = dict(all=result_ALL)
    for frequency in frequencies:
        frequency_string = str(frequency)
        result_per_frequency = dict()
        result_ALL[str(frequency)] = result_per_frequency
        for beam_str in input_data_table[first_station][frequency_string]:

            result_per_beam = numpy.zeros(1, dtype=HDS_data_sample_type)
            std_per_beam = numpy.zeros(1, dtype=HDS_data_sample_type)
            average_per_beam_station, std_real_per_beam_station, std_imag_per_beam_station = \
                __prepare_input_to_weight_dataset(stations, frequency, beam_str, input_data_table)

            for polarization in ['XX', 'XY', 'YX', 'YY']:
                _weighted_average_data_per_polarization(average_per_beam_station,
                                                        result_per_beam,
                                                        std_per_beam,
                                                        std_real_per_beam_station,
                                                        std_imag_per_beam_station,
                                                        polarization)

            result_per_beam['l'] = numpy.average(average_per_beam_station['l'])
            result_per_beam['m'] = numpy.average(average_per_beam_station['m'])
            result_per_beam['flag'] = numpy.average(average_per_beam_station['flag']) >= 1.0
            result_per_beam['t'] = numpy.average(average_per_beam_station['t'])

            result_per_frequency[beam_str] = dict(mean=result_per_beam, std=std_per_beam)
    return result
