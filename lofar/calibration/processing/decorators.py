import functools
import sys


def log_step_execution(step_description, logger):
    def log_step_decorator(function):
        @functools.wraps(function)
        def wrapper(input, *args, **kwargs):
            logger.info('Performing step %s', step_description)
            try:
                result = function(input, *args, **kwargs)
                logger.info('Step %s performed', step_description)
                return result
            except Exception as e:
                logger.exception('exception occurred performing step %s: %s',
                                 step_description, e)
                sys.exit(1)
        return wrapper
    return log_step_decorator