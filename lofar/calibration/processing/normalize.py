import logging

import numpy
from lofar.calibration.common.datacontainers import HolographyDataset

logger = logging.getLogger(__name__)

__POLARIZATION_TO_INDEX = {
    'XX': (0, 0),
    'XY': (0, 1),
    'YX': (1, 0),
    'YY': (1, 1)
}


def normalize_beams_by_central_one(dataset, input_data, central_beamlet_number):
    """

    :param dataset:
    :type dataset: HolographyDataset
    :param central_beamlet_number: number of the central beamleat
    :type central_beamlet_number: dict[str, float]
    :param input_data: input data to be processed
    :type input_data: dict(dict(numpy.ndarray))
    :return:
    """
    normalized_data = dict()
    for station in dataset.reference_stations():
        normalized_data_per_station = dict()
        normalized_data[station] = normalized_data_per_station
        data_per_station = input_data[station]

        for frequency in dataset.frequencies():
            frequency_str = str(frequency)
            data_per_station_per_frequency = data_per_station[frequency_str]

            normalized_data_per_station_frequency = \
                normalize_crosscorrelation_beams(data_per_station_per_frequency,
                                                 central_beamlet_number[frequency_str])

            normalized_data_per_station[frequency_str] = normalized_data_per_station_frequency
    return normalized_data


def __invert_central_beam(data_per_station_frequency, central_beamlet):
    """
    Invert the crosscorrelation of the central beamlet
    :param data_per_station_frequency: input data array
    :param central_beamlet: index of the central beamlet
    :return: if the normalization was successfull and the result of the inversion
    :rtype: (bool, numpy.ndarray)
    """

    is_inversion_matrix_valid = True
    normalization_array = None
    try:
        normalization_array = invert_central_beam_crosscorrelations(data_per_station_frequency,
                                                                    central_beamlet)
    except numpy.linalg.LinAlgError as e:
        logger.warning('error inverting central beamlet crosscorrelation for beamlet %s: %s',
                       central_beamlet,
                       e)
        print('error inverting central beamlet crosscorrelation for beamlet %s: %s' % (
            central_beamlet,
            e))
        is_inversion_matrix_valid = False
    return is_inversion_matrix_valid, normalization_array


def __normalize_data_by_central_beam(normalization_array, data_per_beam, flag_data):
    """
    Normalizes the data by the central beam
    :param normalization_array: the used to normalize the data
    :param data_per_beam: data per beam
    :param flag_data: flag the data
    :return: the normalized data per beam
    """
    """
    TO avoid copying the data in a different array and then copy the data back 
    I compute directly the product of the two array of matrices

    the resulting cross polarization as a function of time are:
    XX = XX_b * XX_n + XY_b * YX_n
    XY = XX_b * XY_n + XY_b * YY_n
    YX = YX_b * XX_n + YY_b * YX_n
    YY = YX_b * XY_n + YY_b * YY_n

    where b is the beam and n is the normalization array
    """
    normalized_data = numpy.array(data_per_beam)

    if flag_data:
        normalized_data['flag'] = True
        return normalized_data

    normalized_data['XX'] = \
        data_per_beam['XX'] * normalization_array[__POLARIZATION_TO_INDEX['XX']] + \
        data_per_beam['XY'] * normalization_array[__POLARIZATION_TO_INDEX['YX']]
    normalized_data['XY'] = \
        data_per_beam['XX'] * normalization_array[__POLARIZATION_TO_INDEX['XY']] + \
        data_per_beam['XY'] * normalization_array[__POLARIZATION_TO_INDEX['YY']]
    normalized_data['YX'] = \
        data_per_beam['YX'] * normalization_array[__POLARIZATION_TO_INDEX['XX']] + \
        data_per_beam['YY'] * normalization_array[__POLARIZATION_TO_INDEX['YX']]
    normalized_data['YY'] = \
        data_per_beam['YX'] * normalization_array[__POLARIZATION_TO_INDEX['XY']] + \
        data_per_beam['YY'] * normalization_array[__POLARIZATION_TO_INDEX['YY']]
    return normalized_data


def normalize_crosscorrelation_beams(data_per_station_frequency,
                                     central_beamlet):
    """
    Normalize the crosscorrelation per each beam given the normalization array computed
    from the central beam
    :param data_per_station_frequency: input data array
    :param central_beamlet: index of the central beamlet
    :return:
    """

    is_invertion_matrix_valid, normalization_array = __invert_central_beam(
        data_per_station_frequency,
        central_beamlet)

    normalized_data_per_beam = dict()
    for beam in data_per_station_frequency:
        data_per_beam = data_per_station_frequency[beam]
        normalized_data_per_beam[beam] = \
            __normalize_data_by_central_beam(normalization_array,
                                             data_per_beam,
                                             not is_invertion_matrix_valid)

    return normalized_data_per_beam


def invert_central_beam_crosscorrelations(data_per_station_per_frequency, central_beamlet_number):
    data_per_central_beamlet = __extract_crosscorrelation_matrices_from_data(
        data_per_station_per_frequency[central_beamlet_number])

    return __invert_crosscorrelation_matrices(data_per_central_beamlet)


def __extract_crosscorrelation_matrices_from_data(data_per_station_frequency_beam):
    new_shape = (data_per_station_frequency_beam.shape[0], 2, 2)
    new_array = numpy.zeros(shape=new_shape, dtype=numpy.complex)

    new_array[:, 0, 0] = data_per_station_frequency_beam['XX']
    new_array[:, 0, 1] = data_per_station_frequency_beam['XY']
    new_array[:, 1, 0] = data_per_station_frequency_beam['YX']
    new_array[:, 1, 1] = data_per_station_frequency_beam['YY']

    return new_array


def __invert_crosscorrelation_matrices(cross_correlation_matrices):
    """
    Invert the matrices along the last axis
    :param cross_correlation_matrices: matrices to invert
    :type cross_correlation_matrices: numpy.ndarray
    :return:
    """
    assert cross_correlation_matrices.ndim >= 3

    result_array = numpy.rollaxis(numpy.linalg.inv(cross_correlation_matrices), 0, 3)

    return result_array
