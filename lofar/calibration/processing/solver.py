import logging

import numpy
from scipy.constants import c as LIGHT_SPEED

import lofar.calibration.common.datacontainers as datacontainers
from lofar.calibration.common.coordinates import _SIGN_LMN_TO_PQR
from lofar.calibration.common.coordinates import _SIGN_PQR_TO_LMN

logger = logging.getLogger(__name__)

_FRQ_TO_PHASE = 2. * numpy.pi / LIGHT_SPEED


def _rotate_antenna_coordinates(dataset):
    """
    Rotate the antenna coordinates to be aligned to the station orientation
    :param dataset: input dataset
    :type dataset: datacontainers.HolographyDataset
    :return: the rotated coordinate system
    :rtype: numpy.array
    """
    station_position = numpy.array(dataset.target_station_position)
    antenna_field_position = numpy.array(dataset.antenna_field_position())

    antenna_position_offset = numpy.array([station_position - antenna_position for
                                           antenna_position in antenna_field_position])

    station_rotation_matrix = dataset.rotation_matrix()
    return numpy.dot(station_rotation_matrix, antenna_position_offset.T).T


def _compute_expected_phase_delay(l, m, x, y, frequency):
    """
    Convert the antenna position (x,y) and the pointing cosines (l,m) into a phase delay for
    the given frequency
    :param l: l pointing cosine
    :param m: m pointing cosine
    :param x: x displacement of the antenna
    :param y: y displacement of the antenna
    :param frequency: frequency under consideration
    :return:
    """
    phase = (x * l + y * m) * _FRQ_TO_PHASE * frequency
    return phase


def _compute_pointing_matrices_per_station_frequency_beam(dataset, datatable, central_beam,
                                                          frequency):
    """
    Compute the pointing matrix of a given station, at a given frequency and for a specific beam.
    :param dataset: datatable's dataset
    :type dataset: datacontainers.HolographyDataset
    :param datatable: input data
    :type datatable: dict(dict(numpy.ndarray))
    :param frequency: frequency at which to compute the pointing matrix
    :type frequency: float
    :param central_beam: central beam
    :type central_beam: str
    :return: the pointing matrix
    :rtype: numpy.array
    """

    rotated_coordinates = _rotate_antenna_coordinates(dataset)
    n_antennas = len(dataset.antenna_field_position())
    one_over_n_antennas = 1. / float(n_antennas)
    n_beams = len(datatable)

    pointing_matrix = numpy.array(numpy.zeros((n_beams, n_antennas), dtype=complex))

    for i, beam_str in enumerate(datatable):
        for j in range(n_antennas):
            l = datatable[central_beam]['mean']['l'][0] - datatable[beam_str]['mean']['l'][0]
            m = datatable[central_beam]['mean']['m'][0] - datatable[beam_str]['mean']['m'][0]
            x, y = rotated_coordinates[j, 0:2]

            phase = _compute_expected_phase_delay(l, m, x, y, frequency)
            pointing_matrix[i, j] = numpy.exp(_SIGN_PQR_TO_LMN*1j * phase) * one_over_n_antennas

    return pointing_matrix


def _invert_matrix_lstsqr(matrix, visibilities, sigmas=None, rcond=None, **kwargs):
    try:
        visibilities = visibilities.squeeze()
        sigmas = sigmas.squeeze()
        gains, residual, _, _ = numpy.linalg.lstsq(matrix, visibilities, rcond=rcond)
        residual_high = numpy.linalg.inv(matrix.T @ matrix) @ matrix.T @ (visibilities + sigmas)
        residual_low = numpy.linalg.inv(matrix.T @ matrix) @ matrix.T @ (visibilities - sigmas)
        residual = abs(residual_high.real - residual_low.real) * .5 + .5j * abs(residual_high.imag - residual_low.imag)
        gains = gains.squeeze()
    except ValueError as e:
        raise numpy.linalg.LinAlgError(e)
    return gains, residual


def _invert_matrix_weighted_lstsqr(matrix, visibilities, sigmas=None, rcond=None, **kwargs):
    try:
        visibilities = visibilities.squeeze()
        sigmas = sigmas.squeeze()
        weights = numpy.diag(1. / abs(sigmas) ** 2.)
        scaled_visibilities = weights @ visibilities
        scaled_matrix = weights @ matrix
        effective_matrix = scaled_matrix.T @ scaled_matrix
        effective_known_value = scaled_matrix.T @ scaled_visibilities
        gains, residual, _, _ = numpy.linalg.lstsq(effective_matrix, effective_known_value, rcond=rcond)
        residual_high = numpy.linalg.inv(matrix.T @ matrix) @ matrix.T @ (visibilities + sigmas)
        residual_low = numpy.linalg.inv(matrix.T @ matrix) @ matrix.T @ (visibilities - sigmas)
        residual = abs(residual_high.real - residual_low.real) * .5 + .5j * abs(residual_high.imag - residual_low.imag)
        gains = gains.squeeze()
    except ValueError as e:
        raise numpy.linalg.LinAlgError(e)
    return gains, residual


def _invert_matrix_direct(matrix, visibilities, rcond=None, **kwargs):
    try:
        # $(M^T M)^{-1}M^T$
        inverse_matrix = numpy.dot(numpy.linalg.inv(numpy.dot(matrix.T, matrix)), matrix.T)
        gains = numpy.dot(inverse_matrix, visibilities)
        # IS the same as : gains = matrix.I * visibilities
        residual = 0

    except ValueError as e:
        raise numpy.linalg.LinAlgError(e)
    return gains, residual


def pointing_model(gains, matrix):
    abs = gains[::2]
    phase = gains[1::2]
    gains = abs * numpy.exp(1.j * phase)

    return numpy.dot(matrix, gains)


def _likelihood(gains, matrix, visibilities, error_visibilities):
    # sigma2 = error_visibilities ** 2
    model = pointing_model(gains, matrix)
    visibilities = visibilities.flatten()
    diff = (visibilities - model)

    diff_real = (diff.real / error_visibilities.real) ** 2.
    diff_imag = (diff.imag / error_visibilities.imag) ** 2.

    diff = numpy.mean(diff_real + diff_imag)
    like = -numpy.exp(-diff / 2.)

    return like


def _likelihood_phase(phases, matrix, visibilities, error_phases):
    gains = numpy.exp(1.j * phases)
    model = matrix @ gains
    diff = numpy.mean(numpy.square(numpy.angle(visibilities) - numpy.angle(model)))
    return -numpy.exp(-diff ** 2)


def _likelihood_amp(amp, phases, matrix, visibilities, error_phases):
    gains = amp * numpy.exp(1.j * phases)
    model = matrix @ gains
    diff = numpy.mean(numpy.square(numpy.abs(visibilities) - numpy.abs(model)))
    return -numpy.exp(-diff ** 2)


def initial_guess_gains_phase(n_antennas):
    phase = numpy.ones(n_antennas, dtype=float)
    return phase


def initial_guess_gains_abs(n_antennas):
    rho = numpy.ones(n_antennas, dtype=float)
    return rho


def _invert_matrix_mcmc(matrix, visibilities, sigmas=None, **kwargs):
    from scipy.optimize import minimize
    rho = initial_guess_gains_abs(matrix.shape[1])
    phase = initial_guess_gains_phase(matrix.shape[1])
    optimize_result = minimize(_likelihood_phase, phase, args=(matrix, visibilities, sigmas))
    phase = optimize_result.x

    optimize_result = minimize(_likelihood_amp, rho, args=(phase, matrix, visibilities, sigmas))
    rho = optimize_result.x
    gains = rho * numpy.exp(1.j * phase)
    return gains, numpy.zeros(10)


def _invert_matrix(matrix, visibilities, type='LSTSQR', **kwargs):
    """
    Invert the pointing to find the gains from the visibilities.
    It is possible to specify the type of solution method applied through the parameter type
    :param matrix: pointing matrix
    :param visibilities: visibilities for each pointing
    :param type: type of methodology used to invert the matrix
    :param kwargs: additional parameters for the solution method
    :return:
    """
    SOLUTION_METHODS = ['LSTSQR', 'MCMC', 'DIRECT', 'WEIGHTED_LSTSQR']
    if type not in SOLUTION_METHODS:
        raise ValueError('wrong type of solution method specified. Alternatives are: %s'
                         % SOLUTION_METHODS)

    if type == 'LSTSQR':
        return _invert_matrix_lstsqr(matrix, visibilities, **kwargs)
    elif type == 'WEIGHTED_LSTSQR':
        return _invert_matrix_weighted_lstsqr(matrix, visibilities, **kwargs)
    elif type == 'MCMC':
        return _invert_matrix_mcmc(matrix, visibilities, **kwargs)
    elif type == 'DIRECT':
        return _invert_matrix_direct(matrix, visibilities, **kwargs)


def _solve_gains(visibilities, matrix, **kwargs):
    """
    SOLVE THE EQUATION M * G = V FOR G
    where M is the pointing matrix
          G are the gains per antenna
          V are the visibilities
    :param visibilities: the visibility computed for each pointing
    :type visibilities: numpy.array
    :param matrix: the pointing matrix containing the phase delay for each pointing and antenna
    :type matrix: numpy.array
    :return: the gains for each antenna
    """
    __empty = dict(gains=numpy.array([numpy.nan]),
                   residual=numpy.array([numpy.nan]),
                   relative_error=numpy.array([numpy.nan]),
                   flag=numpy.array(True))
    try:
        if len(visibilities) == 0:
            return __empty

        gains, residual = _invert_matrix(matrix, visibilities, **kwargs)
        noise = abs(numpy.dot(matrix, gains) - visibilities) / abs(visibilities)

        return dict(gains=gains, residual=residual, relative_error=noise,
                    flag=numpy.array(False))
    except numpy.linalg.LinAlgError as e:
        logger.warning('error solving for the gains: %s - %s', e.__class__.__name__, e)
        logger.exception(e)

        return __empty


def _solve_gains_per_frequency(dataset, datatable, frequency, **kwargs):
    """
    SOLVE THE EQUATION M * G = V FOR G
    :param dataset:
    :type dataset: datacontainers.HolographyDataset
    :param datatable:
    :param frequency:
    :type frequency: float
    :return:
    """

    central_beam = dataset.central_beamlets()[str(frequency)]
    matrix = _compute_pointing_matrices_per_station_frequency_beam(dataset,
                                                                   datatable,
                                                                   central_beam,
                                                                   frequency)
    result = dict()

    flags = numpy.array([datatable[beam_str]['mean']['flag'] for beam_str in datatable])
    #skips using the central beam
    flags[int(central_beam), 0] = True
    matrix = __remove_flagged_data_from_matrix(matrix, flags)

    for polarization in ['XX', 'XY', 'YX', 'YY']:
        visibilities = numpy.array(
            [datatable[beam_str]['mean'][polarization] for beam_str in datatable])
        sigmas = numpy.array(
            [datatable[beam_str]['std'][polarization] for beam_str in datatable])

        visibilities, sigmas = __remove_flagged_data(visibilities, sigmas, matrix, flags)

        result[polarization] = _solve_gains(visibilities, matrix, sigmas=sigmas, **kwargs)

    return result


def __remove_flagged_data(visibilities, sigmas, matrix, flags):
    not_flagged = numpy.where(flags == False)[0]
    visibilities = visibilities[not_flagged]
    sigmas = sigmas[not_flagged]

    return visibilities, sigmas


def __remove_flagged_data_from_matrix(matrix, flags):
    not_flagged = numpy.where(flags == False)[0]
    matrix_new = numpy.zeros([len(not_flagged), matrix.shape[1]], dtype=matrix.dtype)
    for new_index, index in enumerate(not_flagged):
        matrix_new[new_index, :] = matrix[index, :]
    return matrix_new


def solve_gains_per_datatable(dataset, datatable, **kwargs):
    """
    Solve for the gains the given datatable
    :param dataset: dataset containing the specified datatable
    :type dataset: datacontainers.HolographyDataset
    :param datatable: table containing the data
    :type dataset: dict(dict(numpy.ndarray))
    :return: a nested dict containing the gain matrix per station, frequency, polarization
    :rtype: dict(dict(dict(numpy.ndarray)))
    """
    result = dict()

    for station in datatable:
        result_per_station = dict()
        result[station] = result_per_station
        data_per_station = datatable[station]
        for frequency in data_per_station:
            data_per_frequency = data_per_station[frequency]
            result_per_station[str(frequency)] = \
                _solve_gains_per_frequency(dataset, data_per_frequency, float(frequency), **kwargs)

    return result
