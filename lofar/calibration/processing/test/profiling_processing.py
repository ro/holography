#!/usr/bin/env python
import cProfile
import pstats
from io import StringIO

import lofar.calibration.processing as processing
from lofar.calibration.common.datacontainers import HolographyDataset
# READ doc/Holography_Data_Set.md!  It contains the location from which the
# test data must be downloaded and this file generated.
SAMPLE_DATASET = '/data/test/HolographyObservation/CS001HBA0.hdf5'


class ProfilingTest(object):
    def __init__(self):
        super(ProfilingTest, self).__init__()
    
    def pre_setup(self):
        self._profiler =  cProfile.Profile()
        self._profiler.enable()

    def setup(self):
        pass

    def tear_down(self):
        pass

    def post_teardown(self):
        self._profiler.disable()
        self.string_results = StringIO.StringIO()
        sortby = 'cumulative'
        self.result = pstats.Stats(self._profiler, stream=self.string_results).sort_stats(sortby)
        self.result.print_stats()

    def print_results(self):
        print(self.string_results.getvalue())

    def process(self):
        pass

    def run(self):
        self.pre_setup()
        self.setup()
        self.process()
        self.tear_down()
        self.post_teardown()
        return self


class profile_holography_dataset_read(ProfilingTest):
    def process(self):
        HolographyDataset.load_from_file(SAMPLE_DATASET)


class profile_averaging_per_sample(ProfilingTest):
    def process(self):
        processing.average_dataset_by_sample(SAMPLE_DATASET, 2)


if __name__ == '__main__':
    profile_holography_dataset_read().run().print_results()
    profile_averaging_per_sample().run().print_results()
