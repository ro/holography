import numpy
from collections import defaultdict

from lofar.calibration.common.datacontainers.holography_dataset import HolographyDataset
from lofar.calibration.common.datacontainers.holography_dataset_definitions import HDS_coordinate_type, HDS_data_sample_type

DEFAULT_TARGET_STATION = 'CS001'
DEFAULT_TARGET_STATION_POSITION = numpy.array([0, 0, 0])

DEFAULT_REFERENCE_STATIONS = ('RS301', 'RS302')
DEFAULT_SOURCE_NAME = 'test source'
DEFAULT_SOURCE_POINTING = numpy.array([(1.5, 0.9, 'J2000')], HDS_coordinate_type)
DEFAULT_SAS_IDS = numpy.array([123456])
DEFAULT_SOURCE_POSITION = numpy.array([3826896.19174 ,  460979.502113, 5064658.231])
DEFAULT_BEAMLETS = numpy.array([0, 1, 2, 3 , 4])
DEFAULT_FREQUENCIES = [150000.0]
DEFAULT_POINTINGS = {str(DEFAULT_FREQUENCIES[0]): {
        '0': numpy.array([(1.5, 0.9, 'J2000')], HDS_coordinate_type),
        '1': numpy.array([(2.0, 0.9, 'J2000')], HDS_coordinate_type),
        '2': numpy.array([(1.0, 0.9, 'J2000')], HDS_coordinate_type),
        '3': numpy.array([(1.0, 0.8, 'J2000')], HDS_coordinate_type),
        '4': numpy.array([(1.0, 1, 'J2000')], HDS_coordinate_type)}}

DEFAULT_ANTENNA_FIELD_POSITION = numpy.array([[1, 0, 1],
                                              [-1, 0, 1],
                                              [+1, 0, 1]])

DEFAULT_ROTATION_MATRIX = numpy.array([[-1.19595e-01,  9.92823e-01,  3.30000e-05],
         [-7.91954e-01, -9.54190e-02,  6.03078e-01],
         [ 5.98753e-01,  7.20990e-02,  7.97682e-01]])

DEFAULT_MODE = 5
DEFAULT_TIME_SAMPLE = [1, 2, 3, 4, 5, 6]


class TestComplexGaussianNoise():
    def __init__(self, average, std_real, std_imag, **params):
        """
        :param average: average value
        :type average: complex
        :param std_real: standard deviation for the real part
        :type std_real: float
        :param std_imag: standard deviation for the imaginary part
        :type std_imag: float
        """
        self.average = average
        self.std_real = std_real
        self.std_imag = std_imag
        self.params = params

    def generate(self, sample_size, seed=None):
        """
        Generate a series of random values
        :param sample_size:
        :type sample_size: int
        :param seed:
        :type seed: int
        :return:
        """
        if seed:
            numpy.random.seed(seed)

        real = self.std_real * numpy.random.randn(sample_size)
        imag = self.std_imag * numpy.random.randn(sample_size) * 1.j

        return real + imag + self.average


class TestSignal():
    def __init__(self, function, noise):
        """

        :param function:
        :type function: function
        :param noise:
        :type noise: TestComplexGaussianNoise
        """
        self.function = function
        self.noise = noise

    def generate(self, timesamples, seed=None):
        """

        :param timesamples: list of time samples in which to compute the signal
        :type timesamples: Iterable[float]
        :param seed: seed for the gaussian signal generator
        :type seed: None|int
        :return: the generated sample signal
        """
        n_samples = len(timesamples)
        result = numpy.zeros(n_samples, dtype=complex)
        for i, timesample in enumerate(timesamples):
            result[i] = self.function(timesample)
        result += self.noise.generate(n_samples, seed)

        return result


DEFAULT_AVERAGE_VALUES_PER_STATION_AND_POINTING = {
    "RS301": {
        '150000.0': {
            '0': {
                "average": 1+5j,
                "std_imag": 1,
                "std_real": 2,
                "real_coeff": 0,
                "imag_coeff": 0
            },
            '1': {
                "average": 1 + 5j,
                "std_imag": 1,
                "std_real": 2,
                "real_coeff": 0,
                "imag_coeff": 0
            },
            '2': {
                "average": 1 + 5j,
                "std_imag": 1,
                "std_real": 2,
                "real_coeff": 0,
                "imag_coeff": 0
            },
            '3': {
                "average": 1 + 5j,
                "std_imag": 1,
                "std_real": 2,
                "real_coeff": 0,
                "imag_coeff": 0
            },
            '4': {
                "average": 1 + 5j,
                "std_imag": 1,
                "std_real": 2,
                "real_coeff": 0,
                "imag_coeff": 0
            }
        }
    },
    "RS302": {
        '150000.0': {
            '0': {
                "average": 1+5j,
                "std_imag": 1,
                "std_real": 2,
                "real_coeff": 0,
                "imag_coeff": 0
            },
            '1': {
                "average": 1 + 5j,
                "std_imag": 1,
                "std_real": 2,
                "real_coeff": 0,
                "imag_coeff": 0
            },
            '2': {
                "average": 1 + 5j,
                "std_imag": 1,
                "std_real": 2,
                "real_coeff": 0,
                "imag_coeff": 0
            },
            '3': {
                "average": 1 + 5j,
                "std_imag": 1,
                "std_real": 2,
                "real_coeff": 0,
                "imag_coeff": 0
            },
            '4': {
                "average": 1 + 5j,
                "std_imag": 1,
                "std_real": 2,
                "real_coeff": 0,
                "imag_coeff": 0
            }
        }
    }
}


def recursive_default_dict_generator():
    return defaultdict(recursive_default_dict_generator)


def generate_single_pointing_dataset(timesamples, options):
    """

    :param timesamples:
    :type timesamples: list
    :return:
    """
    dataset = numpy.zeros(len(timesamples), dtype=HDS_data_sample_type)
    for i, timestamp in enumerate(timesamples):
        dataset[i]['t'] = timestamp

    options['XX'] = TestComplexGaussianNoise(**options)
    options['XY'] = TestComplexGaussianNoise(**options)
    options['YX'] = TestComplexGaussianNoise(**options)
    options['YY'] = TestComplexGaussianNoise(**options)

    return dataset


def generate_sample_data(holography_dataset,
                         timesamples = DEFAULT_TIME_SAMPLE,
                         data_statistical_characteristics=DEFAULT_AVERAGE_VALUES_PER_STATION_AND_POINTING):
    """
    Generates a sample data to start testing the processing step
    :param holography_dataset: dataset prefilled with the header parameters
    :type holography_dataset: HolographyDataset
    :param timesamples:

    :return:
    """
    holography_dataset.data = recursive_default_dict_generator()

    for reference_station in holography_dataset.reference_stations():
        for frequency in holography_dataset.frequencies():
            frequency_string = str(frequency)
            for beamlet in holography_dataset.beamlets():
                beamlet_string = str(beamlet)
                holography_dataset.data[reference_station][frequency_string][beamlet_string] =\
                    generate_single_pointing_dataset(timesamples,
                                                     data_statistical_characteristics[reference_station][frequency_string][beamlet_string])




def create_holography_dataset(target_station_name=DEFAULT_TARGET_STATION,
                              target_station_position = DEFAULT_TARGET_STATION_POSITION,
                              reference_stations=DEFAULT_REFERENCE_STATIONS,
                              timesamples = DEFAULT_TIME_SAMPLE,
                              pointings=DEFAULT_POINTINGS,
                              sas_ids = DEFAULT_SAS_IDS,
                              source_name=DEFAULT_SOURCE_NAME,
                              source_position=DEFAULT_SOURCE_POINTING,
                              mode=DEFAULT_MODE,
                              beamlets=DEFAULT_BEAMLETS,
                              antenna_field_positions=DEFAULT_ANTENNA_FIELD_POSITION,
                              frequencies=DEFAULT_FREQUENCIES,
                              data_statistical_characteristics=DEFAULT_AVERAGE_VALUES_PER_STATION_AND_POINTING
                              ):
    """
    Creates a mock Holography Dataset that can be used for testing
    """
    dataset = HolographyDataset()
    dataset._mode = mode
    dataset._target_station_position = target_station_position
    dataset._target_station_name = target_station_name
    dataset._beamlets = beamlets
    dataset._source_position = source_position
    dataset._reference_stations = reference_stations
    dataset._antenna_field_position = antenna_field_positions
    dataset._source_name = source_name
    dataset._sas_ids = sas_ids
    dataset._ra_dec = pointings
    dataset._central_beamlets = dataset.find_central_beamlets(source_position,
                                                             pointings,
                                                             frequencies,
                                                             beamlets)
    dataset._frequencies = frequencies

    generate_sample_data(dataset, timesamples)
    return dataset
