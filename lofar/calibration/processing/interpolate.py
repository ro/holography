from typing import Generator, Dict, Tuple, Any
import numpy
import emcee
from scipy.optimize import curve_fit
from scipy.signal import find_peaks
import logging

__POSSIBLE_POLARIZATIONS = ('XX', 'YY')

logger = logging.getLogger(__name__)

PSET_TYPES = numpy.dtype([('m', numpy.float32),
                          ('q', numpy.float32),
                          ('sigma', numpy.float32)])


def wrap_linear(phase):
    return numpy.angle(numpy.cos(phase) + 1.j * numpy.sin(phase))


def __iterate_gains_over_station_polarization_frequency(dataset: Dict[str,
                                                                      Dict[str,
                                                                           Dict[str,
                                                                                numpy.ndarray]]]) -> \
        Generator[
            Tuple[
                Tuple[str, str],
                Tuple[float],
                numpy.ndarray], None, None]:
    for station_name, dataset_per_station_name in dataset.items():
        sorted_frequencies = sorted(map(float, dataset_per_station_name.keys()))

        for polarization in __POSSIBLE_POLARIZATIONS:
            data_per_polarization = list()
            available_frequencies = list()
            for frequency in sorted_frequencies:
                data_per_frequency = dataset_per_station_name[str(frequency)][polarization]
                if not data_per_frequency['flag']:
                    available_frequencies.append(frequency)
                    data_per_polarization.append(data_per_frequency['gains'])

            data_per_polarization = numpy.stack(data_per_polarization, axis=1)
            yield (station_name, polarization), tuple(available_frequencies), data_per_polarization


def _interpolate_gains_mc(x, y):
    x = numpy.array(x).flatten()
    amplitude = numpy.abs(y).flatten()

    phase = numpy.angle(y).flatten()

    amplitude_parameters, amp_mc_samples = _interpolate_mc_mc(x, amplitude, linear, _ln_likelihood_abs)
    phase_parameters, phase_mc_samples = _interpolate_mc_mc(x, phase, linear, _ln_likelihood_abs)

    result = dict(amplitude=dict(parameters=amplitude_parameters, mc_samples=amp_mc_samples),
                  phase=dict(parameters=phase_parameters, mc_samples=phase_mc_samples))

    return result


def phasor(frequency_hz, tau_s, sign=+1.0):
    return numpy.exp(sign * 2 * numpy.pi * 1j * (frequency_hz * tau_s))


def _fit_delays(x, y, min_freq=None, max_freq=None, tau_min=-30, tau_max=30, dtau=0.1, fit_phi_0=True):
    """

    x: frequency in Hz
    y: complex gains
    min_freq: low frequency cut
    max_freq: max frequency cut
    tau_max: in ns
    tau_min: in ns

    """
    phase_parameter = numpy.zeros(1, dtype=PSET_TYPES)
    NS_TO_S = 1.e-9
    f_min = x.min() - 1
    f_max = x.max() + 1

    if min_freq:
        f_min = min_freq
    if max_freq:
        f_max = max_freq

    selection = numpy.logical_and(x >= f_min, x < f_max)
    selection = numpy.logical_and(selection, numpy.absolute(y) >= 0.1)
    selection = numpy.logical_and(selection, numpy.absolute(y) <= 2.0)
    if selection.mean() < 0.5:
        return phase_parameter
    tau_s = numpy.arange(tau_min, (tau_max + dtau / 2), dtau) * NS_TO_S
    f_hz = x[selection]
    fourier = (phasor(f_hz[numpy.newaxis, :], tau_s[:, numpy.newaxis], sign=-1) * y[numpy.newaxis, selection]).mean(
        axis=1)
    max_idx = numpy.argmax(numpy.absolute(fourier))
    phi_0 = 0.0
    if fit_phi_0:
        phi_0 = numpy.angle(fourier[max_idx]) / numpy.pi / 2.

    phase_parameter['m'] = tau_s[max_idx]
    phase_parameter['q'] = phi_0

    return phase_parameter


def _interpolate_gains_fourier(x, y):
    x = numpy.array(x).flatten()
    amplitude = numpy.abs(y).flatten()
    phase = numpy.angle(y).flatten()
    complex_gains = numpy.exp(1.j * phase)

    res, _ = curve_fit(linear, x, amplitude)
    amplitude_parameters = numpy.zeros(1, dtype=PSET_TYPES)
    amplitude_parameters['m'], amplitude_parameters['q'] = res
    phase_parameters = _fit_delays(x, complex_gains)

    result = dict(amplitude=dict(parameters=amplitude_parameters),
                  phase=dict(parameters=phase_parameters))

    return result


def __interpolate_gains_per_antenna(dataset: Dict[str, Dict[str, Dict[str, numpy.ndarray]]],
                                    func=_interpolate_gains_fourier):
    data = {}
    for (station, polarization), frequencies, dataset in __iterate_gains_over_station_polarization_frequency(dataset):
        if station not in data:
            data_per_station = {}
            data[station] = data_per_station
        data_per_station = data[station]
        data_per_polarization = dict()
        data_per_station[polarization] = data_per_polarization

        for antenna_id, data_per_antenna in enumerate(dataset):
            logger.info('interpolating gains for polarization %s antenna %s', polarization,
                        antenna_id)
            interpolated_data = func(frequencies, data_per_antenna)
            data_per_polarization[str(antenna_id)] = interpolated_data
    return data


def _ln_likelihood_abs(parameters, x, y):
    m, q, sigma = parameters
    y_model = m * x + q

    inv_variance = sigma ** - 2.
    residual = numpy.square(y - y_model) * inv_variance

    ln_likelihood = -.5 * numpy.sum(residual - numpy.log(inv_variance))
    return ln_likelihood


def linear(x, m, q):
    return m * x + q


def _first_guess(x, y, function):
    result, covariance = curve_fit(function, x, y)
    m, q = result
    sigma = numpy.std(x * m + q - y) #/ numpy.sqrt(len(x) - 3)
    m, q = numpy.random.normal(m, 10), numpy.random.normal(q, 2)
    sigma = sigma + sigma * numpy.random.uniform(-.1, .1)

    return numpy.array((m, q, sigma))


def __ln_prior(parameters):
    m, b, sigma = parameters
    if 1.e-3 <= sigma <= 5:
        return 0.0
    return -numpy.inf


def __ln_probability(parameters, x, y, likelihood_fn):
    priors = __ln_prior(parameters)
    if numpy.isfinite(priors):
        return + priors + likelihood_fn(parameters, x, y)
    return -numpy.inf


def _interpolate_mc_mc(x, y, function, likelihood):
    dims = 3
    n_walkers = 50
    relaxing_steps = 100

    start_position = [_first_guess(x, y, function) for _ in range(n_walkers)]

    # Burn-in
    sampler = emcee.EnsembleSampler(n_walkers, dims, __ln_probability, args=(x, y, likelihood))
    sampler.run_mcmc(start_position, relaxing_steps)

    samples = sampler.get_chain()

    m, q, sigma = numpy.mean(samples[50:, :, 0]), \
                  numpy.mean(samples[50:, :, 1]), \
                  numpy.mean(samples[50:, :, 2])

    samples_out = numpy.zeros((n_walkers, relaxing_steps), dtype=PSET_TYPES)
    samples_out['m'] = samples[:, :, 0].T
    samples_out['q'] = samples[:, :, 1].T
    samples_out['sigma'] = samples[:, :, 2].T

    parameters_out = numpy.zeros(1, dtype=PSET_TYPES)
    parameters_out['m'] = m
    parameters_out['q'] = q
    parameters_out['sigma'] = sigma

    return parameters_out, samples_out


def derive_interpolation_parameters(dataset: Dict[str, Dict[str, Dict[str, numpy.ndarray]]]) -> \
        Dict[str, Dict[str, Dict[Any, Dict]]]:
    dataset = __interpolate_gains_per_antenna(dataset)
    return dataset
