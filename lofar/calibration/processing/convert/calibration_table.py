from numpy import ndarray
from lofar.calibration.common.datacontainers.holography_dataset import HolographyDataset
from numpy import zeros_like, exp
from datetime import datetime, date
from lofar.calibration.common.coordinates import mjd_to_astropy_time
import logging

logger = logging.getLogger(__name__)

__CALIBRATION_NAME = 'HolographyCalibration'
_DATETIME_FORMAT = '%Y%m%d%H%M'
_DATE_FORMAT = '%Y%m%d'
_MHZ_TO_HZ = 1.e6

def mjds_to_datetime(mjd_in_seconds):
    astropy_time = mjd_to_astropy_time(mjd_in_seconds)
    return astropy_time.datetime


def antenna_polarization_mode_to_rcu(antenna: int,
                                     polarization: str,
                                     mode: int):
    if mode in [3, 4, 5, 6, 7]:
        polarization_index = 0 if polarization == 'XX' else 1
    elif mode in [1, 2]:
        polarization_index = 0 if polarization == 'YY' else 1
    else:
        raise ValueError('mode has to be between 1 and 7')

    rcu = antenna * 2 + polarization_index
    return rcu


def compute_gains_from_fit(frequencies, amplitude_parameters: ndarray, phase_parameters: ndarray):
    amplitudes = amplitude_parameters['m'] * frequencies + amplitude_parameters['q']
    phases = phase_parameters['m'] * frequencies + phase_parameters['q']

    gains = amplitudes * exp(1.j * phases)
    return gains


def compute_calibration_table(dataset: HolographyDataset, comment):
    interpolated_gains = dataset.derived_data['INTERPOLATED_GAINS']['all']
    modes, calibration_tables = zip(*dataset.calibration_tables().items())

    for mode, calibration_table in zip(modes, calibration_tables):
        frequencies = calibration_table.frequencies() * _MHZ_TO_HZ
        data = zeros_like(calibration_table.data)
        for polarization in interpolated_gains:
            for antenna in interpolated_gains[polarization]:
                gains_per_antenna = interpolated_gains[polarization][antenna]
                derived_gains = compute_gains_from_fit(frequencies, gains_per_antenna['amplitude']['parameters'],
                                                       gains_per_antenna['phase']['parameters'])
                rcu_id = antenna_polarization_mode_to_rcu(int(antenna),
                                                          polarization,
                                                          int(mode))
                data[:, rcu_id] = derived_gains
        observation_date = mjds_to_datetime(dataset.start_time)
        observation_date_formatted = observation_date.strftime(_DATETIME_FORMAT)

        new_calibration_table = calibration_table.derive_calibration_table_from_gain_fit(
            observation_source=dataset.source_name,
            observation_date=observation_date_formatted,
            calibration_name=__CALIBRATION_NAME,
            calibration_date=datetime.now().date().strftime(_DATE_FORMAT),
            comment=comment,
            gains=data)

        dataset.derived_calibration_tables[mode] = new_calibration_table


