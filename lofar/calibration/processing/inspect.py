import logging
import os
import typing

import matplotlib.pyplot as plt
import numba
import numpy
from matplotlib import cm, rcParams
from matplotlib.ticker import AutoMinorLocator
from numba import jit
from numpy.fft import fft2, fftshift
from scipy.constants import c as light_speed
from scipy.ndimage.filters import gaussian_filter

from lofar.calibration.common.datacontainers.holography_dataset import HolographyDataset
from lofar.calibration.common.datacontainers.holography_dataset_definitions import HDS_sampled_illumination_type

from lofar.calibration.common.coordinates import _SIGN_LMN_TO_PQR
from lofar.calibration.common.coordinates import _SIGN_PQR_TO_LMN

logger = logging.getLogger(__name__)


def __set_minor_ticks_for_canvas(canvas):
    canvas.xaxis.set_minor_locator(AutoMinorLocator())
    canvas.yaxis.set_minor_locator(AutoMinorLocator())


plt.rcParams['figure.autolayout'] = True
plt.rcParams['figure.max_open_warning'] = False
__DEFAULT_SAMPLING = 250
__LIST_OR_NPARRAY = typing.Union[typing.List[float], numpy.ndarray]
__DATATABLE_TYPE = typing.Dict[
    str, typing.Dict[str, typing.Dict[str, typing.Dict[str, numpy.ndarray]]]]
__MHZ_IN_HZ = 1.e6
__MAX_SIZE_LOFAR_ARRAY_IN_M = 50

DEFAULT_FORMAT = 'png'


def __save_figure_to_path(figure, path, title, format=DEFAULT_FORMAT):
    os.makedirs(path, exist_ok=True)
    full_path = os.path.join(path, title)
    figure.savefig(full_path, format=format)


@jit
def derive_beam_coefficient_at_frequency(freq_hz: float):
    return 2. * numpy.pi * freq_hz / light_speed


@jit
def dft2_numba(x: numpy.ndarray,
               y: numpy.ndarray,
               l: numpy.ndarray,
               m: numpy.ndarray,
               freq_hz,
               v: numpy.ndarray,
               fourier_sign):
    arg_factor = fourier_sign * derive_beam_coefficient_at_frequency(freq_hz)
    result = numpy.zeros((len(y), len(x)), dtype=numpy.complex64)
    x_down = x.astype(numpy.float32)
    y_down = y.astype(numpy.float32)
    l_down = l.astype(numpy.float32)
    m_down = m.astype(numpy.float32)
    v_down = v.astype(numpy.complex64)

    dft2_numba_loop(x_down, y_down, l_down, m_down, v_down, arg_factor, result)
    return result


@jit()
def beam(v: complex, x: float, y: float, l: float, m: float, arg_factor: float) -> complex:
    return v * numpy.exp(1.j * arg_factor * (x * l + y * m))


@jit(parallel=True, fastmath=True)
def dft2_numba_loop(x, y, l, m, v, arg_factor, result):
    y_len = len(y)
    x_len = len(x)
    k_len = len(v)

    for i in numba.prange(y_len):
        for j in range(x_len):
            for k in range(k_len):
                result[i, j] += beam(v[k], x[j], y[i], l[k], m[k], arg_factor)


def compute_antennas_field(dset: HolographyDataset):
    antenna_positions = dset.antenna_field_position()
    antenna_offset = numpy.array(dset.target_station_position) - antenna_positions
    rotated_antenna_offset = numpy.dot(dset.rotation_matrix(), antenna_offset.T).T.squeeze()

    return rotated_antenna_offset


def complex_value_to_color(complex_array: numpy.ndarray, abs_max=None, abs_min=None, log=True):
    linearized_complex_array = complex_array.ravel()

    abs_array = numpy.abs(linearized_complex_array)

    if abs_max is None:
        abs_max = numpy.max(abs_array)

    if abs_min is None:
        abs_min = numpy.min(abs_array)
    if log:
        abs_array = numpy.log10(abs_array + 1)
        abs_min, abs_max = numpy.log10([abs_min + 1, abs_max + 1])

    normalization = abs_max - abs_min
    normalization = normalization if not numpy.isnan(normalization) else 1.

    norm_abs_array = (abs_array - abs_min) / normalization
    phase_array = (numpy.angle(linearized_complex_array) + numpy.pi) / 2 / numpy.pi

    color_map = cm.get_cmap('hsv')

    colors = color_map(phase_array)
    colors[:, 3] = norm_abs_array

    old_shape = list(complex_array.shape)
    new_shape = old_shape + [4]

    colors = colors.reshape(new_shape)

    return colors


def compute_visibility_range(visibitiles_per_polarization):
    v_max_list = []
    v_min_list = []
    for index, polarization in enumerate(('XX', 'XY', 'YX', 'YY')):
        v_pol = numpy.abs(list(map(lambda x: x[polarization], visibitiles_per_polarization)))
        v_max_list.append(numpy.nanmax(v_pol))
        v_min_list.append(numpy.nanmin(v_pol))

    return min(v_min_list), max(v_max_list)


def _grid_visibilities_lm_plane(l, m, v, sampling):
    dl = 2. / sampling
    over_dl = 1. / dl
    dm = 2. / sampling
    over_dm = 1. / dm

    vis = numpy.zeros((sampling, sampling), dtype=complex)
    l = numpy.array(l)
    m = numpy.array(m)
    l_indexes = numpy.floor((l + 1.) * over_dl).astype(numpy.int)
    m_indexes = numpy.floor((m + 1.) * over_dm).astype(numpy.int)

    for l_i, m_i, v_i in zip(l_indexes, m_indexes, v):
        vis[m_i, l_i] = v_i

    sigma = [5, 5]
    vis.real = gaussian_filter(vis.real, sigma)
    vis.imag = gaussian_filter(vis.imag, sigma)

    return vis


def _fft_visibilities_lm_plane(l, m, v, sampling):
    # TODO check that this uses the correct functions/signs
    vis = _grid_visibilities_lm_plane(l, m, v, sampling)

    fft_vis = fftshift(fft2(vis))
    return fft_vis


def _dft_visibilities_lm_plane(l, m, v, sampling, frequency, array_size):
    x = numpy.linspace(-array_size, array_size, sampling)
    y = numpy.linspace(-array_size, array_size, sampling)
    l = numpy.array(l)
    m = numpy.array(m)
    v = numpy.array(v)
    fft_vis = dft2_numba(x, y, l, m, frequency, v, _SIGN_LMN_TO_PQR)

    return fft_vis


def _dft_gains_station_plane(x, y, g, sampling, frequency):
    m = numpy.linspace(-1, 1, sampling)
    l = numpy.linspace(-1, 1, sampling)
    x = numpy.array(x)
    y = numpy.array(y)
    g = numpy.array(g)
    fft_gain = dft2_numba(l, m, x, y, frequency, g, _SIGN_PQR_TO_LMN)
    l_array, m_array = numpy.meshgrid(l, m)
    fft_gain[numpy.where((l_array ** 2. + m_array ** 2) > 1)] = 0.

    return fft_gain


def compute_illumination_per_frequency_polarization(l, m, v, frequency: float, sampling=__DEFAULT_SAMPLING,
                                                    array_size=__MAX_SIZE_LOFAR_ARRAY_IN_M):
    fft_vis = _dft_visibilities_lm_plane(l=l, m=m, v=v,
                                         sampling=sampling,
                                         frequency=frequency,
                                         array_size=array_size)
    return fft_vis


def compute_illumination_per_dataset(input_datatable, central_beam, sampling=__DEFAULT_SAMPLING,
                                     antenna_array_size_in_m=__MAX_SIZE_LOFAR_ARRAY_IN_M, is_model=False):
    output_datatable = dict()
    for polarization in ('XX', 'XY', 'YX', 'YY'):

        illumination_per_polarization = dict()
        output_datatable[polarization] = illumination_per_polarization
        for frequency_string in input_datatable:
            data_per_frequency = input_datatable[frequency_string]
            central_beam_per_frequency = central_beam[frequency_string]
            if is_model:
                l_m_v = [
                    (data_per_frequency[beam_string]['l'][0],
                     data_per_frequency[beam_string]['m'][0],
                     data_per_frequency[beam_string][polarization][0])
                    for beam_string in data_per_frequency]
            else:
                l_m_v = [
                    (- data_per_frequency[beam_string]['mean']['l'][0] +
                     data_per_frequency[central_beam_per_frequency]['mean']['l'][0],
                     - data_per_frequency[beam_string]['mean']['m'][0] +
                     data_per_frequency[central_beam_per_frequency]['mean']['m'][0],
                     data_per_frequency[beam_string]['mean'][polarization][0])
                    for beam_string in data_per_frequency]
            l, m, v = list(zip(*l_m_v))
            illumination = compute_illumination_per_frequency_polarization(l, m, v,
                                                                           float(frequency_string),
                                                                           sampling=sampling,
                                                                           array_size=antenna_array_size_in_m)
            logger.debug('computed illumination for freq %s and polarization %s', frequency_string, polarization)
            illumination_per_polarization[frequency_string] = illumination
    return output_datatable


def compute_illumination(hds: HolographyDataset, sampling=__DEFAULT_SAMPLING,
                         antenna_array_size_in_m=__MAX_SIZE_LOFAR_ARRAY_IN_M):
    input_datatable = hds.derived_data['STATION_AVERAGED']['all']
    ILLUMINATION_TAB_NAME = 'ILLUMINATION'

    hds.derived_data[ILLUMINATION_TAB_NAME] = \
        compute_illumination_per_dataset(input_datatable, hds.central_beamlets(), sampling=sampling,
                                         antenna_array_size_in_m=antenna_array_size_in_m)

    output_datatable = hds.derived_data[ILLUMINATION_TAB_NAME]

    output_datatable['array_size'] = antenna_array_size_in_m
    output_datatable['sampling'] = sampling

    return hds


def compute_beam_shape(hds: HolographyDataset, sampling=__DEFAULT_SAMPLING):
    BEAMSHAPE_TAB_NAME = 'BEAM_SHAPE'
    input_datatable = hds.derived_data['GAINS']['all']
    antenna_field = compute_antennas_field(hds)

    hds.derived_data[BEAMSHAPE_TAB_NAME] = dict()

    output_datatable = hds.derived_data[BEAMSHAPE_TAB_NAME]

    output_datatable['sampling'] = sampling

    for polarization in ('XX', 'XY', 'YX', 'YY'):

        beam_shape_per_frequency = dict()
        output_datatable[polarization] = beam_shape_per_frequency

        for frequency_string in input_datatable:
            gains_per_antenna = input_datatable[frequency_string][polarization]['gains']
            frequency = float(frequency_string)

            x, y, g = antenna_field[:, 0], antenna_field[:, 1], gains_per_antenna[:]
            dft = _dft_gains_station_plane(x, y, g, __DEFAULT_SAMPLING,
                                           float(frequency))  ## GET ONLY ON THE LM OF THE BEAMS

            logger.debug('computed beam shape for freq %s and polarization %s', frequency_string, polarization)
            beam_shape_per_frequency[frequency_string] = dft
    return hds


def _plot_complex_image(canvas, image, extent=None, log=False, abs_max=None, abs_min=None):
    color_mapped_vis = complex_value_to_color(image, log=log, abs_max=abs_max, abs_min=abs_min)
    canvas.imshow(color_mapped_vis, extent=extent, origin='lower', resample=True)


def _plot_station_averaged_visibilities_lm_plane_single_frequency_scatter(target_station,
                                                                          reference_station,
                                                                          frequency,
                                                                          l_m_v,
                                                                          save_to=None):
    frequency_in_mhz = frequency / __MHZ_IN_HZ

    l, m, v, flagged = list(zip(*l_m_v))

    for index, polarization in enumerate(('XX', 'XY', 'YX', 'YY')):
        figure_title = 'OBSERVED_BEAM_VISIBILITIES_{}_{}_{}-{:4.2f}_MHz.{}'.format(target_station,
                                                                                   reference_station,
                                                                                   polarization,
                                                                                   frequency_in_mhz,
                                                                                   DEFAULT_FORMAT)
        figure = plt.figure(figure_title)

        v_pol = numpy.array(list(map(lambda x: x[polarization], v)))
        canvas = figure.add_subplot(1, 1, 1)
        canvas.set_title(figure_title.replace('_', ' ').split('.')[0])
        canvas.set_xlabel('l')
        canvas.set_ylabel('m')
        canvas.set_xlim(-1, 1)
        canvas.set_ylim(-1, 1)
        plt.minorticks_on()
        v_intensity = numpy.abs(v_pol)
        canvas.scatter(l, m, c=v_intensity, s=5, vmin=0, vmax=1, cmap='viridis')

        if save_to:
            __save_figure_to_path(figure, save_to, figure_title)
            plt.close(figure)


def _plot_station_averaged_visibilities_lm_plane_single_frequency(frequency,
                                                                  reference_station,
                                                                  target_station,
                                                                  l_m_v,
                                                                  sampling=512,
                                                                  abs_min=None,
                                                                  abs_max=None,
                                                                  save_to=None):
    frequency_in_mhz = frequency / __MHZ_IN_HZ
    l, m, v, flagged = list(zip(*l_m_v))

    for index, polarization in enumerate(('XX', 'XY', 'YX', 'YY')):
        figure_title = 'GRIDDED_VISIBILITIES_{}_{}_{}-{:4.2f}_MHz.{}'.format(reference_station,
                                                                             target_station,
                                                                             polarization,
                                                                             frequency_in_mhz,
                                                                             DEFAULT_FORMAT)
        figure = plt.figure(figure_title)

        v_pol = numpy.array(list(map(lambda x: x[polarization], v)))
        canvas = figure.add_subplot(1, 1, 1)
        canvas.set_title(figure_title.replace('_', ' ').split('.')[0])

        canvas.set_xlabel('l')
        canvas.set_ylabel('m')
        canvas.set_xlim(-1, 1)
        canvas.set_ylim(-1, 1)
        plt.minorticks_on()
        vis = _grid_visibilities_lm_plane(l, m, v_pol, sampling)
        _plot_complex_image(canvas, vis, extent=[-1, 1, -1, 1], log=True, abs_min=abs_min, abs_max=abs_max)

        if save_to:
            __save_figure_to_path(figure, save_to, figure_title)
            plt.close(figure)


def _plot_station_averaged_visibilities_station_plane_single_frequency(target_station,
                                                                       reference_station,
                                                                       l_m_v,
                                                                       frequency: float,
                                                                       antenna_field,
                                                                       sampling=__DEFAULT_SAMPLING,
                                                                       abs_min=None,
                                                                       abs_max=None,
                                                                       save_to=None):
    frequency_in_mhz = frequency / __MHZ_IN_HZ
    l, m, v, flagged = list(zip(*l_m_v))

    for index, polarization in enumerate(('XX', 'XY', 'YX', 'YY')):
        figure_title = 'MEASURED_ILLUMINATION_{}_{}_{}-{:4.2f}_MHz.{}'.format(target_station,
                                                                              reference_station,
                                                                              polarization,
                                                                              frequency_in_mhz,
                                                                              DEFAULT_FORMAT)
        figure = plt.figure(figure_title)

        v_pol = numpy.array(list(map(lambda x: x[polarization], v)))

        array_size = __MAX_SIZE_LOFAR_ARRAY_IN_M
        fft_vis = _dft_visibilities_lm_plane(l=l, m=m, v=v_pol,
                                             sampling=sampling,
                                             frequency=frequency,
                                             array_size=array_size)

        canvas = figure.add_subplot(1, 1, 1)
        canvas.set_title(figure_title.replace('_', ' ').split('.')[0])

        canvas.set_xlim(-array_size, array_size)
        canvas.set_ylim(-array_size, array_size)

        canvas.set_xlabel('x [m]')
        canvas.set_ylabel('y [m]')

        color_mapped_fft = complex_value_to_color(fft_vis, log=False,
                                                  abs_min=abs_min,
                                                  abs_max=abs_max)
        plt.minorticks_on()
        canvas.imshow(color_mapped_fft[::, ::], origin='lower', extent=[-1 * array_size,
                                                                        1 * array_size,
                                                                        -1 * array_size,
                                                                        1 * array_size],
                      resample=True)
        plt.scatter(antenna_field[:, 0], antenna_field[:, 1], marker='o', s=100,
                    facecolor='none', edgecolors='k')

        if save_to:
            __save_figure_to_path(figure, save_to, figure_title)
            plt.close(figure)


def _plot_station_averaged_visibilities_lm_plane_datatable(data_table: __DATATABLE_TYPE,
                                                           central_beamlets,
                                                           target_station,
                                                           antenna_array_offset,
                                                           save_to=None):
    for station, data_per_station in data_table.items():
        for frequency_str, data_per_frequency in data_per_station.items():
            central_beam = data_per_frequency[central_beamlets[frequency_str]]['mean']

            l_m_v = [(-data_per_beam['mean']['l'][0] + central_beam['l'][0],
                      -data_per_beam['mean']['m'][0] + central_beam['m'][0],
                      dict(XX=data_per_beam['mean']['XX'][0],
                           XY=data_per_beam['mean']['XY'][0],
                           YX=data_per_beam['mean']['YX'][0],
                           YY=data_per_beam['mean']['YY'][0]),
                      data_per_beam['mean']['flag'][0])
                     for beam, data_per_beam in data_per_frequency.items()]
            frequency = float(frequency_str)

            _plot_station_averaged_visibilities_lm_plane_single_frequency(frequency=frequency,
                                                                          target_station=target_station,
                                                                          reference_station=station,
                                                                          l_m_v=l_m_v,
                                                                          save_to=save_to)

            _plot_station_averaged_visibilities_lm_plane_single_frequency_scatter(frequency=frequency,
                                                                                  target_station=target_station,
                                                                                  reference_station=station,
                                                                                  l_m_v=l_m_v,
                                                                                  save_to=save_to)

            _plot_station_averaged_visibilities_station_plane_single_frequency(target_station=target_station,
                                                                               reference_station=station, l_m_v=l_m_v,
                                                                               frequency=frequency,
                                                                               save_to=save_to,
                                                                               antenna_field=antenna_array_offset)


def _plot_gains_as_frequency(data_table: __DATATABLE_TYPE, target_station, antenna_field, antenna_list,
                             sampling=__DEFAULT_SAMPLING,
                             save_to=None):
    for station, data_per_station in data_table.items():
        opened_figures = set()

        for polarization_index, polarization in enumerate(['XX', 'XY', 'YX', 'YY']):
            extracted_data = [
                (float(frequency_str), numpy.array(data_per_frequency[polarization]['gains']))
                for frequency_str, data_per_frequency in data_per_station.items()
                if numpy.array(data_per_frequency[polarization]['flag']) == False
            ]

            frequencies, gains = list(zip(*extracted_data))

            frequencies = numpy.array(frequencies)

            gains = numpy.stack(gains, axis=1)
            for antenna_idx, gains_per_antenna in enumerate(gains):
                antenna_id = antenna_list[antenna_idx]
                figure_name = 'GAINS_{:}_ANTENNA_{:}_{:}.{}'.format(target_station,
                                                                    antenna_id,
                                                                    polarization,
                                                                    DEFAULT_FORMAT)
                figure_title = 'GAINS {:} - ANTENNA {:} - POL {:}'.format(target_station, antenna_id, polarization)
                figure = plt.figure(figure_name)
                opened_figures.add(figure_name)

                canvas = figure.add_subplot(1, 1, 1)
                canvas.set_title(figure_title)

                canvas.set_ylim(-numpy.pi, numpy.pi)
                canvas.set_xlabel('frequency (MHz)')
                canvas.set_ylabel('Phase')
                plt.minorticks_on()
                canvas.plot(frequencies / __MHZ_IN_HZ, numpy.angle(gains_per_antenna), '+-')

            for freq_id, frequency in enumerate(frequencies):
                figure_name = 'GAINS_MAP_{}_{:}_{:.2f}_MHz.{}'.format(target_station,
                                                                      polarization,
                                                                      frequency / __MHZ_IN_HZ, DEFAULT_FORMAT)
                figure_title = 'GAINS MAP {} Freq={:.2f}MHz'.format(target_station, float(frequency) / __MHZ_IN_HZ)

                figure = plt.figure(figure_name)
                figure.tight_layout()
                opened_figures.add(figure_name)
                canvas = figure.add_subplot(1, 1, 1)
                canvas.set_title(figure_title)
                canvas.set_aspect('equal')
                canvas.set_xlabel('x [m]')
                canvas.set_ylabel('y [m]')
                plt.tight_layout()
                plt.minorticks_on()
                plt.scatter(antenna_field[:, 0], antenna_field[:, 1], c=numpy.angle(gains[:, freq_id]), vmax=numpy.pi,
                            vmin=-numpy.pi)

                figure_name = 'BEAM_RECONSTRUCTION_{}_{:}_{:.2f}_MHz.{}'.format(target_station,
                                                                                polarization,
                                                                                frequency / __MHZ_IN_HZ,
                                                                                DEFAULT_FORMAT)
                figure_title = 'BEAM SHAPE {} Freq={:.2f}MHz Pol {:}'.format(target_station,
                                                                             float(frequency) / __MHZ_IN_HZ,
                                                                             polarization)

                figure = plt.figure(figure_name)
                figure.tight_layout()
                opened_figures.add(figure_name)
                canvas = figure.add_subplot(1, 1, 1)
                canvas.set_title(figure_title)
                canvas.set_xlabel('l')
                canvas.set_ylabel('m')
                x, y, g = antenna_field[:, 0], antenna_field[:, 1], gains[:, freq_id]
                dft = _dft_gains_station_plane(x, y, g, __DEFAULT_SAMPLING,
                                               float(frequency))  ## GET ONLY ON THE LM OF THE BEAMS
                color_mapped_fft = complex_value_to_color(dft, log=False)
                plt.minorticks_on()
                canvas.imshow(color_mapped_fft[::, ::], origin='lower', extent=[-1,
                                                                                1,
                                                                                -1,
                                                                                1])
                plt.tight_layout()

        if save_to:
            for figure_name in opened_figures:
                figure = plt.figure(figure_name)
                if save_to:
                    __save_figure_to_path(figure, save_to, figure_name)
                    plt.close(figure)


def __apply_to_datatable(dset: HolographyDataset, derived_datatable_name, function_to_apply, *args,
                         **kwargs):
    if derived_datatable_name in dset.derived_data:
        data_table = dset.derived_data[derived_datatable_name]
        function_to_apply(data_table, *args, **kwargs)


def plot_station_averaged_visibilities_lm_plane(dset: HolographyDataset,
                                                derived_datatable_name='STATION_AVERAGED',
                                                save_to=None):
    antenna_field = compute_antennas_field(dset)
    __apply_to_datatable(dset,
                         derived_datatable_name,
                         _plot_station_averaged_visibilities_lm_plane_datatable,
                         dset.central_beamlets(),
                         dset.target_station_name,
                         antenna_field, save_to=save_to)


def plot_gains_per_antenna(dset: HolographyDataset, derived_datatable_name='GAINS', save_to=None):
    antenna_field = compute_antennas_field(dset)
    __apply_to_datatable(dset,
                         derived_datatable_name,
                         _plot_gains_as_frequency,
                         dset.target_station_name, antenna_field, dset.used_antennas, save_to=save_to)


def plot_gains_fit_per_antenna(dset: HolographyDataset,
                               interpolation_parameters_datable_name='INTERPOLATED_GAINS',
                               gains_datable_name='GAINS', save_to=None):
    interpolation_parameters = dset.derived_data[interpolation_parameters_datable_name]
    gains = dset.derived_data[gains_datable_name]
    opened_figures = set()

    for station, data_per_station in gains.items():

        for polarization_index, polarization in enumerate(['XX', 'YY']):

            extracted_data = [
                (float(frequency_str), data_per_frequency[polarization]['gains'])
                for frequency_str, data_per_frequency in data_per_station.items()
                if numpy.array(data_per_frequency[polarization]['flag']) == False
            ]
            frequencies, gains = list(zip(*extracted_data))
            frequencies = numpy.array(frequencies)
            gains = numpy.stack(gains, axis=1)

            for antenna_idx, antenna_data in enumerate(gains):
                antenna = dset.used_antennas[antenna_idx]
                amplitude_par = interpolation_parameters[station][polarization][str(antenna_idx)]['amplitude'][
                    'parameters']
                phase_par = \
                    interpolation_parameters[station][polarization][str(antenna_idx)]['phase'][
                        'parameters']
                figure_title = 'station_%s_antenna_%s_polarization_%s' % (station,
                                                                          antenna,
                                                                          polarization)
                figure_name = '%s.%s' % (figure_title, DEFAULT_FORMAT)

                figure = plt.figure(figure_name)
                opened_figures.add(figure_name)

                canvas = figure.add_subplot(2, 1, 1)
                __set_minor_ticks_for_canvas(canvas)
                canvas.set_title(figure_title.replace('_', ' '))

                canvas.set_xlabel('frequency (MHz)')
                canvas.set_ylabel('Amplitude')
                canvas.plot(frequencies / __MHZ_IN_HZ, amplitude_par['m'] * frequencies + amplitude_par['q'])
                canvas.plot(frequencies / __MHZ_IN_HZ, numpy.abs(antenna_data), '+', ms=5, mew=10)
                plt.minorticks_on()
                canvas = figure.add_subplot(2, 1, 2)
                __set_minor_ticks_for_canvas(canvas)

                canvas.set_xlabel('frequency (MHz)')
                canvas.set_ylabel('Phase')
                canvas.set_ylim(-numpy.pi, numpy.pi)
                plt.minorticks_on()
                canvas.plot(frequencies / __MHZ_IN_HZ, numpy.angle(antenna_data), '+', ms=5, mew=10)

                canvas.plot(frequencies / __MHZ_IN_HZ, phase_par['m'] * frequencies + phase_par['q'])


    if save_to:
        for figure_name in opened_figures:
            figure = plt.figure(figure_name)
            if save_to:
                __save_figure_to_path(figure, save_to, figure_name)
                plt.close(figure)


def plot_abs_visibilities(figure, data_table):
    pols = ('XX', 'XY', 'YX', 'YY')
    abs_data = numpy.stack([numpy.abs(data_table[pol]) for pol in pols], axis=0)
    max_value = numpy.max(abs_data) * 10
    min_value = numpy.min(abs_data) / 10

    for pol_i, pol in enumerate(pols):
        canvas = figure.add_subplot(2, 2, pol_i + 1)
        canvas.set_ylim([min_value, max_value])
        abs_per_pol = abs_data[pol_i, :]
        time = data_table['t']

        flag = data_table['flag']
        flagged_point = time[flag]
        if flagged_point:
            canvas.axvline(flagged_point, color='red')
        canvas.plot(time, abs_per_pol, '+-')
        plt.xticks(rotation=45)
        plt.semilogy()
        plt.xlabel('t')
        plt.ylabel('$|V|$')
        plt.minorticks_on()
        plt.tight_layout()


def plot_phase_visibilities(figure, data_table):
    pols = ('XX', 'XY', 'YX', 'YY')
    angle_data = numpy.stack([numpy.angle(data_table[pol]) for pol in pols], axis=0)
    max_value = numpy.pi + .5
    min_value = -numpy.pi - .5

    for pol_i, pol in enumerate(pols):
        canvas = figure.add_subplot(2, 2, pol_i + 1)
        canvas.set_ylim([min_value, max_value])
        ang_per_pol = angle_data[pol_i, :]

        time = data_table['t']
        plt.xticks(rotation=45)
        flag = data_table['flag']
        flagged_point = time[flag]
        if flagged_point:
            canvas.axvline(flagged_point, color='red')

        canvas.plot(time, ang_per_pol, '+-')
        plt.xlabel('t')
        plt.ylabel('$\\phi$')

        plt.minorticks_on()
        plt.tight_layout()


def plot_raw_uv(dset: HolographyDataset, selected_stations=None, save_to=None):
    datatable = dset.data
    selected_stations = selected_stations if selected_stations else datatable.keys()
    central_beamlet_number_per_frequency = dset.central_beamlets
    opened_figures = []
    for station_name in selected_stations:
        for frequency_str in datatable[station_name]:
            title = 'RAWUV_AMPL_%s_%s.%s' % (station_name, frequency_str, DEFAULT_FORMAT)
            figure = plt.figure(title)
            opened_figures.append(title)
            vis_per_frequency_station = datatable[station_name][frequency_str]
            vis_central_beam = vis_per_frequency_station[central_beamlet_number_per_frequency[frequency_str]]
            plot_abs_visibilities(figure, vis_central_beam)

            title = 'RAWUV_PHASE_%s_%s.%s' % (station_name, frequency_str, DEFAULT_FORMAT)
            figure = plt.figure(title)
            opened_figures.append(title)
            vis_per_frequency_station = datatable[station_name][frequency_str]
            vis_central_beam = vis_per_frequency_station[central_beamlet_number_per_frequency[frequency_str]]
            plot_phase_visibilities(figure, vis_central_beam)

    if save_to:
        for figure_name in opened_figures:
            figure = plt.figure(figure_name)
            if save_to:
                __save_figure_to_path(figure, save_to, figure_name)
                plt.close(figure)


def derive_illumination_per_antenna(antenna_position, visibilities, l, m, frequency):
    coeff = derive_beam_coefficient_at_frequency(frequency)
    value = beam(visibilities, antenna_position[0], antenna_position[1], l, m, coeff)

    return numpy.mean(value.real) + 1.j * numpy.mean(value.imag)


def derive_illumination_per_array(antenna_positions, visibilities, frequency, central_beam_l, central_beam_m):
    vis = numpy.array([value['mean'] for value in visibilities.values()])
    n_antennas = antenna_positions.shape[0]

    result = numpy.zeros(n_antennas, dtype=HDS_sampled_illumination_type)

    for antenna_id, antenna_position in enumerate(antenna_positions):
        for pol in ['XX', 'YY']:
            result[antenna_id][pol] = derive_illumination_per_antenna(antenna_position,
                                                                      vis[pol],
                                                                      central_beam_l - vis['l'],
                                                                      central_beam_m - vis['m'],
                                                                      frequency) * n_antennas
            result[antenna_id]['x'], result[antenna_id]['y'], result[antenna_id]['z'] = antenna_position[0], \
                                                                                        antenna_position[1], \
                                                                                        antenna_position[2]
    return result


def compute_sampled_illumination(dset: HolographyDataset):
    antenna_positions = compute_antennas_field(dset)
    visibilities = dset.derived_data['STATION_AVERAGED']
    central_beamlets = dset.central_beamlets()
    result = dict()
    for station_str in visibilities.keys():
        visibilities_per_station = visibilities[station_str]
        result[station_str] = dict()

        for frequency_str in visibilities_per_station.keys():
            visibilities_per_frequency = visibilities_per_station[frequency_str]
            central_beamlet_vis = visibilities_per_frequency[central_beamlets[frequency_str]]['mean']
            result[station_str][frequency_str] = \
                derive_illumination_per_array(antenna_positions,
                                              visibilities_per_frequency,
                                              float(frequency_str),
                                              central_beamlet_vis['l'],
                                              central_beamlet_vis['m'])
    return result


def get_or_compute_sampled_illuminations(dset: HolographyDataset):
    if 'SAMPLED_ILLUMINATION' in dset.derived_data:
        return dset.derived_data['SAMPLED_ILLUMINATION']
    else:
        return compute_sampled_illumination(dset)


def compute_model_visibilities(dset: HolographyDataset):
    antenna_positions = compute_antennas_field(dset)
    central_beams = dset.central_beamlets()
    visibilities = dset.derived_data['STATION_AVERAGED']
    gains = dset.derived_data['GAINS']
    result = dict()
    for station_str in visibilities.keys():
        visibilities_per_station = visibilities[station_str]
        result[station_str] = dict()

        for frequency_str in visibilities_per_station.keys():
            visibilities_per_frequency = visibilities_per_station[frequency_str]
            gains_per_frequency = gains[station_str][frequency_str]
            central_beam_number = central_beams[frequency_str]
            result[station_str][frequency_str] = derive_model_visibilities(antenna_positions,
                                                                           visibilities_per_frequency,
                                                                           gains_per_frequency,
                                                                           float(frequency_str),
                                                                           central_beam_number)
    return result


def plot_visibilities_comparison(dset: HolographyDataset, selected_stations=None, save_to=None):
    datatable = get_or_compute_model_visibilities(dset)
    central_beamlets = dset.central_beamlets()
    visibilities = dset.derived_data['STATION_AVERAGED']
    selected_stations = selected_stations if selected_stations else datatable.keys()

    opened_figures = []
    for station_name in selected_stations:
        for frequency_str in datatable[station_name]:
            for polarization in ['XX', 'YY']:
                title = 'RESIDUAL VISIBILITIES %s %s %s-%4.2f MHz' % (dset.target_station_name,
                                                                      station_name,
                                                                      polarization,
                                                                      float(frequency_str) / __MHZ_IN_HZ)
                name = '%s.%s' % (title.replace(' ', '_'), DEFAULT_FORMAT)
                figure = plt.figure(name)
                canvas = figure.add_subplot(1, 1, 1, aspect=1)
                canvas.set_title(title)
                canvas.set_xlabel('l')
                canvas.set_ylabel('m')
                canvas.set_xlim([-1, 1])
                canvas.set_ylim([-1, 1])
                opened_figures.append(name)

                datatable_per_station_frequency = datatable[station_name][frequency_str]
                beams = list(datatable_per_station_frequency.keys())

                l_0, m_0 = datatable_per_station_frequency[central_beamlets[frequency_str]]['l'], \
                           datatable_per_station_frequency[central_beamlets[frequency_str]]['m']

                l_m = numpy.array(
                    [[l_0 - datatable_per_station_frequency[beam]['l'] ,
                      m_0 - datatable_per_station_frequency[beam]['m'] ] for beam in beams], dtype=float)[:, :, 0]

                flagged_mask = numpy.array([datatable_per_station_frequency[beam]['flag'] for beam in beams],
                                           dtype=numpy.bool).flatten()
                unflagged_mask = ~flagged_mask
                model_vis = numpy.array(
                    [datatable_per_station_frequency[beam][polarization][0] for beam in beams], dtype=complex)
                vis = numpy.array(
                    [visibilities[station_name][frequency_str][beam]['mean'][polarization][0] for beam in beams])

                color = 100 * numpy.abs(model_vis - vis)
                acb = canvas.scatter(l_m[unflagged_mask, 0], l_m[unflagged_mask, 1], c=color[unflagged_mask])
                canvas.scatter(l_m[flagged_mask, 0], l_m[flagged_mask, 1],
                               facecolors='none', edgecolors='r', label='flagged beams')

                figure.colorbar(acb, label='amplitude difference [%]')
                plt.legend()
                plt.tight_layout()
    if save_to:
        for figure_name in opened_figures:
            figure = plt.figure(figure_name)
            if save_to:
                __save_figure_to_path(figure, save_to, figure_name)
                plt.close(figure)


def get_or_compute_model_visibilities(dset: HolographyDataset):
    if 'MODEL_VISIBILITIES' in dset.derived_data:
        return dset.derived_data['MODEL_VISIBILITIES']
    else:
        return compute_model_visibilities(dset)


def visibilities_from_gain(antenna_positions, gains, l, m, frequency):
    coeff = _SIGN_PQR_TO_LMN * derive_beam_coefficient_at_frequency(frequency)
    x = antenna_positions[:, 0]
    y = antenna_positions[:, 1]
    values = beam(gains, x, y, l, m, coeff)
    return numpy.mean(values.real) + 1.j * numpy.mean(values.imag)


def derive_model_visibilities(antenna_position, visibilities, gains, frequency, central_beam):
    result = dict()
    for beam, vis_per_beam in visibilities.items():
        vis_per_beam = numpy.array(vis_per_beam['mean'])
        result[beam] = vis_per_beam
        l = -vis_per_beam['l'] + visibilities[central_beam]['mean']['l']
        m = -vis_per_beam['m'] + visibilities[central_beam]['mean']['m']
        for polarization in ('XX', 'XY', 'YX', 'YY'):
            vis_per_beam[polarization] = visibilities_from_gain(antenna_position, gains[polarization]['gains'], l, m,
                                                                frequency)
    return result


def plot_illumination_comparison(dset: HolographyDataset, selected_stations=None, save_to=None):
    datatable = get_or_compute_sampled_illuminations(dset)
    illumin = dset.derived_data['ILLUMINATION']
    gains = dset.derived_data['GAINS']
    selected_stations = selected_stations if selected_stations else datatable.keys()

    opened_figures = []
    for station_name in selected_stations:
        for frequency_str in datatable[station_name]:
            for polarization in ['XX', 'YY']:
                title = 'RESIDUAL ILLUMINATION %s %s %s-%4.2f MHz' % (dset.target_station_name,
                                                                      station_name,
                                                                      polarization,
                                                                      float(frequency_str) / __MHZ_IN_HZ)
                name = '%s.%s' % (title.replace(' ', '_'), DEFAULT_FORMAT)
                figure = plt.figure(name)
                canvas = figure.add_subplot(1, 4, 1, aspect=1)
                canvas.set_title('diff')
                canvas.set_xlabel('x [m]')
                canvas.set_ylabel('y [m]')

                canvas_gains = figure.add_subplot(1, 4, 2, aspect=1)
                canvas_gains.set_title('gains')
                canvas_gains.set_xlabel('x [m]')
                canvas_gains.set_ylabel('y [m]')

                canvas_pvis = figure.add_subplot(1, 4, 3, aspect=1)
                canvas_pvis.set_title('pvis')
                canvas_pvis.set_xlabel('x [m]')
                canvas_pvis.set_ylabel('y [m]')

                canvas_illum = figure.add_subplot(1, 4, 4, aspect=1)
                canvas_illum.set_title('img')
                canvas_illum.set_xlabel('x [m]')
                canvas_illum.set_ylabel('y [m]')

                opened_figures.append(name)

                datatable_per_station_frequency = datatable[station_name][frequency_str]
                x, y = datatable_per_station_frequency['x'], datatable_per_station_frequency['y']
                illuminations = datatable_per_station_frequency[polarization]
                gains_per_station_frequency = gains[station_name][frequency_str][polarization]['gains']
                difference = numpy.angle(illuminations) - numpy.angle(gains_per_station_frequency)

                abs_difference = numpy.abs(illuminations - gains_per_station_frequency)

                # sc = canvas.scatter(x, y, c=numpy.angle(difference), s=200, vmin=-2 * numpy.pi, vmax=2 * numpy.pi)
                sc = canvas.scatter(x, y, c=abs_difference)
                plt.colorbar(sc)
                canvas_pvis.scatter(x, y, c=numpy.abs(illuminations), vmin=.8, vmax=1.1)
                canvas_gains.scatter(x, y, c=numpy.abs(gains_per_station_frequency), vmin=0.8, vmax=1.1)

                canvas_illum.imshow(numpy.abs(illumin[polarization][frequency_str]) / numpy.sqrt(48), origin='lower',
                                    extent=[-30, 30, -30, 30], vmin=.8, vmax=1.1)
                canvas_illum.scatter(x, y, marker='o', s=100, facecolor='none', edgecolors='red')

                # figure.colorbar(sc)
                plt.tight_layout()

                # for diff, x_i, y_i in zip(abs_difference, x, y):
                #    canvas.annotate('{:4.2f}'.format(numpy.abs(diff)), (x_i, y_i), color='red')

    if save_to:
        for figure_name in opened_figures:
            figure = plt.figure(figure_name)
            if save_to:
                __save_figure_to_path(figure, save_to, figure_name)
                plt.close(figure)


def plot_illumination_comparison_full(dset: HolographyDataset, selected_stations=None, save_to=None):
    model_visibilities = get_or_compute_model_visibilities(dset)
    central_beam = dset.central_beamlets()
    illumination = dset.derived_data['ILLUMINATION']

    array_size = illumination['array_size'][()]
    sampling = illumination['sampling'][()]
    antenna_field = compute_antennas_field(dset)
    n_beams = len(dset.beamlets())
    n_antennas = antenna_field.shape[0]
    factor = n_beams / n_antennas
    model_illumination = compute_illumination_per_dataset(model_visibilities['all'],
                                                          central_beam,
                                                          sampling,
                                                          array_size, is_model=True)
    selected_stations = selected_stations if selected_stations else model_visibilities.keys()

    opened_figures = []
    for station_name in selected_stations:
        for frequency_str in model_visibilities[station_name]:
            for polarization in ['XX', 'YY']:
                title = 'RESIDUAL ILLUMINATION FULL GRID %s %s %s-%4.2f MHz' % (dset.target_station_name,
                                                                                station_name,
                                                                                polarization,
                                                                                float(frequency_str) / __MHZ_IN_HZ)
                name = '%s.%s' % (title.replace(' ', '_'), DEFAULT_FORMAT)
                opened_figures.append(name)

                figure = plt.figure(name, figsize=(13, 3))

                model_illumination_per_freq_pol = model_illumination[polarization][frequency_str] / factor
                illumination_per_freq_pol = numpy.array(illumination[polarization][frequency_str]) / factor

                residual_illumination = model_illumination_per_freq_pol - illumination_per_freq_pol

                canvas = figure.add_subplot(1, 3, 1, aspect=1)
                canvas.set_title('model illumination')
                canvas.set_xlabel('x [m]')
                canvas.set_ylabel('y [m]')

                _plot_complex_image(canvas, model_illumination_per_freq_pol, log=False,
                                    extent=[-1 * array_size,
                                            1 * array_size,
                                            -1 * array_size,
                                            1 * array_size])
                canvas.scatter(antenna_field[:, 0], antenna_field[:, 1], marker='o', s=100,
                               facecolor='none', edgecolors='k')
                canvas = figure.add_subplot(1, 3, 2, aspect=1)
                canvas.set_title('illumination')
                canvas.set_xlabel('x [m]')
                canvas.set_ylabel('y [m]')

                _plot_complex_image(canvas, illumination_per_freq_pol,
                                    extent=[-1 * array_size,
                                            1 * array_size,
                                            -1 * array_size,
                                            1 * array_size])
                canvas.scatter(antenna_field[:, 0], antenna_field[:, 1], marker='o', s=100,
                               facecolor='none', edgecolors='k')


                canvas = figure.add_subplot(1, 3, 3, aspect=1)
                canvas.set_title('residual illumination')
                canvas.set_xlabel('x [m]')
                canvas.set_ylabel('y [m]')

                cm = canvas.imshow(numpy.abs(residual_illumination),
                                   origin='lower',
                                   extent=[-1 * array_size,
                                           1 * array_size,
                                           -1 * array_size,
                                           1 * array_size])
                canvas.scatter(antenna_field[:, 0], antenna_field[:, 1], marker='o', s=100,
                               facecolor='none', edgecolors='k')
                plt.colorbar(cm)

                plt.tight_layout()
    if save_to:
        for figure_name in opened_figures:
            figure = plt.figure(figure_name)
            if save_to:
                __save_figure_to_path(figure, save_to, figure_name)
                plt.close(figure)
