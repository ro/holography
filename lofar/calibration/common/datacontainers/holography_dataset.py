import logging
import os

import h5py

from typing import Dict, List, Union
from .calibration_table import CalibrationTable
from .holography_dataset_definitions import *
from .holography_observation import HolographyObservation
from .holography_specification import HolographySpecification
from numpy import isnan, ndarray

logger = logging.getLogger(__file__)


def compare_nested_collections(dict1, dict2):
    """
    This function compares the contents of nested collections that can be accessed through the __item__ method.
    It recursively iterates over all keys and embedded dicts.
    @return: False if a key in dict1 is not a key in dict2 or if values for
    a key differ in the dicts, True if the contents of both dicts match.
    @param dict1:  A dict
    @param dict2:  Another dict
    """
    result = True
    for key in dict1.keys():
        if key in dict2.keys():
            if isinstance(dict1[key], dict) and isinstance(dict2[key], dict):
                result = result and compare_nested_collections(dict1[key], dict2[key])
            else:
                if isinstance(dict1[key], numpy.ndarray) and isinstance(dict2[key],
                                                                        numpy.ndarray):
                    # Compares element by element the two arrays
                    return numpy.array_equal(dict1[key], dict2[key])
                else:
                    return dict1[key] == dict2[key]
        else:

            return False
    return result


def bytestring_list_to_string(list):
    return [item.decode('utf-8') for item in list]


def bytestring_to_string(bstring):
    if isinstance(bstring, bytearray):
        return bstring.decode('utf-8')
    else:
        return bstring


def to_numpy_array_string(string_list):
    string_array = numpy.empty(len(string_list), dtype="<S10")
    for i, string_item in enumerate(string_list):
        string_array[i] = string_item
    return string_array


class HolographyDataset():
    def __init__(self):
        '''
        Initialise the member variables with sane defaults.  Simple types are
        initialised with None, dicts, lists and other fancy things are
        initialised with an assignment to their respective constructors.
        '''
        # float, HDS version
        self._version = HOLOGRAPHY_DATA_SET_VERSION

        # list of ints
        self._rcu_list = list()

        # integer
        self._mode = None

        # list of strings
        self._sas_ids = list()

        # string
        self._target_station_name = None

        # list of 3 floats
        self._target_station_position = None
        # name of the source (str)
        self._source_name = None
        # position of the source array[ (RA, numpy.float64), (DEC, numpy.float64), (EPOCH, 'S10')]
        self._source_position = None
        # date time when the observation started in MJD in seconds (float)
        self._start_time = None
        # date time when the observation started in MJD in seconds (float)
        self._end_time = None
        # array(3,3), translation matrix for
        # (RA, DEC) <-> (l, m) conversion
        self._rotation_matrix = None
        # str, filter name
        self._filter = None
        # str, antenna set
        self._antenna_set = None
        # list of beamlet numbers
        self._beamlets = list()
        # list of used antennas
        self._used_antennas = list()
        # The central beam or beamlet for each frequency
        self._central_beamlets = dict()
        self._calibration_tables = dict()
        self.derived_calibration_tables = dict()
        # coordinates of the antenna position in the target
        self._antenna_field_position = list()
        # list of reference station names
        self._reference_stations = list()
        # list of _frequencies
        self._frequencies = list()
        # dict(frequency:
        #       array that contains the ra_dec of which a beam
        #       points at given a frequency and a beamlet number
        #       numpy.dtype([('RA', numpy.float64),
        #                    ('DEC',numpy.float64),
        #                    ('EPOCH', 'S10')])
        self._ra_dec = dict()
        # dict(reference_station_name:
        #      dict(frequency:
        #           dict(beamlet_number:
        #                array that contains the 4 polarization crosscorrelation for
        #                the 4 polarizations, the l and m coordinates, and the timestamp
        #                in mjd of the sample, and whether or not the data has been flagged
        # numpy.dtype([('XX', numpy.float64),
        #              ('YY', numpy.float64),
        #              ('XY', numpy.float64),
        #              ('YX', numpy.float64),
        #              ('l', numpy.float64),
        #              ('m', numpy.float64),
        #              ('t', numpy.float64),
        #              ('flag', numpy.bool)]
        #              )
        self.data = dict()
        # a dict of dicts and eventually str, ndarray or that can be converted in a ndarray calling
        # numpy.array()
        self.derived_data = None

    @property
    def version(self) -> str:
        return self._version

    @property
    def rcu_list(self) -> List[int]:
        return self._rcu_list

    @property
    def mode(self) -> int :
        return self._mode

    @property
    def sas_ids(self) -> List[id]:
        return self._sas_ids

    @property
    def target_station_name(self) -> str:
        return self._target_station_name

    @property
    def target_station_position(self) -> List[Union[str, float, float]]:
        return self._target_station_position

    @property
    def antenna_set(self):
        return self._antenna_set

    @property
    def used_antennas(self):
        return self._used_antennas

    @property
    def filter(self):
        return self._filter

    @property
    def source_name(self) -> str:
        return self._source_name

    @property
    def source_position(self) -> List[Union[str, float, float]]:
        return self.source_position

    @property
    def start_time(self) -> float:
        return self._start_time

    @property
    def end_time(self) -> float:
        return self._end_time

    def rotation_matrix(self) -> ndarray:
        return self._rotation_matrix

    def beamlets(self) -> List[int]:
        return self._beamlets

    def central_beamlets(self) -> Dict[str, int]:
        return self._central_beamlets

    def calibration_tables(self) -> Dict[int, CalibrationTable]:
        return self._calibration_tables

    def antenna_field_position(self) -> List[List[float]]:
        return self._antenna_field_position

    def reference_stations(self) -> List[str]:
        return self._reference_stations

    def frequencies(self) -> List[float]:
        return self._frequencies

    def ra_dec(self) -> Dict[str, Dict[str, ndarray]]:
        return self._ra_dec

    def __eq__(self, hds=None):
        '''
        This comparison operator compares the values of the relevant members of
        this class instance and the ones in the parameter hds.  This allows to
        compare a Holography Data Set in memory with one that has been loaded
        from an HDF5 file.  It is also possible to compare two HDSs that were
        imported from Holography Measurement Sets.
        @param hds:  An instance of the HolographyDataset class
        @return: True if the values of the members that are relevant for HDS
        are identical, False otherwise.
        '''
        equality = False
        if hds is not None and isinstance(hds, HolographyDataset) is True:
            equality = True
            for attribute_name, attribute_value in self.__dict__.items():
                other_value = getattr(hds, attribute_name)
                this_equality = True
                try:
                    if isinstance(attribute_value, numpy.ndarray) is True and isinstance(
                            other_value, numpy.ndarray) is True:
                        this_equality = numpy.array_equal(attribute_value, other_value)
                    elif isinstance(attribute_value, dict) is True and isinstance(other_value,
                                                                                  dict) is True:
                        this_equality = compare_nested_collections(attribute_value, other_value)
                    elif attribute_value != other_value:
                        this_equality = False
                        logger.error("values for field \"%s\" differs", attribute_name)
                except Exception as e:
                    logger.warning("Cannot compare HDS values!", attribute_name,
                                   type(attribute_value),
                                   e)
                    return False

                try:
                    equality = equality and this_equality
                except Exception as e:
                    logger.warning(
                        "Comparison of two values resulted in something that is neither True nor False!",
                        attribute_name, type(attribute_value), type(other_value))
                    return False
        return equality

    def find_central_beamlets(self, source, ra_dec, frequencies, beamlets):
        '''
        This function finds the central beamlet of a target station for every
        frequency.
        @param source:  A dict containing right ascension (source["RA"]) and
        declination (source["DEC"]) of the source.
        @param ra_dec:  A dict[frequency][beamlet] that contains the right
        ascension (ra_dec[frequency][beamlet][0]) and declination
        (ra_dec[frequency][beamlet][1]) for every beamlet.
        @param frequencies:  A dict that contains the _frequencies at which the
        holography observation was performed.
        @param beamlets:  A list of beamlet numbers. 
        '''
        logger.debug("Finding the central beamlets...")
        # calculate a pseudo distance that is an indicator if this
        # beamlet is closer to the source than the ones before.
        # Note that I am too lazy to calculate the spherical distance.
        # But keep in mind that this pseudo distance like its cousin
        # the geometrical distance are not really cutting it since RA
        # and DEC are coordinates of a spherical coordinate system.
        # But here the pseudo distance is good enough.
        pseudo_distance = self._compute_pseudo_distance_per_frequency_beam(ra_dec, source["RA"], source["DEC"])
        # OK.  Done with all the iteration business.  Now check if across all
        # _frequencies the same beamlet is the central one.  It is allowed that
        # the central beamlets are not always the same but it would be better
        # and let the astronomers sleep better if it would always be the same.
        # And also check if there is actually only one central beamlet per
        # frequency.
        central_beamlets = self._find_minimal_distance_per_frequency(pseudo_distance)

        # Now check if the central beamlet is the same across all _frequencies.
        # I do that by creating a set from the central beamlets of all stations.
        # If there is only one central beamlet for all _frequencies, the size of
        # the set will be 1.
        central_beamlet_set = set(central_beamlets.values())
        if len(central_beamlet_set) == 1:
            logger.debug(
                "All is good, unicorns everywhere, there is only one central beamlet \"%s\" for all _frequencies.",
                central_beamlet_set)
        else:
            logger.warning("Multiple central beamlets have been identified: ", central_beamlets)
        return central_beamlets

    def load_from_beam_specification_and_ms(self, station_name, list_of_hbs_ms_tuples):
        """
        Loads the dataset from the specification files and the measurements for the given station
        name
        :param station_name: target station name
        :param hb_specifications: a list containing (hbs, ms) per frequency

        """
        logger.info("Creating a holography data set for station \"%s\"...", station_name)
        try:
            logger.debug('collecting preliminary information')
            self._collect_preliminary_information(station_name, list_of_hbs_ms_tuples)
            logger.debug('collected preliminary information')
            logger.debug('reading data')
            self._read_data(station_name, list_of_hbs_ms_tuples)
            logger.debug('read data')
            self._central_beamlets = self.find_central_beamlets(self._source_position, self._ra_dec,
                                                                self._frequencies, self._beamlets)
            logger.info("Creation of a holography data set for station \"%s\" done.", station_name)
        except Exception as e:
            logger.exception("Error creating dataset for station \"%s\": %s", station_name, e)
            raise e

    def _compute_pseudo_distance_per_frequency_beam(self, ra_dec, source_ra, source_dec):
        """
        Compute the pseudo distance defined as abs(ra-source_ra) + abs(dec-source_dec) for all the ra_dec in
        ra_dec a function of the frequencies and beamlet
        :param ra_dec: ra and dec
        :type ra_dec: Dict[str, Dict[str, numpy.ndarray]]
        :param source_ra:
        :param source_dec:
        :return:
        """
        # Store each pseudo distance in a dict of dicts: pd[frequency][beamlet]
        pseudo_distance = dict()
        # Iterate over all _frequencies...
        for frequency_string in ra_dec.keys():
            pseudo_distance[frequency_string] = dict()
            # and iterate over the beamlets.
            for beamlet_string in ra_dec[frequency_string].keys():
                ra = ra_dec[frequency_string][beamlet_string]['RA']
                dec = ra_dec[frequency_string][beamlet_string]['DEC']

                pseudo_distance[frequency_string][beamlet_string] = abs(ra - source_ra) + abs(
                    dec - source_dec)
        return pseudo_distance

    def _find_minimal_distance_per_frequency(_, distance_per_frequency_beam):
        minimal_distance_per_frequency = dict()
        for (frequency_string, beamlets) in distance_per_frequency_beam.items():
            values = list(beamlets.values())
            keys = list(beamlets.keys())
            minimal_distance = min(values)
            # If I find a second beamlet that is 'closest' to the source for the
            # same frequency then that is a problem.  The outset was that there
            # is only a single central beamlet and not a bunch of 'em.
            if values.count(minimal_distance) == 1:
                # All is good.  Only one central beamlet.
                minimal_distance_per_frequency[frequency_string] = keys[values.index(minimal_distance)]
            else:
                text = "Found %d beamlets that have the same distance from the source position." \
                       " Therefore a unique central beamlet does not exist." % (
                           values.count(minimal_distance))
                logger.error(text)
                raise ValueError(text)
        return minimal_distance_per_frequency

    def _read_data(self, station_name, list_of_hbs_ms_tuples):
        """

        :param station_name:
        :param list_of_hbs_ms_tuples:
        :type list_of_hbs_ms_tuples: list[(HolographySpecification, HolographyObservation)]
        :return:
        """

        self.data = dict()
        for hbs, ho in list_of_hbs_ms_tuples:
            if station_name in hbs.target_station_names:
                frequency = ho.frequency
                frequency_string = str(frequency)
                for beamlet in self._beamlets:
                    beamlet_string = str(beamlet)
                    if beamlet_string not in self._ra_dec[frequency_string]:
                        logger.warning('missing pointing %s at frequency %s for station %s skipping',
                                       beamlet_string,
                                       frequency_string,
                                       station_name)
                        continue
                    if beamlet not in ho.ms_for_a_given_beamlet_number:
                        logger.error('missing beamlet %s for station %s - beamlets present are:', beamlet,
                                     station_name, sorted(ho.ms_for_a_given_beamlet_number.keys()))
                        raise ValueError('missing beamlet %s for station %s' % (beamlet, station_name))
                    reference_station_names, cross_correlation = \
                        ho.ms_for_a_given_beamlet_number[
                            beamlet].read_cross_correlation_time_flags_lm_per_station_name(
                            station_name,
                            self._reference_stations,
                            self._ra_dec[frequency_string][beamlet_string],
                            self._rotation_matrix)

                    for reference_station_index, reference_station in \
                            enumerate(reference_station_names):

                        if reference_station not in self.data:
                            self.data[reference_station] = dict()

                        if frequency_string not in self.data[reference_station]:
                            self.data[reference_station][frequency_string] = dict()

                        self.data[reference_station][frequency_string][beamlet_string] = \
                            cross_correlation[reference_station_index, :]

    def _collect_preliminary_information(self, station_name, list_of_hbs_ho_tuples):
        """
        This routines reads both the holography beam specifications files and the holography
        observation to gather the list of rcus, the mode, the target station name and position,
        the source name and position, the start and the end time, the rotation matrix to convert
        from ra and dec to l and m, the antenna field positions, the list of the reference
        stations, the _frequencies, the ra and dec at which the beams point at.

        All this data is essential to interpret the recorded holography beams cross correlations
        :param list_of_hbs_ho_tuples: a list containing (hbs, ho) per frequency
        :type list_of_hbs_ho_tuples: list[(HolographySpecification, HolographyObservation)]
        """
        target_stations, used_antennas, virtual_pointing = \
            self._collect_from_beam_specification_file(station_name,
                                                       list_of_hbs_ho_tuples)
        self._collect_from_observation(station_name,
                                       list_of_hbs_ho_tuples,
                                       virtual_pointing=virtual_pointing,
                                       used_antennas=used_antennas)

    def _collect_from_beam_specification_file(self, station_name, list_of_hbs_ho_tuples):
        mode = set()
        source_name = set()
        source_position = set()
        target_stations = set()
        reference_stations = set()
        beamlets = set()
        virtual_pointing = dict()
        frequencies = set()
        sas_ids = set()
        rcu_list = set()
        filterset = set()
        antenna_set = set()
        start_mjd = None
        end_mjd = None
        used_antennas = set()
        if len(list_of_hbs_ho_tuples) == 0:

            raise ValueError('match is empty')
        for hbs, ho in list_of_hbs_ho_tuples:

            target_stations.update(hbs.target_station_names)

            if station_name in hbs.target_station_names:
                beam_specifications = hbs.get_beam_specifications_per_station_name(station_name)
                if len(beam_specifications) == 0:
                    logger.error('beam spec notfound for ', station_name)
                    raise ValueError('Input data incomplete')
                for beam_specification in beam_specifications:
                    rcu_list.update(beam_specification.rcus_involved)
                    used_antennas.update(beam_specification.antennas_used)
                    mode.add(beam_specification.rcus_mode)
                    filterset.add(beam_specification.filter)
                    antenna_set.add(beam_specification.antenna_set)
                    source_name.add(ho.source_name)
                    source_position.add(
                        (beam_specification.station_pointing['ra'],
                         beam_specification.station_pointing['dec'],
                         beam_specification.station_pointing['coordinate_system']
                         ))
                    if start_mjd is None or start_mjd > ho.start_mjd:
                        start_mjd = ho.start_mjd

                    if end_mjd is None or end_mjd < ho.end_mjd:
                        end_mjd = ho.end_mjd

                    frequencies.add(ho.frequency)

                    sas_ids.add(ho.sas_id)

                    self._target_station_name = station_name
                    reference_stations.update(hbs.reference_station_names)
                    try:
                        # MOD 1000 in case of the HBA_ONE specification
                        single_beamlet = int(beam_specification.beamlets) % 1000
                    except ValueError as e:
                        logger.exception('Target station specification incorrect')
                        raise e

                    beamlets.add(single_beamlet)

                    virtual_pointing[(ho.frequency, single_beamlet)] = \
                        (beam_specification.virtual_pointing['ra'],
                         beam_specification.virtual_pointing['dec'],
                         beam_specification.virtual_pointing['coordinate_system'])
            else:
                continue

        if len(source_position) == 1:
            self._source_position = numpy.array(source_position.pop(), dtype=HDS_coordinate_type)
        else:
            logger.error('Multiple source positions are not supported: %s', source_position)
            raise ValueError('Multiple source positions are not supported')

        if len(mode) == 1:
            self._mode = mode.pop()
        else:
            raise ValueError('Multiple RCUs mode are not supported')

        if len(source_name) == 1:
            self._source_name = source_name.pop()
        else:
            raise ValueError('Multiple source name are not supported')

        if len(antenna_set) == 1:
            self._antenna_set = antenna_set.pop()
        else:
            raise ValueError('Multiple antenna set are not supported')

        if len(filterset) == 1:
            self._filter = filterset.pop()
        else:
            raise ValueError('Multiple filters are not supported')

        if station_name not in target_stations:
            logger.error('Station %s was not involved in the observation.'
                         ' The target stations for this observation are %s',
                         station_name, target_stations)
            raise Exception('Station %s was not involved in the observation.'
                            % station_name, )

        self._frequencies = sorted(frequencies)
        self._beamlets = sorted(beamlets)
        self._start_time = start_mjd
        self._end_time = end_mjd
        self._sas_ids = list(sas_ids)
        self._reference_stations = list(reference_stations)
        self._rcu_list = list(rcu_list)
        self._used_antennas = list(used_antennas)

        return target_stations, used_antennas, virtual_pointing

    def _collect_from_observation(self, station_name, list_of_hbs_ho_tuples, virtual_pointing, used_antennas):
        self._ra_dec = dict()
        for frequency in self._frequencies:
            frequency_string = str(frequency)
            if frequency not in self._ra_dec:
                self._ra_dec[frequency_string] = dict()
            for beamlet in self._beamlets:
                beamlet_string = str(beamlet)
                ra, dec, _ = virtual_pointing[(frequency, beamlet)]
                if isnan(ra) or isnan(dec):
                    logger.warning('skipping pointing %s for frequency %s malformed : %s',
                                 beamlet_string, frequency_string, virtual_pointing[(frequency, beamlet)])
                    # skip if the pointing is ill specified
                    continue

                self._ra_dec[frequency_string][beamlet_string] = numpy.array(
                    virtual_pointing[(frequency, beamlet)],
                    dtype=HDS_coordinate_type)

        # reads the target station position and the coordinate of its axes
        # and does this only once since the coordinate will not change
        first_holography_observation = list_of_hbs_ho_tuples[0][1]
        first_ms = list(first_holography_observation.ms_for_a_given_beamlet_number.values())[0]
        station_position, tile_offset, axes_coordinates = first_ms. \
            get_station_position_tile_offsets_and_axes_coordinate_for_station_name(
            station_name)

        logger.debug('selecting used antenna ids %s', used_antennas)
        self._antenna_field_position = [list(station_position - antenna_offset)
                                        for antenna_id, antenna_offset in enumerate(tile_offset)
                                        if antenna_id in used_antennas]
        self._target_station_position = list(station_position)
        self._rotation_matrix = axes_coordinates

    def print_dataset(self, text=None):
        '''
        Print out the values of an HDS.
        @param hds:  The HDS of which values shall be printed.
        @param text:  An additional text string that will be printed alongside
        the values in the HDS.
        '''
        if text is not None and isinstance(text, str):
            logger.info("%s", text)
        logger.info("Version = %s", self._version)
        logger.info("Mode = %s", self._mode)
        logger.info("RCU list = ", self._rcu_list)
        logger.info("SAS IDs = %s", self._sas_ids)
        logger.info("Target station name = %s", self._target_station_name)
        logger.info("Target station position = %s", self._target_station_position)
        logger.info("Source name = %s", self._source_name)
        logger.info("Source position = %s", self._source_position)
        logger.info("Central beamlets = %s", self._central_beamlets)
        logger.info("Start time = %s", self._start_time)
        logger.info("End time = %s", self._end_time)
        logger.info("Rotation matrix = %s", self._rotation_matrix)
        logger.info("Antenna field position = %s", self._antenna_field_position)
        logger.info("Reference stations = %s", self._reference_stations)
        logger.info("Frequencies = %s", self._frequencies)
        logger.info("Beamlets = %s", self._beamlets)
        logger.info("RA DEC = %s", self._ra_dec)
        logger.info("Data = %s", self.data)

    @staticmethod
    def print_info(hds=None, text=None):
        if hds is not None and isinstance(hds, HolographyDataset) is True:
            hds.print_dataset(text)
        else:
            logger.warning(
                "The object passed is not a HolographyDataset instance.  Cannot print any data.")

    def _read_grouped_data(self, h5file, uri):
        """
        Read the data in a nested hierarchy starting from the address uri
        into a python dict
        :param h5file: input HDF5 file
        :type h5file:  h5py.File
        :param uri: starting point address
        :type uri: str
        :return: a dict encoding the structure
        """
        starting_leaf = h5file[uri]
        result = dict()

        # Read the attributes in the hdf5 dataset
        for attribute_key, attribute_value in starting_leaf.attrs.items():
            if 'ATTRIBUTES' not in result:
                result['ATTRIBUTES'] = dict()
            result['ATTRIBUTES'][attribute_key] = attribute_value

        for key, value in starting_leaf.items():

            if isinstance(value, h5py.Group) is True:
                result[key] = self._read_grouped_data(h5file, value.name)
            else:
                try:
                    if numpy.issubdtype(value.dtype, numpy.string_):
                        result[key] = str(numpy.array(value).astype(str))
                    else:
                        result[key] = numpy.array(value)
                except ValueError as e:
                    logger.exception('Cannot interpret %s a a numpy array: %s', type(value), e)
                    raise e
        return result

    def _store_grouped_data(self, h5file, uri, data_to_store):
        """
        Store the data in a nested hierarchy starting from the address uri
        into the HDS
        :param h5file: input HDF5 file
        :type h5file:  h5py.File
        :param uri: starting point address
        :type uri: str
        :param data_to_store: dict that contains the data to store
        :type data_to_store: dict
        :return: a dict encoding the structure
        """
        if uri not in h5file:
            starting_leaf = h5file.create_group(uri)
        else:
            starting_leaf = h5file[uri]

        # Store the attributes in the hdf5 dataset

        attributes = data_to_store.get('ATTRIBUTES', dict())
        for attribute_key, attribute_value in attributes.items():
            starting_leaf.attrs[attribute_key] = attribute_value

        for key, value in data_to_store.items():
            if key == 'ATTRIBUTES':
                continue
            if isinstance(value, dict) is True:
                self._store_grouped_data(h5file, '/'.join([uri, key]), value)
            else:
                try:
                    if isinstance(value, str):
                        starting_leaf[key] = numpy.string_(value)
                    else:
                        starting_leaf[key] = value

                except ValueError as e:
                    logger.exception('Cannot interpret %s a a numpy array: %s', type(value), e)
                    raise e

    @staticmethod
    def load_from_file(path):
        """
        Read a holography dataset from an HDF5 file and returns a
        HolographyDataset class
        :param path: path to file
        :return: the read dataset
        """
        f = None

        if not os.path.exists(path):
            raise FileNotFoundError(path)

        try:
            f = h5py.File(path, 'r')

            result = HolographyDataset.read_holography_file_from_h5file(f)

            result.data = dict()
            for reference_station in f["CROSSCORRELATION"].keys():
                for frequency in f["CROSSCORRELATION"][reference_station].keys():
                    for beamlet in f["CROSSCORRELATION"][reference_station][frequency].keys():
                        if reference_station not in result.data:
                            result.data[reference_station] = dict()
                        if frequency not in result.data[reference_station]:
                            result.data[reference_station][frequency] = dict()
                        result.data[reference_station][frequency][beamlet] = numpy.array(
                            f["CROSSCORRELATION"][reference_station][frequency][beamlet])

            if '/DERIVED_DATA' in f:
                result.derived_data = result._read_grouped_data(f, '/DERIVED_DATA')
        except Exception as e:
            logger.exception(
                "Cannot read the Holography Data Set data from the HDF5 file \"%s\"."
                " This is the exception that was thrown:  %s",
                path, e)
            raise e
        finally:
            if f is not None:
                f.close()

        return result

    @staticmethod
    def read_holography_file_from_h5file(f: h5py.File):
        """
        Opens the Holography HDF file for access and returns the Holography Dataset and
        it's file descriptor
        :param path: path to file
        :return: the read dataset
        :rtype: List[HolographyDataset, h5py.File]
        """
        try:

            result = HolographyDataset()
            result._version = f.attrs[HDS_VERSION]
            result._mode = f.attrs[HDS_MODE]
            result._rcu_list = list(f.attrs[HDS_RCU_LIST])
            result._sas_ids = list(f.attrs[HDS_SAS_ID])
            result._target_station_name = f.attrs[HDS_TARGET_STATION_NAME]
            result._target_station_position = list(f.attrs[HDS_TARGET_STATION_POSITION])
            result._source_name = f.attrs[HDS_SOURCE_NAME]
            result._source_position = numpy.array(f.attrs[HDS_SOURCE_POSITION])
            (result._start_time, result._end_time) = f.attrs[HDS_OBSERVATION_TIME]
            result._rotation_matrix = f.attrs[HDS_ROTATION_MATRIX]
            result._beamlets = list(f.attrs[HDS_BEAMLETS])
            result._antenna_field_position = f.attrs[HDS_ANTENNA_FIELD_POSITION].tolist()
            result._reference_stations = bytestring_list_to_string(list(f[HDS_REFERENCE_STATION]))

            if float(result._version) >= 1.1:
                result._antenna_set = f.attrs[HDS_ANTENNASET]
                result._filter = f.attrs[HDS_FILTER]
                result._used_antennas = f.attrs[HDS_USEDANTENNAS]

            result._frequencies = list(f[HDS_FREQUENCY])

            if HDS_CALIBRATION_TABLES in f:
                for mode in f[HDS_CALIBRATION_TABLES]:
                    uri = '/%s/%s' % (HDS_CALIBRATION_TABLES, mode)
                    result._calibration_tables[mode] = \
                        CalibrationTable.load_from_hdf(file_descriptor=f, uri=uri)

            if HDS_DERIVED_CAL_TABLES in f:
                for mode in f[HDS_DERIVED_CAL_TABLES]:
                    uri = '/%s/%s' % (HDS_DERIVED_CAL_TABLES, mode)
                    result._calibration_tables[mode] = \
                        CalibrationTable.load_from_hdf(file_descriptor=f, uri=uri)

            result._ra_dec = dict()
            for frequency in f["RA_DEC"].keys():
                for beamlet in f["RA_DEC"][frequency].keys():

                    if frequency not in result._ra_dec:
                        result._ra_dec[frequency] = dict()

                    result._ra_dec[frequency][beamlet] = numpy.array(f["RA_DEC"][frequency][beamlet])

            beamlets = set()
            for reference_station in f["CROSSCORRELATION"].keys():
                for frequency in f["CROSSCORRELATION"][reference_station].keys():
                    for beamlet in f["CROSSCORRELATION"][reference_station][frequency].keys():
                        beamlets.add(int(beamlet))

            result.data = f['CROSSCORRELATION']

            result._central_beamlets = result._read_grouped_data(f, HDS_CENTRAL_BEAMLETS)

            if '/DERIVED_DATA' in f:
                result.derived_data = f['/DERIVED_DATA']

        except Exception as e:
            logger.exception(
                "Cannot read the Holography Data Set data from the HDF5 file \"%s\"."
                " This is the exception that was thrown:  %s",
                f.filename, e)
            raise e

        return result

    def insert_calibration_table(self, caltable: CalibrationTable):
        mode = caltable.observation_mode
        self._calibration_tables[mode] = caltable

    def store_to_file(self, path):
        """
        Stores the holography dataset at the given path
        :param path: path to file
        """
        f = None
        # Prepare the HDF5 data structs.
        try:
            f = h5py.File(path, "w")

            # Create the ATTRS
            # Start with the version information
            f.attrs[HDS_VERSION] = HOLOGRAPHY_DATA_SET_VERSION

            # RCU list
            f.attrs[HDS_RCU_LIST] = numpy.array(self._rcu_list, dtype=int)

            # RCU mode
            f.attrs[HDS_MODE] = self._mode

            # Moan...  Again this needs to be stored like that.
            f.attrs[HDS_SAS_ID] = numpy.array(self._sas_ids,
                                              dtype=h5py.special_dtype(vlen=str))
            f.attrs[HDS_TARGET_STATION_NAME] = self._target_station_name
            f.attrs[HDS_TARGET_STATION_POSITION] = self._target_station_position
            f.attrs[HDS_SOURCE_NAME] = self._source_name

            f.attrs[HDS_SOURCE_POSITION] = self._source_position
            f.attrs[HDS_OBSERVATION_TIME] = numpy.array([self._start_time, self._end_time])
            f.attrs[HDS_ROTATION_MATRIX] = self._rotation_matrix
            f.attrs[HDS_ANTENNA_FIELD_POSITION] = self._antenna_field_position
            f.attrs[HDS_BEAMLETS] = self._beamlets
            f.attrs[HDS_FILTER] = self._filter
            f.attrs[HDS_ANTENNASET] = self._antenna_set
            f.attrs[HDS_USEDANTENNAS] = self._used_antennas
            # Store the list of reference stations and _frequencies.  We just
            # want to keep 'em around for quick reference.
            f[HDS_REFERENCE_STATION] = to_numpy_array_string(self._reference_stations)
            f[HDS_FREQUENCY] = self._frequencies


            f.create_group(HDS_CALIBRATION_TABLES)
            for mode in self._calibration_tables:
                self._calibration_tables[mode].store_to_hdf(f,
                                                            '/{}/{}'.format(
                                                                HDS_CALIBRATION_TABLES, mode))
            for mode in self.derived_calibration_tables:
                self.derived_calibration_tables[mode].store_to_hdf(f,
                                                                   '/{}/{}'.format(
                                                                       HDS_DERIVED_CAL_TABLES,
                                                                       mode))

            self._store_grouped_data(h5file=f, uri='/RA_DEC',
                                                  data_to_store=self._ra_dec)

            # We create groups for the reference stations and the _frequencies.
            # Then we store the data samples [XX, YY, XY, YX, t, l, m, flag]
            # in an array.  The reference station name, the frequency and the
            # beamlet number (index of the data sample array) allow random
            # access of the data.
            self._store_grouped_data(h5file=f, uri='/CROSSCORRELATION',
                                                  data_to_store=self.data)

            self._store_grouped_data(h5file=f,
                                                  uri=HDS_CENTRAL_BEAMLETS,
                                                  data_to_store=self._central_beamlets)
            if self.derived_data:
                self._store_grouped_data(h5file=f,
                                         uri='/DERIVED_DATA',
                                         data_to_store=self.derived_data)

        except Exception as e:
            logger.exception(
                "Cannot write the Holography Data Set data to the HDF5 file \"%s\"."
                " This is the exception that was thrown:  %s",
                path, e)
            raise e
        finally:
            if f is not None:
                f.close()
