import numpy

HOLOGRAPHY_DATA_SET_VERSION = 1.1

# Allowed HDS keywords
#
# ATTRS
HDS_VERSION = "HDS_version"
HDS_MODE = "Mode"
HDS_RCU_LIST = "RCU_list"
HDS_SAS_ID = "SAS_ids"
HDS_TARGET_STATION_NAME = "Target_station_name"
HDS_TARGET_STATION_POSITION = "Target_station_position"
HDS_SOURCE_NAME = "Source_name"
HDS_SOURCE_POSITION = "Source_position"
HDS_OBSERVATION_TIME = "Observation_time"
HDS_ROTATION_MATRIX = "Rotation_matrix"
HDS_ANTENNA_FIELD_POSITION = "Antenna_field_position"
HDS_SPECIFIED_REFERENCE_STATION = "Specified_Reference_station"
HDS_SPECIFIED_FREQUENCY = "Specified_frequency"
HDS_BEAMLETS = "Beamlets"
HDS_CENTRAL_BEAMLETS = "Central_beamlets"
HDS_CALIBRATION_TABLES = "Calibration_tables"
HDS_DERIVED_CAL_TABLES = "Derived_calibration_tables"
HDS_FILTER = 'Filter'
HDS_ANTENNASET = 'AntennaSet'
HDS_USEDANTENNAS = 'Antennas'

# GROUP "RA DEC"
HDS_SPECIFIED_RA_DEC = "Specified_RA_DEC"
HDS_RA_DEC = "RA_DEC"

HDS_REFERENCE_STATION = "Reference_station"
HDS_FREQUENCY = "Frequency"

# GROUP "CROSSCORRELATION"
HDS_DATA = "Data"

# The HDF5 coordinate data type. 
HDS_coordinate_type = numpy.dtype([
    ('RA', numpy.float64),
    ('DEC', numpy.float64),
    ('EPOCH', '<S10')])

# One element that is stored in "Data".
HDS_data_sample_type = numpy.dtype([
    ('XX', numpy.complex64),
    ('XY', numpy.complex64),
    ('YX', numpy.complex64),
    ('YY', numpy.complex64),
    ('l', numpy.float64),
    ('m', numpy.float64),
    ('t', numpy.float64),
    ('flag', bool)])


HDS_sampled_illumination_type = numpy.dtype([
    ('XX', numpy.complex64),
    ('YY', numpy.complex64),
    ('x', numpy.float64),
    ('y', numpy.float64),
    ('z', numpy.float64)])
